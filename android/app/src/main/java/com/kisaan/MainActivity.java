package com.kisaan;

import android.content.Intent;
import android.content.res.Configuration;
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen; // Import this.
import android.os.Bundle; // Import this.
public class MainActivity extends ReactActivity {
  @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);
        super.onCreate(savedInstanceState);
    }
  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
      @Override
       public void onConfigurationChanged(Configuration newConfig) {
           super.onConfigurationChanged(newConfig);
           Intent intent = new Intent("onConfigurationChanged");
           intent.putExtra("newConfig", newConfig);
           this.sendBroadcast(intent);
       }

  @Override
  protected String getMainComponentName() {
    return "kisaan";
  }
}
