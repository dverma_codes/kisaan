import Register from './src/views/Register';
import LoginWith from './src/views/LoginWith';
import ForgotPassword from './src/views/ForgotPassword';
import Login from './src/views/Login';
import Searchscreen from  './src/components/searchbarscreen';
import Home from './src/views/Drawer/index';
import YourAddresses from './src/views/Profile/YourAddresses/YourAddresses';
import AddressForm from './src/views/Profile/YourAddresses/AddressForm/AddressForm';
import ProfileAndSecurity from './src/views/Profile/ProfileAndSecurity/ProfileAndSecurity';
import MyAccount from './src/views/Profile/MyAccount/MyAccount';
import DrawerHome from './src/views/Tabnav/Tabstack';
import PaymentOptions from './src/views/Profile/PaymentOptions/PaymentOptions';
import Shopbycat from './src/views/Category/Shopbycategory';
import VariableProduct from './src/views/Product_page/variableproduct';
import Orders from './src/views/Yoursorders/orders';

import Cartmain from './src/views/cart/cartmain';
import Recentbuy from './src/views/dependpage/recentbuy';
import Lastoders from './src/views/dependpage/last30oders';
import Oderfilter from './src/views/dependpage/oderfilter';
import notsupported from './src/components/notsuppported';
import Address from "./src/views/Address/Address";
import Payment from "./src/views/Paymentpages/Payment";
import Trackorder from "./src/views/Trackorder/Trackorder";
import Contactus from "./src/views/Contactus";
export default [
    {name: "Home", component: Home, options: {headerShown: false}, isPrivate: false},
]

export const drawerNavigation = [

    {name: "DrawerHome", component: DrawerHome, options: {headerShown: false}, isPrivate: false},
    {name: "MyAccount", component: MyAccount, options: {headerShown: false}, isPrivate: false},
    {name: "MyAccount.YourAddresses", component: YourAddresses, options: {headerShown: false}, isPrivate: false},
    {name: "MyAccount.YourAddresses.AddAddress", component: AddressForm, options: {headerShown: false}, isPrivate: false},
    {name: "MyAccount.ProfileAndSecurity", component: ProfileAndSecurity, options: {headerShown: false}, isPrivate: false},
    {name: "MyAccount.PaymentOptions", component: PaymentOptions, options: {headerShown: false}, isPrivate: false},
    {name: "Category", component: Shopbycat, options: {headerShown: false}, isPrivate: false},
    {name: "Product", component: VariableProduct, options: {headerShown: false}, isPrivate: false},
    {name: "Orders", component: Orders, options: {headerShown: false}, isPrivate: false},
    {name: "Cart", component: Cartmain, options: {headerShown: false}, isPrivate: false},
    {name: "BuyAgain", component: Recentbuy, options: {headerShown: false}, isPrivate: false},
    {name: "LastMonth", component: Lastoders, options: {headerShown: false}, isPrivate: false},
    {name: "Filter", component: Oderfilter, options: {headerShown: false}, isPrivate: false},
    {name: "Searchscreen", component: Searchscreen, options: {headerShown: false}, isPrivate: false},
    {name: "Notsupported", component: notsupported, options: {headerShown: false}, isPrivate: false},
    {name: "Address", component: Address, options: {headerShown: false}, isPrivate: false},
    {name: "Payment", component: Payment, options: {headerShown: false}, isPrivate: false},
    {name: "TrackOrder", component: Trackorder, options: {headerShown: false}, isPrivate: false},
    {name: "Contact", component: Contactus, options: {headerShown: false}, isPrivate: false},
];

export const authNavigation = [
    {name: "LoginWithScreen", component: LoginWith, options: {headerShown: false}, isPrivate: false},
    {name: "LoginScreen", component: Login, options: {headerShown: false}, isPrivate: false},
    {name: "RegisterScreen", component: Register, options: {headerShown: false}, isPrivate: false},
    {name: "ForgotPasswordScreen", component: ForgotPassword, options: {headerShown: false}, isPrivate: false}
];
