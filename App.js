import React from 'react';

import AppContainer from './src/containers/App';
import {NavigationContainer} from '@react-navigation/native';
import Loading from './src/views/Loading';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import MainNavigation from './src/components/Navigation/MainNavigation/MainNavigation';

const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
        ...DefaultTheme.colors,
        primary: '#2b5938',
        accent: '#f1c40f',
    },
};

function App({auth, loading}) {
    return (
        <PaperProvider theme={theme}>
            <NavigationContainer>
                <MainNavigation authUser={auth.user}/>
            </NavigationContainer>
        </PaperProvider>
    );
}

export default AppContainer(App);
