import React from 'react';

import {TypingAnimation} from 'react-native-typing-animation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';
import {firebase} from '@react-native-firebase/auth';

import * as themes from '../sliderfirsttime/theme';
import Gender from 'react-native-vector-icons/Fontisto';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import asyncStr from '../config/Asyncstr';

export default class signupcom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Name: '',
      gender: 'M',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.tittle}>Sign Up</Text>

          <View style={styles.action}>
            <View style={styles.section}>
              <View styles={{minHeight: 215}}>
                <DropDownPicker
                  items={[
                    {
                      label: 'Mr',
                      value: 'M',
                      icon: () => (
                        <Gender
                          name="male"
                          size={18}
                          color={themes.colors.primary}
                        />
                      ),
                    },
                    {
                      label: 'Mrs',
                      value: 'F',
                      icon: () => (
                        <Gender
                          name="female"
                          size={18}
                          color={themes.colors.primary}
                        />
                      ),
                    },
                  ]}
                  defaultValue={this.state.gender}
                  containerStyle={{height: 50, width: 90}}
                  style={{backgroundColor: '#fff'}}
                  itemStyle={{
                    justifyContent: 'flex-start',
                  }}
                  dropDownStyle={{backgroundColor: '#ffffff'}}
                  onChangeItem={(item) =>
                    this.setState({
                      gender: item.value,
                    })
                  }
                />
              </View>

              <TextInput
                placeholder="Name"
                style={styles.textinput}
                keyboardType={'default'}
                onChangeText={(Name) => {
                  this.setState({Name});
                }}
                maxLength={20}
              />
            </View>
            <View style={styles.footer} />
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={null}
                style={[
                  styles.button,
                  {
                    backgroundColor: themes.colors.primary,
                    borderColor: themes.colors.white,
                    borderWidth: 0,
                    borderRadius: 15,
                    marginTop: 80,
                  },
                ]}>
                <Text style={{color: 'white'}}>Finish </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 100,
  },
  tittle: {
    color: themes.colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
  text: {
    color: 'grey',
  },
  section: {
    flexDirection: 'row',
    borderWidth: 0,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10,
    minHeight: 80,
  },
  textinput: {
    flex: 1,
    paddingLeft: 30,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: themes.colors.tertiary,
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
