

import React, { useState } from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {firebase} from '@react-native-firebase/auth';
import LoginContainer from '../containers/Auth/Login';

import * as themes from '../sliderfirsttime/theme';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
// import AsyncStorage from 'react-native';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator
  
} from 'react-native';
import asyncStr from '../config/Asyncstr';

class login extends React.Component  {
  state = {
    phone: '',
    confirmResult: null,
    verificationCode: '',
    userId: '',
    isload:false
    
  }
  componentDidMount()
  {
   this.checktosave();
   firebase.auth().onAuthStateChanged( (user) => {
    if (user) {
        console.warn(user);
        this.setState({ userId: user.uid })
        this.props.navigation.navigate('mainScreen');     
       } 
   
});
  }
  
  checktosave = async () => {
    // try {
    //   const value = await AsyncStorage.getItem('isload');
    //   if (value !== null) {
    //  // there is data so not to add again 
    //   }
    //   else{
    //     asyncStr.storeDatalogin(); 
     
    //   }
    // } catch (error){
    
    // }
  };



validatePhoneNumber = () => {
  var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/
  return regexp.test(this.state.phone)
}
handleSendCode = () => {
  const er ='';// Request to send OTP
  if (this.validatePhoneNumber()) {
    firebase
      .auth()
      .signInWithPhoneNumber(this.state.phone)
      .then(confirmResult => {
        this.setState({ confirmResult })
        
      })
      .catch(error => {
        alert(error.message)
        er ='error';
        console.log(error)
      })
      if(er!=='error')
      {//this.props.navigation.navigate('otpscreen');
      }
    } else {
    alert('Invalid Phone Number')
  }
}


handleVerifyCode = () => {
  // Request for OTP verification
  const { confirmResult, verificationCode } = this.state
  if (verificationCode.length == 6) {
  confirmResult
  .confirm(verificationCode)
  .then(user => {
  this.setState({ userId: user.uid })
  

 if(user)
 {
  this.props.navigation.navigate('mainScreen');   
 } 
  })
  .catch(error => {
  alert(error.message)
  console.log(error)
  
  })
  } else {
  alert('Please enter a 6 digit OTP code.')
  }
  }
 



  render()
  {
   if(!this.state.confirmResult)
   {
    return(
      <View style={styles.container}>
      
   
        <View> 
        <Text style={styles.tittle}>LOGIN</Text>
      <Text style={styles.text}>Phone number with country code</Text>
      <View style={styles.action}>
          <View style={styles.section}>
          <MaterialIcons name="call" size={20} color={themes.colors.primary} />
          
          <TextInput
          placeholder="Phone Number" 
          style={styles.textinput}  
          keyboardType={'phone-pad'}
      
          onChangeText={phone => {
          this.setState({ phone })
          }}
          maxLength={13}
          editable={this.state.confirmResult ? false : true}
   
          />
                
          </View>
          <View style ={styles.footer}></View>
      <View style={{flexDirection:'row'}}>
      
      <TouchableOpacity 
     onPress ={ this.handleSendCode
     }
      style={[styles.button,{ 
        backgroundColor:themes.colors.primary,
        borderColor:themes.colors.white,
        borderWidth:0,
        borderRadius:15,
        marginTop:60}]

      }>
        <Text style={{color:'white'}}>GENERATE OTP </Text>
      </TouchableOpacity>
      
      </View>
      

      </View>
      </View>
      </View> 
   
      //</View>
    )}
    else{
     return(
      <View style={styles.container}>
    

      <View> 
      <Text style={styles.tittle}>OTP</Text>
    <Text style={styles.text}>Enter 6 digit OTP send to your Number {this.state.phone}</Text>
    <View style={styles.action}>
        <View style={styles.section}>
        <FontAwesome5 name="lock" size={20} />
        <TextInput
        placeholder="*****" 
        style={styles.textinput}  
        keyboardType={'numeric'}
            onChangeText={verificationCode => {
            this.setState({ verificationCode })
            }}
        maxLength={6}
   

        />
          
        </View>
        <View style ={styles.footer}>
    <View style={{flexDirection:'row'}}>
    
    <TouchableOpacity 
  onPress ={ this.handleVerifyCode
  }
    style={[styles.button,{ 
      backgroundColor:themes.colors.primary,
      borderColor:themes.colors.white,
      borderWidth:0,
      borderRadius:15,
      marginTop:60}]

    }>
      <Text style={{color:'white'}}>VERIFY </Text>
    </TouchableOpacity>
    
    </View>
    

    </View>
    </View>
    </View> 
    
    </View>
)}
  }
  }

const {width, height}=Dimensions.get("screen");  

const widthbutton =width *0.8;
const styles =StyleSheet.create({
  container:
  {
   flex:1,
   backgroundColor:'white',
   justifyContent:'center',
   paddingHorizontal:30,
   paddingVertical:100
  },
  tittle:
  {
    color:themes.colors.primary,
    fontWeight:'bold',
    fontSize:30
  },
  text:
  {
    color:'grey'
  },
  section:
  {
    flexDirection:'row',
    borderWidth:1,
    borderRadius:5,
    paddingHorizontal:15,
    paddingVertical:10,
    alignItems:'center',
    marginTop:10

  },
  textinput:
  {
    flex:1,
    paddingLeft:30
  },
  button:
{
width:widthbutton,
height:40,
justifyContent:"center",
alignItems:"center"
},
  header:{
  flex:2,
  justifyContent:'center',
  alignItems:'center'},
footer:
  {
   flex:1,
   justifyContent:'center',
   alignItems:'center'
  }

});

export default LoginContainer(login);