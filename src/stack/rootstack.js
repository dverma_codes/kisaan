/* eslint-disable prettier/prettier */
import React from 'react';

import {Text} from 'react-native';

import {createAppContainer, createSwitchNavigator}  from 'react-navigation';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';


import Register from '../views/Register';
// import Login from '../views/Login';
// import ForgotPassword from '../views/ForgotPassword';
// import Home from '../views/Home';
// import Loading from '../views/Loading';
import LoginWith from '../views/LoginWith';
import Home from '../views/Drawer/index';
//import signupcom from "./signup";
//import swipper from "./swipper";
//import splash from '../splash/splash';
//import main from '../drawer/index'
//import dash from '../dashboard/dashboard'

// const AppStack = createStackNavigator({
//   HomeScreen:
//   {
//     screen: main,
//     navigationOptions:{headerShown:false},
//   },
// });

// const AuthStack = createStackNavigator({
//   LoginWithScreen:
//   {
//     screen: LoginWith,
//     navigationOptions:{headerShown:false},
//   },
//   RegisterScreen:
//   {
//     screen: Register,
//     navigationOptions:{headerShown:false},
//   },
//    LoginScreen:
//    {
//      screen:Login,
//     navigationOptions:{headerShown:false},
//  },
//  ForgotPasswordScreen:
//  {
//    screen:ForgotPassword,
//   navigationOptions:{headerShown:false},
// },

//  });

// const stackNavigator = createSwitchNavigator(
//     {
//       AuthLoading: Loading,
//       App: AppStack,
//       Auth: AuthStack,
//     },
//     {
//       // initialRouteName: 'Auth',
//       initialRouteName: 'App',
//     }
//   );


// const Stacknavigator = createStackNavigator({
//     RegisterScreen:{
//         screen: Register,
//         navigationOptions:{
//             headerShown:false
//         }
//     }
//     // SignupScreen:{
//     //     screen:signupcom,
//     //     navigationOptions:{
//     //         headerShown:false
//     //     }
//     // }

//     // mainScreen:
//     // {
//     //     screen:main,
//     //     navigationOptions:{
//     //         headerShown:false
//     //     }
//     // }
// });

const Auth = createStackNavigator();

function AuthStack() {
  return (
    <Auth.Navigator>
      <Auth.Screen name="LoginWithScreen" component={LoginWith} options={{ headerShown: false }}/>
      <Auth.Screen name="RegisterScreen" component={Register} options={{ headerShown: false }}/>
    </Auth.Navigator>
  );
}

const Stack = createStackNavigator();

const stackNavigator = props => {
  return <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} options={{ headerShown: false }}/>
      <Stack.Screen name="Auth" component={AuthStack} options={{ headerShown: false }}/>
    </Stack.Navigator>
  </NavigationContainer>
}

export default stackNavigator;
// export default createAppContainer(stackNavigator);
