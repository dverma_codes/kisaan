const colors = {
  accent: '#f0f5f9',
  primary: '#2b5938',
  pricedetails:'#e4ecf3',
  brown:'#672900',
  secondary: '#4a7355',
  textcolor: '#0c461b',
  black: '#000000',
  whiteback: '#f9f9f9',
  white: '#ffffff',
  gray: '#b3b3b3',
  gray2: '#D3D3D3',
  checkavail: '#2b5938',
};
const sizes = {
  // global sizeshttps://www.atmeplay.com/images/users/avtar/avtar.png
  base: 12,
  font: 12,
  border: 0,

  // font sizes
  h1: 40,
  h2: 30,
  h3: 10,
  title: 16,
  body: 12,
  caption: 12,
  small: 8,
};

const fonts = {
  h1: {
    fontFamily: 'Poppins-Regular',
    fontSize: sizes.h1,
  },
  h2: {
    fontFamily: 'Poppins-Regular',
    fontSize: sizes.h2,
  },
  h3: {
    fontFamily: 'Poppins-Regular',
    fontSize: sizes.h3,
  },
  title: {
    fontFamily: 'Poppins-Regular',
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
  small: {
    fontSize: sizes.small,
  },
};

export {colors, sizes, fonts};
