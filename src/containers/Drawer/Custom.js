import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
// import * as AuthActions from '../../redux/actions/auth';
import { logout } from '../../redux/actions/auth';

function ComponentsContainer(WrappedComponent) {
  const mapStateToProps = (state, ownProps) => {
    return {
        auth: state.auth,
    };
  };

  const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(
      {
          logout
      },
      dispatch,
    ),
  });

  WrappedComponent = connect(
    mapStateToProps,
    mapDispatchToProps,
  )(WrappedComponent);

  return class extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}

export default ComponentsContainer;
