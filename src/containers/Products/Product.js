import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getProductById} from '../../redux/actions/products';

function ComponentsContainer(WrappedComponent) {
  const mapStateToProps = (state, ownProps) => {
    return {};
  };

  const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        getProductById
      },
      dispatch,
    ),
  });

  WrappedComponent = connect(
    null,
    mapDispatchToProps,
  )(WrappedComponent);

  return class extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}

export default ComponentsContainer;
