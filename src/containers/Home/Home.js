import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {fetchHome} from '../../redux/actions/home';

function ComponentsContainer(WrappedComponent) {
  const mapStateToProps = (state, ownProps) => {
    return {
      // auth: state.auth,
    };
  };

  const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(
      {
        fetchHome
      },
      dispatch,
    ),
  });

  WrappedComponent = connect(
    null,
    mapDispatchToProps,
  )(WrappedComponent);

  return class extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}

export default ComponentsContainer;
