import React from 'react';
import {connect, Provider} from 'react-redux'
// import App from '../App';
// import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import * as CommonActions from "../redux/actions/common";
import * as AuthActions from "../redux/actions/auth";
import {firebaseInit} from '../helpers/firebase';
import auth from '@react-native-firebase/auth';

function AppContainer(WrappedComponent) {
    const mapStateToProps = (state, ownProps) => {
        return ({
            auth: state.auth,
            loading: state.common.showLoader
        })
    }

    const mapDispatchToProps = dispatch => ({
        actions: bindActionCreators({
            checkAuth: CommonActions.checkAuth,
            toggleLoader: CommonActions.toggleLoader,
            saveIntendedUrl: AuthActions.saveIntendedUrl,
            storeUser: AuthActions.storeUser,
            showError: CommonActions.showError,
            logout: AuthActions.logout,
            toggleEmailPasswordLoginForm: AuthActions.toggleEmailPasswordLoginForm,
            toggleLoginForm: AuthActions.toggleLoginForm,
        }, dispatch)
    })

    class ApContainer extends React.Component {

        componentDidMount() {

            firebaseInit();

            console.log('auth user: ');
            console.log(this.props.auth.user);

            this.checkAuth();
        }

        checkAuth() {
            auth().onAuthStateChanged((user) => {
                if (user) {

                    if (!user.emailVerified) {
                        this.props.actions.showError("Email not verified");

                        this.props.actions.logout();
                        return;
                    }

                    user.getIdTokenResult().then(user => {
                        if (typeof user.claims.admin !== "undefined" && user.claims.admin) {
                            this.props.actions.storeUser(null);
                        } else {
                            this.props.actions.toggleEmailPasswordLoginForm(false);
                            this.props.actions.storeUser(user);
                        }
                    })
                    .catch(err => {
                        this.props.actions.storeUser(null);
                    })
                } else {
                    this.props.actions.toggleEmailPasswordLoginForm(false);
                    this.props.actions.toggleLoginForm(false);
                    this.props.actions.storeUser(null);
                }
            });
        }

        render() {

            return <>
                <WrappedComponent {...this.props} />
            </>;
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(ApContainer);
}

export default AppContainer;