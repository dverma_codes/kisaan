import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as AuthActions from '../../redux/actions/auth';

function ComponentsContainer(WrappedComponent) {
  const mapStateToProps = (state, ownProps) => {
    return {
      auth: state.auth,
    };
  };

  const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(
      {
        toggleLoginForm: AuthActions.toggleLoginForm,
        attemptLogin: AuthActions.attemptLogin,
        attemptGoogleLogin: AuthActions.attemptGoogleLogin,
        attemptFacebookLogin: AuthActions.attemptFacebookLogin,
        toggleEmailPasswordLoginForm: AuthActions.toggleEmailPasswordLoginForm,
        toggleForgotForm: AuthActions.toggleForgotForm,
      },
      dispatch,
    ),
  });

  WrappedComponent = connect(
    mapStateToProps,
    mapDispatchToProps,
  )(WrappedComponent);

  return class extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}

export default ComponentsContainer;
