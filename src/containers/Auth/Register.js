import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as AuthActions from '../../redux/actions/auth';

function ComponentsContainer(WrappedComponent) {
  const mapStateToProps = (state, ownProps) => {
    return {
      auth: state.auth,
    };
  };

  const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(
      {
        toggleRegisterForm: AuthActions.toggleRegisterForm,
        attemptRegister: AuthActions.attemptRegister,
        attemptGoogleLogin: AuthActions.attemptGoogleLogin,
        attemptFacebookLogin: AuthActions.attemptFacebookLogin,
        attemptTwitterLogin: AuthActions.attemptTwitterLogin,
      },
      dispatch,
    ),
  });

  class Register extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return connect(mapStateToProps, mapDispatchToProps)(Register);
}

export default ComponentsContainer;
