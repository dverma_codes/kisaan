import * as authTypes from './AuthTypes';
import * as commonTypes from './CommonTypes';

export const auth = authTypes;
export const common = commonTypes;
