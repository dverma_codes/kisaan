export const AUTH_LOGIN = "login";
export const SAVE_INTENDED_URL = "intended_url";
export const AUTH_SHOW_LOGIN_FORM = "show_login_form";
export const AUTH_SHOW_REGISTER_FORM = "show_register_form";
export const AUTH_SHOW_EMAIL_LOGIN_FORM = "show_email_login_form";
export const AUTH_SHOW_FORGOT_FORM = "show_forgot_form";