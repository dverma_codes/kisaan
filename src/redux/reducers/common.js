import { common as COMMON_TYPES } from "../constants/ActionTypes";
import {Alert} from 'react-native';
// import { toast } from "react-toastify";

const initialState = {
  showLoader: false
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case COMMON_TYPES.TOGGLE_LOADER:
      return {
        ...state,
        showLoader: action.isShow
      };
    case COMMON_TYPES.SHOW_ERROR:
      console.log(action);
      // toast.error(action.message)

      Alert.alert(
        action.message,
      )

      return state;
    case COMMON_TYPES.SHOW_MESSAGE:
      showToast(action.message_type, action.message);
      return state;
    default:
      return state;
  }
};

const showToast = (type, message) => {
  switch (type) {
    case 'success':
      // toast.success(message);
      break;
    case 'warning':
      // toast.warn(message);
      break;
    default:
      // toast.info(message);
      break;
  };
}
