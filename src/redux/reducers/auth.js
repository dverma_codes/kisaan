import {
  auth as AUTH_TYPES
} from "../constants/ActionTypes";

const initialState = {
  user: null,
  intended_url: '/',
  showLoginForm: false,
  showRegisterForm: false,
  showEmailLoginForm: false,
  showForgotPasswordForm: false
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case AUTH_TYPES.AUTH_LOGIN:
      return {
        ...state,
        user: action.user
      };
    case AUTH_TYPES.AUTH_SHOW_LOGIN_FORM:
        return {
          ...state,
          showLoginForm: !!action.isShow
        }
    case AUTH_TYPES.AUTH_SHOW_EMAIL_LOGIN_FORM:
        return {
          ...state,
          showEmailLoginForm: !!action.isShow
        }
    case AUTH_TYPES.AUTH_SHOW_FORGOT_FORM:
      return {
        ...state,
        showForgotPasswordForm: !!action.isShow
      }
    case AUTH_TYPES.AUTH_SHOW_REGISTER_FORM:
      return {
        ...state,
        showRegisterForm: !!action.isShow
      }
    case AUTH_TYPES.SAVE_INTENDED_URL:
      return {
        ...state,
        intended_url: action.url
      }
    default:
      return state;
  }
};
