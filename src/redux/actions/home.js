import {auth as AUTH_TYPES} from '../constants/ActionTypes';
import {checkAuth, showMessage, showSuccess, toggleLoader} from './common';
import axios from 'axios';

// import firebase, {firebaseInit} from '../../helpers/firebase';
// import auth from '@react-native-firebase/auth';
// import { GoogleSignin } from '@react-native-community/google-signin';

import {Alert} from 'react-native';
import {getApiUrl} from '../../helpers/api';

export const fetchHome = () => dispatch => {
  dispatch(toggleLoader(true));

  return axios
    .get(getApiUrl('app/home'))
    .then((res) => {
      dispatch(toggleLoader(false));
      return res.data;
    })
    .catch((err) => {
      dispatch(toggleLoader(false));
      console.log(err);
      throw err;
    });
};
