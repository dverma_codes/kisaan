import {cart as CART_TYPES} from '../constants/ActionTypes';
import {showError, toggleLoader} from './common';
import axios from 'axios';
import {getApiUrl} from '../../helpers/api';

// import _ from 'lodash';
// import {getApiUrl} from "../../helpers/common";

// export const getProducts = () => dispatch => {
//   // dispatch(toggleLoader(true));
//
//   return axios.get(getApiUrl('products')).then(res => {
//     // dispatch(toggleLoader(false));
//     return res.data;
//   }).catch(err => {
//     // dispatch(toggleLoader(false));
//     throw err;
//   })
// }
//
export const getProductById = (product_id) => dispatch => {
    dispatch(toggleLoader(true));

    return axios.get(getApiUrl(`products/${product_id}`)).then(res => {
        return res.data.data;
    }).catch(err => {
        throw err;
    })
        .finally(() => {
            dispatch(toggleLoader(false));
        })
}
//
// export const getProductsByCategory = (category_id) => dispatch => {
//   dispatch(toggleLoader(true));
//
//   return axios.post(getApiUrl('products'), {
//     category_id
//   }).then(res => {
//     return res.data.data;
//   }).catch(err => {
//     throw err;
//   })
//       .finally(() => {
//         dispatch(toggleLoader(false));
//       })
// }

export const getCategoriesWithProducts = () => dispatch => {
    dispatch(toggleLoader(true));

    return axios.get(getApiUrl('categories/with-products')).then(res => {
        return res.data.data;
    }).catch(err => {
        dispatch(showError(err.message));
        throw err;
    })
        .finally(() => {
            dispatch(toggleLoader(false));
        })
}


// export const checkLocation = (lat, lng, address) => dispatch => {
//   dispatch(toggleLoader(true));
//
//   return axios.post(getApiUrl('products/location/check'), {
//     lat, lng, address
//   }).then(res => {
//     dispatch(toggleLoader(false));
//     return res.data;
//   }).catch(err => {
//     dispatch(toggleLoader(false));
//     dispatch(showError(err.response.data.message))
//     throw err;
//   })
// }
//
// const setLocationData = (location) => {
//   return {
//     type: CART_TYPES.CART_SET_AVAILABILITY_LOCATION,
//     location
//   };
// }
//
// const setLocationDateData = (date) => {
//   return {
//     type: CART_TYPES.CART_SET_AVAILABILITY_LOCATION_DATE,
//     date
//   };
// }
//
// export const setLocation = (location) => dispatch => {
//   dispatch(toggleLoader(true));
//
//   dispatch(setLocationData(location));
//
//   dispatch(toggleLoader(false));
// }
//
// export const setLocationDate = (date) => dispatch => {
//   // dispatch(toggleLoader(true));
//
//   dispatch(setLocationDateData(date));
//
//   // dispatch(toggleLoader(false));
// }
