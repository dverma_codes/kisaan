import {common as COMMON_TYPES} from "../constants/ActionTypes";
// import axios from "axios";
// import {login} from "./auth";
// import {getToken} from "../../helpers/auth";
import {SHOW_ERROR, SHOW_MESSAGE} from "../constants/CommonTypes";

export const toggleLoader = isShow => ({ type: COMMON_TYPES.TOGGLE_LOADER, isShow });

export const loader = (isShow) => {
  return dispatch => {
    dispatch(toggleLoader(isShow));
  };
}

export const showError =  (message) => {
  return dispatch => {
    dispatch({type: SHOW_ERROR, message});
  }
}

export const showMessage =  (message_type, message) => {
  return dispatch => {
    dispatch({type: SHOW_MESSAGE, message, message_type});
  }
}

export const showSuccess = (message) => {
  return showMessage("success", message);
}

export const checkAuth = () => {
  return dispatch => {
    dispatch(toggleLoader(false));
    // import('jwt-simple').then(jwt => {
    //   getToken().then(token => {
    //     if (token) {
    //       dispatch(toggleLoader(true));
    //
    //       try {
    //         dispatch(login(jwt.decode(token, "sj-tokens")));
    //       } catch (res) {
    //         dispatch(showError("Invalid Authentication"));
    //       }
    //
    //       dispatch(toggleLoader(false));
    //     } else {
    //       dispatch(login(null));
    //     }
    //   });
    // });
  }
};
