import {auth as AUTH_TYPES} from '../constants/ActionTypes';
import {checkAuth, showError, showMessage, showSuccess, toggleLoader} from './common';
// import axios from 'axios';
// import {storeToken} from "../../helpers/auth";
// import * as firebase from "firebase/app";
import firebase, {firebaseInit} from '../../helpers/firebase';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';

// import { LoginManager, AccessToken } from 'react-native-fbsdk';

import { Alert } from 'react-native';
// import "firebase/auth";
// import _ from 'lodash';
// import {getApiUrl} from "../../helpers/common";

GoogleSignin.configure({
    webClientId: '254759501147-386jo7ipns37n3g7v4eue22tdr8ua0iv.apps.googleusercontent.com',
});

export const login = user => ({type: AUTH_TYPES.AUTH_LOGIN, user});

export const storeUser = user => {
    return dispatch => {
        console.log("user: ", user);
        dispatch(login(user));
    }
}

export const toggleRegisterForm = (isShow = true) => {
    return dispatch => {
        dispatch({
            type: AUTH_TYPES.AUTH_SHOW_REGISTER_FORM,
            isShow
        })
    }
}

export const toggleLoginForm = (isShow = true) => {
    return dispatch => {
        dispatch({
            type: AUTH_TYPES.AUTH_SHOW_LOGIN_FORM,
            isShow
        })
    }
}

export const toggleEmailPasswordLoginForm = (isShow = true) => {
    return dispatch => {
        dispatch({
            type: AUTH_TYPES.AUTH_SHOW_EMAIL_LOGIN_FORM,
            isShow
        })
    }
}

export const toggleForgotForm = (isShow = true) => {
    return dispatch => {

        dispatch({
            type: AUTH_TYPES.AUTH_SHOW_LOGIN_FORM,
            isShow: !isShow
        });

        dispatch({
            type: AUTH_TYPES.AUTH_SHOW_EMAIL_LOGIN_FORM,
            isShow: !isShow
        });

        dispatch({
            type: AUTH_TYPES.AUTH_SHOW_FORGOT_FORM,
            isShow
        })
    }
}

export const logout = () => {
    return dispatch => {
        auth()
            .signOut()
            .then(() => console.log('User signed out!'))
            .catch(err => console.error(err));
    }
}

export const attemptLogin = (username, password, setErrors) => {
    return dispatch => {
        dispatch(toggleLoader(true));

        console.log('testing')

        auth().signInWithEmailAndPassword(username, password)
            .then((user) => {
                // dispatch(toggleEmailPasswordLoginForm(false))
                // dispatch(toggleLoginForm(false))
                // dispatch(toggleLoader(false));
                console.log('user logged in');
            })
            .catch(err => {
                console.log(err);
                dispatch(toggleLoader(false));

                setErrors({
                    email: "Invalid Credentials"
                })
            })
    }
};

export const attemptGoogleLogin = () => {
    return async dispatch => {
        dispatch(toggleLoader(true));

        try {
            // Get the users ID token
            const { idToken } = await GoogleSignin.signIn();

            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);

            // Sign-in the user with the credential
            auth().signInWithCredential(googleCredential)
                .catch(err => {
                    console.log(err);
                });
        } catch (err) {console.error(err)};

        // var provider = new firebase.auth.GoogleAuthProvider();
        // // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');

        // auth().signInWithPopup(provider).then(function (result) {
        //     // This gives you a Google Access Token. You can use it to access the Google API.
        //     var token = result.credential.accessToken;
        //     // The signed-in user info.
        //     var user = result.user;

        //     console.log(user.displayName);
        //     dispatch(toggleLoginForm(false));
        //     dispatch(toggleRegisterForm(false));
        //     // ...
        // }).catch(function (error) {
        //     // Handle Errors here.
        //     var errorCode = error.code;
        //     var errorMessage = error.message;
        //     // The email of the user's account used.
        //     var email = error.email;
        //     // The firebase.auth.AuthCredential type that was used.
        //     var credential = error.credential;
        //     // ...
        // });
    }
}

export const attemptFacebookLogin = () => {
    return async dispatch => {

        // try {
        //     // Attempt login with permissions
        //     const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
        // } catch (err) {
        //     console.error(err);
        //     return;
        // };
        //
        // if (result.isCancelled) {
        //     throw 'User cancelled the login process';
        // }
        //
        // // Once signed in, get the users AccesToken
        // const data = await AccessToken.getCurrentAccessToken();
        //
        // if (!data) {
        //     throw 'Something went wrong obtaining access token';
        // }
        //
        // // Create a Firebase credential with the AccessToken
        // const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
        //
        // // Sign-in the user with the credential
        // return auth().signInWithCredential(facebookCredential);
    }
}

export const attemptTwitterLogin = () => {
    return dispatch => {
        var provider = new firebase.auth.TwitterAuthProvider();

        firebase.auth().signInWithPopup(provider).then(function (result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            dispatch(toggleLoginForm(false));
            dispatch(toggleRegisterForm(false));
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }
}

// export const attemptPhoneLogin = (uid, setErrors) => {
//   return dispatch => {
//     dispatch(toggleLoader(true));
//
//     axios.post(getApiUrl('/api/admin/auth/login'), {
//       id_token: uid
//     })
//       .then(res => {
//         storeToken(res.data.data.token).then(() => {
//           dispatch(checkAuth());
//           dispatch(toggleLoader(false));
//         });
//       })
//       .catch(er => {
//         console.log(er);
//       })
//   }
// };

export const attemptRegister = (fieldValues, setErrors) => {
    return dispatch => {

        console.log("testing register")

        dispatch(toggleLoader(true))

        auth().createUserWithEmailAndPassword(fieldValues.email, fieldValues.password)
            .then(userCreds => {
                userCreds.user.sendEmailVerification();

                userCreds.user.updateProfile({
                    displayName: fieldValues.name
                }).then(function () {
                    // Update successful.
                    dispatch(toggleLoader(false));
                    dispatch(toggleRegisterForm(false));
                }, function (error) {
                    dispatch(toggleLoader(false))
                    // An error happened.
                });
            })
            .catch(function (error) {
                // dispatch(toggleLoader(false));
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;

                if (errorCode === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                    setErrors({
                        email: 'That email address is already in use!'
                    });
                }

                if (errorCode === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                    setErrors({
                        email: 'That email address is invalid!'
                    });
                }

                if (errorCode === 'auth/network-request-failed') {
                    console.log('auth/network-request-failed!');
                    // setErrors({
                    //     email: 'That email address is invalid!'
                    // });

                    dispatch(showError("Please check your internet connection"));

                    return;
                }

                if (errorCode == 'auth/weak-password') {
                    setErrors({
                        password: 'The password is too weak.'
                    });
                } else {
                    setErrors({
                        email: errorMessage
                    });
                }
            });
    }
}

export const sendResetPasswordEmail = (email, setErrors) => {
    return dispatch => {
        firebase.auth().sendPasswordResetEmail(email).then(function() {
            dispatch(toggleForgotForm(false));
            dispatch(showSuccess("Password reset mail sent successfully!"));
        }).catch(function(error) {
            setErrors({
                email: error.message
            })
        });
    }
}

export const saveIntendedUrl = url => ({type: AUTH_TYPES.SAVE_INTENDED_URL, url});

export const setPassword = (password, successCb, errorCb) => {
    return dispatch => {
        var user = firebase.auth().currentUser;

        user.updatePassword(password).then(function() {
            successCb()
        }).catch(function(error) {
        // An error happened.
            errorCb(error.message);
        });
    }
}

export const setName = (name, successCb, errorCb) => {
    return dispatch => {
        var user = firebase.auth().currentUser;

        user.updateProfile({
            displayName: name
        }).then(function() {
            // Update successful.
            successCb();
        }).catch(function(error) {
            // An error happened.
        });
    }
}
