import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: 'AIzaSyB3bNgSl0yqLa7lm_xJ4eO7iXMSn4l6ymY',
  authDomain: 'kisaanwebandappdevelopment.firebaseapp.com',
  databaseURL: 'https://kisaanwebandappdevelopment.firebaseio.com',
  projectId: 'kisaanwebandappdevelopment',
  storageBucket: 'kisaanwebandappdevelopment.appspot.com',
  messagingSenderId: '254759501147',
  appId: '1:254759501147:web:98ace10b90a17cd58dee7e',
  measurementId: 'G-4EMDPYPXWP',
};

// Initialize Firebase
// firebase.initializeApp(firebaseConfig);

export const firebaseInit = () => {
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
};

export default firebase;
