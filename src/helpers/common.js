export const formatPrice = (price, prefix = "$", suffix = false) => {
    price = parseInt(price);
    return (prefix ? (prefix + " ") : "") + price.toFixed(2) + (suffix ? ("" + suffix) : "");
}