// export const apiUrl = 'http://localhost:8080';
//export const apiUrl = "http://192.168.43.85:8080";
 export const apiUrl = "http://kisaanwebandappdevelopment.appspot.com";
//
export const getApiUrl = (path) => `${apiUrl}/api/${path}`;

export const getAppUrl = (path) => `${apiUrl}/${path}`;
