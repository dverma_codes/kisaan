/* eslint-disable react-native/no-inline-styles */
import React from 'react';
//import {firebase} from '@react-native-firebase/auth';
import {Block, Text} from '../config';
import * as themes from '../stack/theme';
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  Platform, KeyboardAvoidingView,
} from "react-native";

import ForgotPasswordContainer from '../containers/Auth/ForgotPassword';
import Imagecom from '../components/Imagecom';
//import asyncStr from '../config/Asyncstr';
import {Formik} from 'formik';
import CustomInput from '../components/CustomInput';
import * as Yup from 'yup';
import { KeyboardAwareView } from "react-native-keyboard-aware-view";

const validationSchema = Yup.object().shape({
  name: Yup.string().required('Please enter name'),
  email: Yup.string()
    .email('Email is not valid')
    .required('Please enter email'),
  password: Yup.string()
    .min(8, 'Password must be of 8 characters')
    .required('Please enter password'),
});

class ForgotPassword extends React.Component {
  componentDidMount() {
    /* StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor(themes.colors.white);*/
  }

  render() {
    return (
      <KeyboardAvoidingView
        contentContainerStyle={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'position' : 'position'}
        style={{flex: 1, backgroundColor: themes.colors.white}}>
        <View style={{flex: 1, backgroundColor: themes.colors.white}}>
        <SafeAreaView style={{flex: 1}}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              backgroundColor: themes.colors.white,
            }}>
            <View
              style={{
                flex: Platform.OS === 'ios' ? 1 : 2,
                backgroundColor: themes.colors.white,
                flexDirection: 'row',
              }}>
              <View style={{flex: 2}}>
                <View style={{alignSelf: 'center'}}>
                  <Imagecom
                    type={'kisaan_logo'}
                    left={10}
                    top={Platform.OS === 'ios' ? 10 : 45}
                    height={80}
                    width={120}
                  />
                </View>
              </View>
              <View style={{flex: 1, backgroundColor: themes.colors.white}}>
                <Imagecom type={'right_circle'} />
              </View>
            </View>
          </View>

          <View style={{flex: 0.5}} />
          <View style={{flex: 4, backgroundColor: themes.colors.white}}>
            <Imagecom
              type={'forgot_password'}
              left={width / 2 - width * 0.3}
              height={hieghtcircle * 1.8}
              width={widthcircle * 1.5}
            />
          </View>
          <View style={{flex: 0.5}} />
          <View style={{flex: Platform.OS === 'ios' ? 4.5 : 4}}>
            <Block
              card
              shadow
              color="white"
              flex={4}
              style={{
                flex: 4,
                flexDirection: 'column',
                justifyContent: 'space-around',
                paddingTop: 0,
                zIndex: 1,
                width: width - (2 * width) / 20,

                marginLeft: width / 20,
                marginRight: width / 20,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 0,
                },
                shadowOpacity: 0.5,
                shadowRadius: 12.35,
                elevation: Platform.OS === 'ios' ? 10000 : 8,
              }}>
              <View
                style={{
                  flex: 1,
                  marginLeft: 20,
                  marginRight: 20,
                  marginTop: 20,
                  backgroundColor: themes.colors.white,
                  flexDirection: 'column',
                }}>
                <View style={{flex: 0.5}}>
                  <Text
                    style={{fontSize: Platform.OS === 'ios' ? 20 : 30}}
                    textcolor>
                    Forgot- Your- Password
                  </Text>
                </View>
                <View style={{flex: 0.3, alignContent: 'center', marginTop: 5}}>
                  <Text
                    style={{fontSize: Platform.OS === 'ios' ? 8 : 12}}
                    textcolor>
                    {' '}
                    Enter your email to let us send you a password reset link!
                  </Text>
                </View>
                <View style={{flex: 2, marginTop: 0}}>
                  <Formik
                    initialValues={{
                      email: '',
                      password: '',
                      name: '',
                    }}
                    onSubmit={(values, {setErrors}) =>
                      this.props.actions.attemptRegister(values, setErrors)
                    }
                    validationSchema={validationSchema}>
                    {(props) => {
                      const {handleSubmit} = props;
                      return (
                        <>
                          <CustomInput
                            stylepro={{flex: 1, fontFamily: 'Poppins-Regular'}}
                            title={'Email'}
                            placeholder={'Email'}
                            name={'email'}
                            icon={require('../assets/common/mail.png')}
                            {...props}
                          />
                          <View style={{flex: 1, marginTop: 10}}>
                            <TouchableOpacity
                              onPress={handleSubmit}
                              style={[
                                styles.button,
                                {
                                  backgroundColor: themes.colors.secondary,
                                  borderColor: themes.colors.white,
                                  borderWidth: 0,
                                  borderRadius: 0,
                                  marginTop: 0,
                                },
                              ]}>
                              <Text
                                style={{
                                  color: 'white',
                                  fontFamily: 'Poppins-Light',
                                  fontSize: width / 20,
                                }}>
                                CONTINUE
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </>
                      );
                    }}
                  </Formik>
                </View>
              </View>
            </Block>
            <View style={{flex: 2.5}} />
          </View>
        </SafeAreaView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const {width, height} = Dimensions.get('screen');
const hieghtcircle = height * 0.2;

const widthcircle = width * 0.4;
const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  container: {},

  tittle: {
    color: themes.colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
  text: {
    color: 'grey',
  },
  section: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  textinput: {
    flex: 1,
    paddingLeft: 30,
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ForgotPasswordContainer(ForgotPassword);
