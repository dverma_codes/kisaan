import React from 'react';
import Subscribe from '../components/Subscribe';
import * as themes from '../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Block, Text} from '../config';
import Imagecom from '../components/Imagecom';
import Banner from '../components/Banner';
import ContactusImage from '../assets/common/ContactUs.png';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
const Contactus = () => {
  return (
    <View style={{flex: 1, backgroundColor: themes.colors.white}}>
      <KeyboardAvoidingView
        behavior={Platform.OS=== 'ios'? 'padding': null }
        enabled
        style={{flexGrow: 1, height: '100%', flex:1}}>
        <ScrollView bounces={false}
          style={{flex: 1, backgroundColor: themes.colors.white}}>
          <View
            style={{
              flex: 1,
              height: hp('15'),
              width: wp('100%'),
              flexDirection: 'column',
              backgroundColor: themes.colors.white,
            }}>
            <View
              style={{
                flex: 1,
                marginTop: Platform.OS === 'ios' ? hp('5') : 0,
                flexDirection: 'row',
              }}>
              <View style={{flex: 2}}>
                <View style={{alignSelf: 'center'}}>
                  <Imagecom
                    type={'kisaan_logo'}
                    left={10}
                    top={Platform.OS === 'ios' ? 10 : 45}
                    height={80}
                    width={120}
                  />
                </View>
              </View>
              <View style={{flex: 1, backgroundColor: themes.colors.white}}>
                <Imagecom type={'right_circle'} />
              </View>
            </View>
          </View>
          <View style={{flex: 1, marginTop: 30, marginLeft: 20}}>
            <Text
              style={{fontSize: Platform.OS === 'ios' ? 25 : wp('100%') / 15}}>
              Contact Us
            </Text>
          </View>
          <View style={{margin: 20}}>
            <Banner Case={'small_banner'} imageSource={ContactusImage} />
          </View>
          <View
            style={{
              height: hp('70%'),
              backgroundColor: themes.colors.accent,
              flexDirection: 'column',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text textcolor style={{alignSelf: 'center' , fontSize:Platform.OS==='ios' ? 20 :24}}>How can we help ?</Text>
              <Text style={{ alignSelf:'center', margin:10 ,marginBottom:0, fontSize:Platform.OS==='ios' ? 8 : 10}}>we are in the business around 2001 ,  and have worked
                on a varitty range of projects across
                many industries </Text>
            </View>
            <View style={{flex: 1 ,marginTop:10}}>
              <Text style={{alignSelf:'center' , fontSize:Platform.OS==='ios' ? 12 :15}}>
                We'd love to discusshow we can help you.
              </Text></View>
            <View style={{flex: 1}} />

            <View style={{flex: 1}} />
            <View style={{flex: 1}} />
            <View style={{flex: 1}} />
            <View style={{flex: 1}} />
            <View style={{flex: 1}} />
          </View>

          <View
            style={{
              height: hp('45%'),
              backgroundColor: themes.colors.white,
              flexDirection: 'row',
              flexWrap: 'wrap',
              margin:30,
              marginTop:30,
            }}>
            <View style={{height:hp('20%'),width:wp('40%') , backgroundColor:themes.colors.textcolor ,
            borderTopLeftRadius:30 , borderBottomRightRadius:30}}>

            </View>

            <View style={{height:hp('20%'),width:wp('40%') , backgroundColor:themes.colors.textcolor ,
              borderTopLeftRadius:30 , borderBottomRightRadius:30 , marginLeft:20 , }}>

            </View>

            <View style={{height:hp('20%'),width:wp('40%') , backgroundColor:themes.colors.textcolor ,
              borderTopLeftRadius:30 , borderBottomRightRadius:30 ,marginTop:30 }}>

            </View>

            <View style={{height:hp('20%'),width:wp('40%') , backgroundColor:themes.colors.textcolor ,
              borderTopLeftRadius:30 , borderBottomRightRadius:30 , marginLeft:20 , marginTop:30}}>

            </View>
          </View>
          <Subscribe />
          <View
            style={{
              backgroundColor: themes.colors.accent,
              height: hp('2%'),
            }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};
export default Contactus;
