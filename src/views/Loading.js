import React, {useState} from 'react';
//import {firebase} from '@react-native-firebase/auth';
import Mystatusbar from '../components/mystatusbar';

import * as themes from '../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
} from 'react-native';

class Loading extends React.Component {
  componentDidMount() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor(themes.colors.white);
  }

  render() {
    return (
      <View style={{opacity: this.props.loading ? 0.0 : 0.0}}>
        <Mystatusbar />
        <SafeAreaView style={styles.container}>
          <View>

            <ActivityIndicator size="large" color="#0000ff"/>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}
const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 100,
  },
  tittle: {
    color: themes.colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
  text: {
    color: 'grey',
  },
  section: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  textinput: {
    flex: 1,
    paddingLeft: 30,
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Loading;
