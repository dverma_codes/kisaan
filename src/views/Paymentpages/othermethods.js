import React, {useState} from 'react';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
} from 'react-native';
import {Text} from '../../config';
import Mystatusbar from '../../components/mystatusbar';
import Header from '../../components/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {CheckBox} from 'react-native-elements';
import VISA from '../../assets/common/visa.png';
export default function Othermethods(props) {
  const [checked, Setchecked] = useState([
    {
      title: 'UPI ID',
      Check: false,
    },
    {
      title: 'ADD DEBIT/ CREDIT CARD/ ATM CARD',
      Check: false,
    },
    {
      title: props.emi ? 'EMI AVAILABLE' : 'EMI UNAVAILABLE',
      Check: false,
    },
    {
      title: 'Pay on Delivery',
      Check: false,
    },
  ]);
  const checkboxHandler = (value, index) => {
    const newValue = checked.map((checkbox, i) => {
      if (i !== index) {
        return {
          ...checkbox,
          check: false,
        };
      }
      if (i === index) {
        const item = {
          ...checkbox,
          check: !checkbox.check,
        };
        return item;
      }
      return checkbox;
    });
    Setchecked(newValue);
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: themes.colors.accent,
        marginLeft: 20,
        marginRight: 20,
      }}>
      {
        // map is based on props be base te kar lio me sirf check karan lyi is nu static array le leya si props pass kar lio
      }
      {checked.map((opt, i) => (
        <View
          style={{flex: 1, marginTop: 5, backgroundColor: themes.colors.white}}>
          <CheckBox
            containerStyle={{
              backgroundColor: themes.colors.white,
              borderWidth: 0,
              marginRight: wp('5%'),

            }}
            title={opt.title}
            titleProps={{selectionColor:themes.colors.secondary}}
            key={i}
            textStyle={{
              fontSize: Platform.OS === 'ios' ? 15 : wp('100%') / 25,
              fontWeight:'bold',
            }}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checkedColor={themes.colors.primary}
            checked={opt.check}
            onPress={() => {
              checkboxHandler(opt.check, i);
            }}
          />
          {opt.check && opt.title === 'UPI ID' ? (
            <View
              style={{height: hp('11%'), margin: 10, flexDirection: 'column'}}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 1, marginLeft: 40}}>
                  <TextInput
                    style={{
                      borderColor: themes.colors.gray,
                      flex: 1,
                      backgroundColor: themes.colors.white,
                      borderWidth: 2,
                      fontSize: Platform.OS === 'ios' ? 12 : wp('100%') / 30,
                    }}
                    placeholderTextColor={themes.colors.gray}
                    keyboardType={'name-phone-pad'}
                    maxLength={3}
                    placeholder={'ENTER YOUR UPI ID@bank'}
                  />
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  marginTop: 20,
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    marginLeft: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: themes.colors.primary,
                    borderWidth: 0.1,
                    borderRadius: 5,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      alignContent: 'center',
                      alignSelf: 'center',
                    }}>
                    <Text
                      white
                      style={{
                        borderColor: themes.colors.gray,
                        flex: 1,
                        alignItems:'center',
                        alignContent:'center',
                        justifyContent: 'center',
                        marginTop:5,
                        height:50,
                        fontSize: Platform.OS === 'ios' ? 15 : wp('100%') / 20,
                      }}>
                      PAY NOW
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </View>
      ))}
    </View>
  );
}
