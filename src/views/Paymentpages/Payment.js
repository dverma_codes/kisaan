import React from 'react';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
} from 'react-native';
import {Text} from '../../config';
import Mystatusbar from '../../components/mystatusbar';
import Header from '../../components/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Paymentcard from "./paymentcard";
import Othermethods from "./othermethods";
const Payment = ({navigation, ...props}) => {
  return (
    <View style={{flex: 1, backgroundColor: themes.colors.white}}>
      <SafeAreaView style={{backgroundColor: themes.colors.white}}>
        <ScrollView
          scrollEventThrottle={16}
          style={{backgroundColor: themes.colors.white}}>
          <Mystatusbar />
          <Header
            widhheader={wp('100%')}
            heightheader={hp('10%')}
            navigation={navigation}
          />
          <View style={{backgroundColor: themes.colors.textcolor, height: 3}} />
          <View
            style={{
              flex: 1,
              backgroundColor: themes.colors.accent,
              width: wp('100%'),
            }}>
            <View style={{margin: 10}}>
              <Text
                textcolor
                style={{
                  fontSize: Platform.OS === 'ios' ? 25 : wp('100%') / 12,
                  marginLeft: 10,
                }}>
                Select a Payment method
              </Text>
            </View>
          <Paymentcard />
            <View style={{margin: 10}}>
              <Text
                textcolor
                style={{
                  fontSize: Platform.OS === 'ios' ? 25 : wp('100%') / 12,
                  marginLeft: 10,
                }}>
                Select a Payment method
              </Text>
            </View>
        <Othermethods />

            <View style={{flex:1 , margin:20 , justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>navigation.navigate('TrackOrder')}
                style={{flex:1 , backgroundColor:themes.colors.textcolor,height:50 ,marginTop:20}}>
                <Text white style={{fontSize:Platform.OS === 'ios' ? 18 : wp('100%')/20 , alignItems:'center'
                , marginTop:13,justifyContent:'center', alignSelf:'center' ,fontWeight:'bold'}}>PLACE ORDER</Text>
              </TouchableOpacity>
            </View>

            <View style={{height: 40}} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};
export default Payment;
