import React, {useState} from 'react';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
} from 'react-native';
import {Text} from '../../config';
import Mystatusbar from '../../components/mystatusbar';
import Header from '../../components/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {CheckBox} from 'react-native-elements';
import VISA from '../../assets/common/visa.png';
export default function Paymentcard(props) {
  const [checked, Setchecked] = useState([
    {
      title: 'props i with height checlkdswa fhdliksa fghu',
      Check: false,
    },
    {
      title: 'Open Orders',
      Check: false,
    },
    {
      title: 'Cancelled orders',
      Check: false,
    },
    {
      title: 'Subsription Cancelled orders',
      Check: false,
    },
  ]);
  const checkboxHandler = (value, index) => {
    const newValue = checked.map((checkbox, i) => {
      if (i !== index) {
        return {
          ...checkbox,
          check: false,
        };
      }
      if (i === index) {
        const item = {
          ...checkbox,
          check: !checkbox.check,
        };
        return item;
      }
      return checkbox;
    });
    Setchecked(newValue);
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: themes.colors.accent,
        marginLeft: 20,
        marginRight: 20,
      }}>
      {
        // map is based on props be base te kar lio me sirf check karan lyi is nu static array le leya si props pass kar lio
      }
      {checked.map((opt, i) => (
        <View
          style={{flex: 1, marginTop: 5, backgroundColor: themes.colors.white}}>
          <View>
            <Text
              style={{
                marginLeft: 50,
                marginTop: 10,
                fontWeight: 'bold',
                fontSize: Platform.OS === 'ios' ? 22 : wp('100%') / 14,
              }}>
              BANK NAME {props.name}
            </Text>
          </View>
          <View style={{margin: 1, marginLeft: 50, flexDirection: 'column'}}>
            <Image source={VISA} style={{height: 10, width: 30}} />
          </View>
          <CheckBox
            containerStyle={{
              backgroundColor: themes.colors.white,
              borderWidth: 0,
              marginRight: wp('30%'),
            }}
            title={
              <View style={{flex: 1, flexDirection: 'column', marginLeft: 10}}>
                <View style={{flex: 1}}>
                  {
                    //opt should be used in place of props jus t for ref
                  }
                  <Text
                    style={{
                      fontSize: Platform.OS === 'ios' ? 13 : wp('100%') / 20,
                    }}>
                    NAME PERSON{props.name}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <Text
                    style={{
                      fontSize: Platform.OS === 'ios' ? 13 : wp('100%') / 20,
                    }}>
                    Expires {props.expiredate}
                  </Text>
                </View>
              </View>
            }
            key={i}
            textStyle={{
              fontSize: Platform.OS === 'ios' ? 10 : wp('100%') / 30,
              fontColor: themes.colors.textcolor,
            }}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checkedColor={themes.colors.primary}
            checked={opt.check}
            onPress={() => {
              checkboxHandler(opt.check, i);
            }}
          />
          {opt.check ? (
            <View
              style={{height: hp('11%'), margin: 10, flexDirection: 'column'}}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 0.5 , marginLeft:40}} >
                  <TextInput
                  style={{borderColor:themes.colors.gray ,flex:1 , backgroundColor:themes.colors.white , borderWidth:2 , fontSize:Platform.OS==='ios' ? 12 : wp('100%')/30 }}
                  placeholderTextColor={themes.colors.gray}
                  keyboardType={'number-pad'}
                  maxLength={3}
                  placeholder={'ENTER CVV'}></TextInput>
                </View>
                <View style={{flex: 1.5}}>
                  <TouchableOpacity
                    onPress={null}
                    style={{
                      flex: 1,

                      marginTop: 10,
                      justifyContent: 'center',
                    }}>
                    <Text
                      textcolor
                      style={{
                        marginLeft:10,
                        alignSelf: 'flex-start',
                        fontSize: Platform.OS === 'ios' ? 15 : wp('100%') / 23,
                      }}>
                      What is CVV?
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{flex: 1, flexDirection: 'column' , marginTop:20}}>
                <View style={{flex: 1   , marginLeft:40,justifyContent:'center'}} >
                 <Text  style={{borderColor:themes.colors.gray ,flex:1 ,
                   backgroundColor:themes.colors.white
                 , fontSize:Platform.OS==='ios' ? 10 : wp('100%')/30 }}>i  This Card is recommended for You</Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={null}
                    style={{
                      flex: 1,

                      marginTop: 5,
                      justifyContent: 'center',
                    }}>
                    <Text
                      textcolor
                      style={{
                        marginLeft:40,
                        fontSize: Platform.OS === 'ios' ? 12 : wp('100%') / 23,
                      }}>
                      Why ?
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : null}
        </View>
      ))}
    </View>
  );
}
