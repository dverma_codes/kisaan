import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Checkavail from '../../components/checkavail';
import SearchBar from '../../components/Searchbar';
import Mystatusbar from '../../components/mystatusbar';
import {Text} from '../../config';
import Addresscom from './addresscom';
const Address = ({navigation, ...props}) => {
  const [address, Setaddress] = useState(['address']);
  return (
    <View style={{flex: 1, backgroundColor: themes.colors.white}}>
      <SafeAreaView style={{backgroundColor: themes.colors.white}}>
        <ScrollView
          scrollEventThrottle={16}
          style={{backgroundColor: themes.colors.white}}>
          <Mystatusbar />
          <Header
            widhheader={wp('100%')}
            heightheader={hp('10%')}
            navigation={navigation}
          />
          <SearchBar back={true} navigation={navigation} />
          <View
            style={{
              flex: 1,
              backgroundColor: themes.colors.accent,
              width: wp('100%'),
            }}>
            <View style={{margin: 10}}>
              <Text
                textcolor
                style={{
                  fontSize: Platform.OS === 'ios' ? 25 : wp('100%') / 12,
                  marginLeft: 10,
                }}>
                Select a delivery address
              </Text>
            </View>
          </View>
          {address && address.length ? (
            <View>
              <View
                style={{
                  width: wp('100%'),
                  backgroundColor: themes.colors.accent,
                }}>
                <View style={{margin: 10}}>
                  <Addresscom name={'hello'} />
                </View>
                <TouchableOpacity>
                  <View
                    style={{
                      marginLeft: 20,
                      marginRight: 20,
                      flexDirection: 'row',
                      backgroundColor: themes.colors.white,
                      height: hp('5%'),
                    }}>
                    <View
                      style={{
                        flex: 3,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      <Text
                        style={{fontWeight:'bold',
                          fontSize: Platform.OS === 'ios' ? 15 : width / 20,
                        }}>
                        Add a New Address
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignSelf: 'center',
                        alignItems: 'flex-end',
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS === 'ios' ? 30 : width / 8,
                        }}>
                        {' '}
                        >{' '}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View
                    style={{
                      marginLeft: 20,
                      marginRight: 20,
                      marginTop: 3,
                      flexDirection: 'row',
                      backgroundColor: themes.colors.white,
                      height: hp('5%'),
                    }}>
                    <View
                      style={{
                        flex: 3,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      <Text
                        style={{fontWeight:'bold',
                          fontSize: Platform.OS === 'ios' ? 15 : width / 20,
                        }}>
                        Deliver to multiple addresses
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignSelf: 'center',
                        alignItems: 'flex-end',
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS === 'ios' ? 30 : width / 8,
                        }}>
                        {' '}
                        >{' '}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={{marginTop:20}}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Payment')}
                    style={[
                      styles.button,
                      {
                        marginLeft: 20,
                        backgroundColor: themes.colors.primary,
                        borderColor: themes.colors.white,
                        borderRadius: 5,
                        alignItems: 'center',
                        alignContent: 'center',
                        justifyContent: 'center',
                      },
                    ]}>
                    <Text
                      white
                      style={{
                        fontSize: Platform.OS === 'ios' ? 15 : 20,
                        alignSelf: 'center',
                      }}>
                      CONTINUE
                    </Text>
                  </TouchableOpacity>
                </View>
                  <View style={{height: 50, width: width}} />

              </View>
            </View>
          ) : (
            <View>
              <View style={{margin: 25}}>
                <Text
                  textcolor
                  style={{
                    alignSelf: 'center',
                    fontSize: Platform.OS === 'ios' ? 15 : wp('100%') / 15,
                  }}>
                  PLEASE ADD A ADDRESS
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => navigation.navigate('Address')}
                style={[
                  styles.button,
                  {
                    marginLeft: 20,
                    backgroundColor: themes.colors.primary,
                    borderColor: themes.colors.white,
                    borderRadius: 5,
                    alignItems: 'center',
                    alignContent: 'center',
                    justifyContent: 'center',
                  },
                ]}>
                <Text
                  white
                  style={{
                    fontSize: Platform.OS === 'ios' ? 15 : 20,
                    alignSelf: 'center',
                  }}>
                  ADD NEW DELIVERY ADDRESS
                </Text>
              </TouchableOpacity>
              <View style={{height: 30, width: width}} />
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};
const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.9;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themes.colors.primary,
    flexDirection: 'column',
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default Address;
