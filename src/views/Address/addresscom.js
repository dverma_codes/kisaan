import React, {useState} from 'react';
import * as themes from '../../stack/theme';
import { Platform, TouchableOpacity, View } from "react-native";
import {CheckBox} from 'react-native-elements';
import {Text} from '../../config';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default function Addresscom(props) {
  const [checked, Setchecked] = useState([
    {
      title: 'props i with height checlkdswa fhdliksa fghu',
      Check: false,
    },
    {
      title: 'Open Orders',
      Check: false,
    },
    {
      title: 'Cancelled orders',
      Check: false,
    },
    {
      title: 'Subsription Cancelled orders',
      Check: false,
    },
  ]);
  const checkboxHandler = (value, index) => {
    const newValue = checked.map((checkbox, i) => {
      if (i !== index) {
        return {
          ...checkbox,
          check: false,
        };
      }
      if (i === index) {
        const item = {
          ...checkbox,
          check: !checkbox.check,
        };
        return item;
      }
      return checkbox;
    });
    Setchecked(newValue);
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: themes.colors.accent,
        marginLeft: 10,
        marginRight: 10,
      }}>
      {
        // map is based on props be base te kar lio me sirf check karan lyi is nu static array le leya si props pass kar lio
      }
      {checked.map((opt, i) => (
        <View
          style={{flex: 1, marginTop: 5, backgroundColor: themes.colors.white}}>
          <View>
            <Text
              style={{
                marginLeft: 50,
                marginTop: 10,
                fontWeight:'bold',
                fontSize: Platform.OS === 'ios' ? 22 : wp('100%') / 14,
              }}>
              check {props.name}
            </Text>
          </View>
          <CheckBox
            containerStyle={{
              backgroundColor: themes.colors.white,
              borderWidth: 0,
              marginRight: wp('30%'),
            }}
            title={opt.title}
            key={i}
            textStyle={{
              fontSize: Platform.OS === 'ios' ? 10 : wp('100%') / 30,
              fontColor: themes.colors.textcolor,
            }}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checkedColor={themes.colors.primary}
            checked={opt.check}
            onPress={() => {
              checkboxHandler(opt.check, i);
            }}
          />
          {opt.check ? (
            <View
              style={{height: hp('10%'), margin: 10, flexDirection: 'column'}}>
              <TouchableOpacity
                onPress={null}
                style={{
                  flex: 1,
                  backgroundColor: themes.colors.accent,
                  marginTop: 10,
                  borderWidth:2,
                  borderColor:themes.colors.pricedetails,
                  justifyContent:'center'
                }}>
                <View style={{justifyContent:'center'}}>
                  <Text textcolor style={{alignSelf:'center' , fontSize:Platform.OS==='ios' ? 15 : wp('100%') / 23}}>Edit Address</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={null}
                style={{
                  flex: 1,
                  backgroundColor: themes.colors.accent,
                  marginTop: 10,
                  borderWidth:2,
                  borderColor:themes.colors.pricedetails,
                  justifyContent:'center'
                }}>
                <View style={{justifyContent:'center'}}>
                  <Text textcolor style={{alignSelf:'center' ,
                    fontSize:Platform.OS==='ios' ? 15 : wp('100%') / 23}}>Add delivery instructions</Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      ))}
    </View>
  );
}
