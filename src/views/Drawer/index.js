import * as React from 'react';
import {Button, View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Custom from './custom';
import tab from '../Tabnav/Tabstack';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    // <NavigationContainer>
    <Drawer.Navigator drawerContent={(props) => <Custom {...props} />}>
      <Drawer.Screen name="Home" component={tab} />
    
    </Drawer.Navigator>
    // </NavigationContainer>
  );
}
