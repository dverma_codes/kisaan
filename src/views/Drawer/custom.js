/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet} from 'react-native';

import * as themes from '../../stack/theme';
// eslint-disable-next-line no-unused-vars
// import {Block, Text} from '../../config';

import {Avatar, Title, Caption, Drawer} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';

// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import IconFA from 'react-native-vector-icons/FontAwesome5';
// import IconEn from 'react-native-vector-icons/Entypo';
// import {TouchableOpacity} from 'react-native-gesture-handler';
// import {ThemeColors} from 'react-navigation';
// import {ThemeConsumer} from 'styled-components';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import CustomContainer from '../../containers/Drawer/Custom';

function Custom({auth, actions, ...props}) {

  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <Avatar.Image
              style={{marginTop: heightPercentageToDP('2%')}}
              source={{
                uri: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
              }}
              size={heightPercentageToDP('6%')}
            />
            <View
              style={{
                marginLeft: 25,
                flexDirection: 'column',
                marginTop: heightPercentageToDP('2%'),
              }}>
              <Title style={styles.title}>User</Title>
            </View>
          </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          <DrawerItem label="Home" onPress={() => {props.navigation.navigate("DrawerHome")}} />
          <DrawerItem label="Shop by Category" onPress={() => {}} />
          <DrawerItem label="Today Deal's" onPress={() => {}} />
        </Drawer.Section>
        <Drawer.Section style={styles.drawerSection}>
          {!auth.user && <DrawerItem label="Sign in" onPress={() => { props.navigation.navigate("Auth", { screen: 'LoginWithScreen' }) }} />}
          {!auth.user && <DrawerItem label="Register" onPress={() => { props.navigation.navigate("Auth", { screen: 'RegisterScreen' }) }} />}
          <DrawerItem label="My account" onPress={() => { props.navigation.navigate("MyAccount") }} />
          <DrawerItem label="Shop by Category" onPress={() => {props.navigation.navigate("Category") }} />
          <DrawerItem label="Yours Order" onPress={() => {props.navigation.navigate("Orders")}} />
          <DrawerItem label="Track order" onPress={() => {}} />
          <DrawerItem label="Blog" onPress={() => {}} />
          <DrawerItem label="Contact us" onPress={() => {props.navigation.navigate("Contact")}} />
          <DrawerItem label="Help Desk" onPress={() => {}} />
          <DrawerItem label="Cart" onPress={() => {props.navigation.navigate("Cart")}} />
          {auth.user && <DrawerItem label="Sign Out" onPress={() => { actions.logout() }} />}
        </Drawer.Section>
        <Drawer.Section style={styles.drawerSection}>
          {/* <DrawerItem label="Language" onPress={() => {}} /> */}
          <DrawerItem label="Your Notification" onPress={() => {}} />
          <DrawerItem label="Settings" onPress={() => {}} />
          <DrawerItem label="Customer Services" onPress={() => {}} />
        </Drawer.Section>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
    flexDirection: 'row',
    backgroundColor: themes.colors.primary,
    height: heightPercentageToDP('10%'),
  },
  title: {
    fontSize: 20,
    marginTop: heightPercentageToDP('2%'),
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Black',
    color: themes.colors.white,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
    borderBottomColor: themes.colors.primary,
    borderBottomWidth: 1,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: themes.colors.primary,
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

export default CustomContainer(Custom);
