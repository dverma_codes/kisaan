import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import Cat from '../../components/Cat';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Checkavail from '../../components/checkavail';
import SearchBar from '../../components/Searchbar';
import {FlatGrid} from 'react-native-super-grid';
import Mystatusbar from '../../components/mystatusbar';

export default function Item(props) {
  producthandle = () => {
    props.navigation.navigate('Product', {Productid: props.Productid});
  };
  return (
    // <View style={{ height: 20,
    //     marginTop: 0,
    //     backgroundColor: 'white',

    // }}>
    // <TouchableOpacity
    // style={{backgroundColor:"#f00f",
    // borderWidth:1,

    // flex:1}}
    // onPress={producthandle}>
    // <Image source={{uri: props.productimage}}

    //   />
    //  </TouchableOpacity>
    // </View>
    <TouchableOpacity onPress={producthandle}>
      <View
        style={{
          height: hp('8%'),
          width: wp('18%'),
          backgroundColor: themes.colors.white,
          marginLeft: 10,
          marginTop: 9,
        }}>
        <View style={{flex: 1}}>
          <Image
            source={{uri: props.image}}
            style={{
              flex: 2,
              width: null,
              height: hp('5'),
              resizeMode: 'stretch',
            }}
          />
          {/* <Text style={{flex: 1, fontSize: 12, marginLeft: 42}}>
              {props.productid}
            </Text> */}
        </View>
      </View>
    </TouchableOpacity>
  );
}
