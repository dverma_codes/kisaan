import React from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Text} from '../../config';
import search from '../../assets/android/search.png';
export default function OderSearch() {
  return (
    <View
      style={{
        backgroundColor: themes.colors.white,
        flexDirection: 'row',
        marginTop: 5,
        borderRadius: 3,
        borderWidth: 1,
        width: '100%',
        marginRight: 5,
        borderColor: themes.colors.white,
      }}>
      <TouchableOpacity
        style={{
          height: 40,
          width: 40,
          resizeMode: 'stretch',
          alignSelf: 'center',
          borderRadius: 3,
          backgroundColor: themes.colors.accent,
        }}
        onPress={null}>
        <Image source={search} style={styles.imageStyle} />
      </TouchableOpacity>
      <TextInput
        style={{
          flex: 1,
          fontFamily: 'Poppins-Regular',
          fontSize: 12,
          paddingLeft: 15,
          height: 40,
          borderRadius: 3,
          width: '60%',
          backgroundColor: themes.colors.accent,
        }}
        placeholder={'Search for Orders'}
        placeholderTextColor={themes.colors.gray}
        underlineColorAndroid="transparent"
        onChangeText={null}
      />
      <View>
        <TouchableOpacity
          style={{
            width: 120,
            height: 40,
            backgroundColor: themes.colors.textcolor,
            borderWidth: 1,
            borderRadius: 0,
            borderColor: themes.colors.secondary,
          }}
          onPress={null}>
          <Text
            white
            bold
            style={{
              marginLeft: 10,
              marginRight: 10,
              marginTop: 10,
              marginBottom: 10,
              fontSize: 15,
            }}>
            Search orders
          </Text>
        </TouchableOpacity>
      </View>
      {
        // if touch button  search add 'TouchableOpacity' to image
      }
    </View>
  );
}

const styles = StyleSheet.create({
  imageStyle: {
    height: 15,
    width: 15,
    alignSelf: 'center',
    marginTop: 10,

    resizeMode: 'stretch',
  },
});
