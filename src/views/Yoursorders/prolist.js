import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import Cat from '../../components/Cat';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Checkavail from '../../components/checkavail';
import SearchBar from '../../components/Searchbar';
import {FlatGrid} from 'react-native-super-grid';
import Mystatusbar from '../../components/mystatusbar';
import Item from './item';

export default function ProComList(props) {
  return (
    <View style={{marginTop: 20}}>
      <View
        style={{
          backgroundColor: themes.colors.accent,
          height: hp('5%'),
          flexDirection: 'row',
        }}>
        <View style={{flex: 2, marginTop: 5}}>
          <View
            style={{
              flex: 1,
              marginLeft: 5,
              flexDirection: 'column',
              justifyContent: 'center',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                ORDER PLACED
              </Text>
            </View>
            <View style={{flex: 1, justifyContent: 'flex-start'}}>
              <Text
                gray
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                22 March 2021{props.date}
              </Text>
            </View>
          </View>
        </View>
        <View style={{flex: 1, marginTop: 5, justifyContent: 'center'}}>
          <View
            style={{
              flex: 1,
              marginLeft: 5,
              flexDirection: 'column',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                TOTAL
              </Text>
            </View>

            <View style={{flex: 1, justifyContent: 'flex-start'}}>
              <Text
                gray
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                $8.89
              </Text>
            </View>
          </View>
        </View>
        <View style={{flex: 2, justifyContent: 'center', marginTop: 5}}>
          <View
            style={{
              marginLeft: 5,
              flex: 1,
              flexDirection: 'column',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                SHIP TO
              </Text>
            </View>
            <View style={{flex: 1, justifyContent: 'flex-start'}}>
              <Text
                gray
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                Jasspreet Kaur{props.date}
              </Text>
            </View>
          </View>
        </View>
        <View style={{flex: 3, justifyContent: 'center', marginTop: 5}}>
          <View
            style={{
              flex: 1,
              marginLeft: 5,
              flexDirection: 'column',
            }}>
            <View style={{flex: 1, marginRight: 5, justifyContent: 'center'}}>
              <Text
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                ORDER #402-1234567-1234567{props.orderid}
              </Text>
            </View>
            <View style={{flex: 1, justifyContent: 'flex-start'}}>
              <Text
                gray
                style={{fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40}}>
                22 March 2021{props.date}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View style={{height: hp('30%') , borderWidth:1.5 , borderColor:themes.colors.accent}}>
        <View style={{flexDirection: 'row', flex: 1, marginLeft: 5}}>
          <View
            style={{
              flex: 3.5,

              flexDirection: 'column',
            }}>
            <View style={{flex: 0.5}} />
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  flex: 1.4,
                  fontSize: Platform.OS === 'ios' ? 12 : wp('100%') / 28,
                }}>
                Arrival text
              </Text>
              <Text
                gray
                style={{
                  flex: 1,
                  fontSize: Platform.OS == 'ios' ? 9 : wp('100') / 40,
                }}>
                Not yet displached
              </Text>
            </View>
            <View style={{flex: 3, flexDirection: 'row'}}>
              <View style={{flex: 2}}>
                <Image
                  source={{uri: props.image}}
                  style={{
                    flex: 1,
                    margin: 2,
                      backgroundColor:themes.colors.brown,
                  }}
                />
              </View>
              <View style={{flex: 1}} />
            </View>
            <View
              style={{
                flex: 1,

                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: Platform.OS === 'ios' ? 11 : wp('100%') / 30,
                  marginRight: 15,
                }}>
                Product name long text long a very long text dsf kjjh kj sdhfkss
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',

                flexDirection: 'column',
              }}>
              <View style={{flex: 1, marginTop: 3, flexDirection: 'row'}}>
                <Text
                  gray
                  style={{
                    fontSize: Platform.OS === 'ios' ? 9 : wp('100%') / 40,
                  }}>
                  Sold By{' '}
                </Text>
                <TouchableOpacity>
                  <Text
                    gray
                    style={{
                      fontSize: Platform.OS === 'ios' ? 9 : wp('100%') / 40,
                    }}>
                    {';'} abc{props.shopname}
                  </Text>
                </TouchableOpacity>
                <Text
                  gray
                  style={{
                    fontSize: Platform.OS === 'ios' ? 9 : wp('100%') / 40,
                  }}>
                  {'  |  '} Product questions ?
                </Text>
                <TouchableOpacity>
                  <Text
                    style={{
                      fontSize: Platform.OS === 'ios' ? 9 : wp('100%') / 40,
                    }}>
                    Ask Seller ?
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 2}}>
                <Text
                  style={{
                    fontSize: Platform.OS === 'ios' ? 12 : wp('100%') / 25,
                  }}>
                  $price dollar
                </Text>
              </View>
            </View>
            <View style={{flex: 0.5}} />
          </View>

          <View style={{flex: 2, marginLeft: 2}}>
              <View style={{flex:1 , marginRight:10 , flexDirection:'column'}}>
                  <View style={{flex:0.5}}/>
                  <View style={{flex:1 , backgroundColor:themes.colors.textcolor , justifyContent:'center'}}>
                    <TouchableOpacity
                      onPress={null}
                    style={{flex:1 , justifyContent:'center'}}><Text white style={{fontSize:Platform.OS==='ios' ? 13 : wp('100%') / 25,
                          alignSelf:'center'}}>Order Details</Text></TouchableOpacity>
                  </View>
                  <View style={{flex:0.4}}/>
                  <View style={{flex:1 , backgroundColor:themes.colors.accent ,justifyContent:'center'}}>
                      <TouchableOpacity
                        onPress={null}
                        style={{flex:1 , justifyContent:'center'}}><Text style={{fontSize:Platform.OS==='ios' ? 13 : wp('100%') / 25,
                          alignSelf:'center'}}>Track Package</Text></TouchableOpacity>
                  </View>
                  <View style={{flex:0.4}}/>
                  <View style={{flex:2 , justifyContent:'center' , backgroundColor:themes.colors.accent}}>

                          <TouchableOpacity
                            style={{flex:1 , justifyContent:'center' , marginLeft:Platform.OS==='ios' ?0 : 20}}>
                              <Text style={{fontSize:Platform.OS==='ios' ? 13 : wp('100%') / 25, alignSelf:'center'}}>View  Related Subscription</Text></TouchableOpacity>

                  </View>
                  <View style={{flex:0.4}}/>
                  <View style={{flex:1 , backgroundColor:themes.colors.accent , justifyContent:'center'}}>
                      <TouchableOpacity
                        style={{flex:1 , justifyContent:'center' }}>
                      <Text style={{fontSize:Platform.OS==='ios' ? 13 : wp('100%') / 25, alignSelf:'center'}}>Cancel Order</Text></TouchableOpacity>
                  </View>
                  <View style={{flex:0.4}}/>
                  <View style={{flex:1}}/>


              </View>
          </View>
        </View>
      </View>
    </View>
  );
}
