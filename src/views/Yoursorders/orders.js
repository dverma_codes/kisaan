import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import Cat from '../../components/Cat';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import {DrawerNavigator} from 'react-navigation';
import Checkavail from '../../components/checkavail';
import SearchBar from '../../components/Searchbar';
import {FlatGrid} from 'react-native-super-grid';
import Mystatusbar from '../../components/mystatusbar';
import Item from './item';
import OderSearch from './odersearchcom';
import ProComList from "./prolist";
const submitData = () => {
  console.warn('button pressed');
  alert('hiiiii');
};
export default function Oders({route, props}) {
  const [products, Setproducts] = useState([
    {
      productid: 1,
      productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
      name: 'milk',
    },
    {
      productid: 2,
      productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
      name: 'milk2',
    },
    {
      productid: 4,
      productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
      name: 'milk4',
    },
    {
      productid: 3,
      productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
      name: 'milk3',
    },
  ]);

  const productobj = route.params;
  const navigation = useNavigation();

  function producthandle() {
    navigation.navigate('Filter');
  }
  function Buyhandle() {
    navigation.navigate('BuyAgain');
  }
  return (
    <View style={{flex: 1, backgroundColor: themes.colors.white}}>
      <SafeAreaView style={{backgroundColor: themes.colors.white}}>
        <ScrollView scrollEventThrottle={16}>
          <Mystatusbar />
          <Header
            widhheader={wp('100%')}
            heightheader={hp('10%')}
            navigation={navigation}
          />

          <SearchBar back={true} navigation={navigation} />

          <View style={[styles.container, {}]}>
            <View
              style={{
                flex: 2,
                flexDirection: 'row',
                backgroundColor: themes.colors.white,
              }}>
              <Text textcolor style={{flex: 3, fontSize: 35, marginTop: 10}}>
                Your Orders
              </Text>
              {/* <View style={{flex:1,}}>
        <TouchableOpacity style={{height:"70%",
        width:'95%',
        marginTop:10,
        backgroundColor:themes.colors.secondary,
        borderWidth:1,
        borderRadius: 0,
        borderColor:themes.colors.secondary}}>
        <Text white bold style={{marginLeft:5,marginTop:15,fontSize:15}}>
        Search orders</Text>
        </TouchableOpacity>
        </View>                              */}
            </View>
            <OderSearch />
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 30,
                backgroundColor: themes.colors.accent,
              }}>
              <TouchableOpacity
                style={{flex: 2, flexDirection: 'column'}}
                onPress={Buyhandle}>
                <Text
                  style={{
                    flex: 1,
                    fontSize: 22,
                    marginTop: 10,
                    marginLeft: 10,
                  }}>
                  Buy Again
                </Text>
                <Text
                  grey
                  style={{
                    flex: 1,
                    marginTop: 10,
                    marginBottom: 20,
                    marginLeft: 10,
                  }}>
                  See More
                </Text>
              </TouchableOpacity>
              <View style={{flex: 4}}>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {products.map((product) => (
                    <Item
                      image={
                        // 'https://www.atmeplay.com/images/users/avtar/avtar.png'
                        product.productimage
                      }
                      productid={product.productid}
                      navigation={navigation}
                    />
                  ))}
                </ScrollView>
              </View>
            </View>

            <View
              style={{
                marginTop: 40,
                flex: 1,
                flexDirection: 'row',
                backgroundColor: themes.colors.accent,
              }}>
              <Text
                style={{
                  flex: 6,
                  fontSize: 22,
                  marginTop: 10,
                  marginBottom: 10,
                  marginLeft: 10,
                }}>
                Last Month
              </Text>
              <View style={{flex: 3}}>
                <TouchableOpacity
                  style={{
                    backgroundColor: themes.colors.textcolor,
                    height: hp('3.5'),

                    position: 'relative',
                    borderRadius: 5,
                    marginTop: 10,
                    marginRight: 10,
                  }}
                  onPress={producthandle}>
                  <Text
                    white
                    style={{marginLeft: 4, fontSize:Platform.OS==='ios' ? 15 :wp('100%')/22, marginTop: 3}}>
                    Filter Orders {'>>'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <ProComList />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themes.colors.white,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10,
  },
});
