
import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Checkavail from '../../components/checkavail';


