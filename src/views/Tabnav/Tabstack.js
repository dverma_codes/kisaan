import React from 'react';
import * as themes from '../../stack/theme';
import {Image, StyleSheet} from 'react-native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Home from '../Home';
import Cart from '../cart/cartmain';
import Oder from './Oder';
import Wallet from './Wallet';
import Help from './Help';
const Tab = createMaterialBottomTabNavigator();

function Tabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      shifting={true}
      labeled={true}
      sceneAnimationEnabled={false}
      inactiveColor="#95a5a6"
      barStyle={{backgroundColor:themes.colors.accent }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.imageStyle}
              source={require('../../assets/android/home.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name="cart"
        component={Cart}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.imageStyle}
              source={require('../../assets/android/tomorrow.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Oder"
        component={Oder}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.imageStyle}
              source={require('../../assets/android/oder.png')}
            />
          ),
        }}
      />
      {/* <Tab.Screen
        name="wallet"
        component={Wallet}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.imageStyle}
              source={require('../../assets/android/wallet.png')}
            />
          ),
        }}
      /> */}
      {
        //change in new design of home page cart image is not there....
      }
      <Tab.Screen
        name="Help"
        component={Help}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.imageStyle}
              source={require('../../assets/android/help.png')}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  imageStyle: {
    padding: 10,
    height: hp('3%'),
    width: wp('6%'),
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});

export default Tabs;
