import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
} from 'react-native';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Searchbar from '../../components/Searchbar';
import Banner from '../../components/Banner';
import Cat from '../../components/Cat';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Product from '../../components/Product';
import Checkavail from '../../components/checkavail';
import {FlatGrid} from 'react-native-super-grid';
import HomeContainer from '../../containers/Home/Home';
import {formatPrice} from '../../helpers/common';

export default class Catcompro extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('Product', {
            product_id: this.props.product_id,
          })
        }>
        {/* // this.props.productid */}
        <View
          style={{
            height: 100,
            width: width / 2 - 20,
            backgroundColor: themes.colors.white,
            marginTop: 5,
            marginLeft: 10,
            borderColor: '#000',
            borderWidth: 1,
            borderRadius: 5,
            flexDirection:'row',
          }}>
          <Image source={{uri: this.props.imageUri}}
          style={{flex:1, height:80, width: 120 , resizeMode:'center', alignSelf:'center',}}/>
          <View style={{flex:2 , alignItems:'center', alignSelf:'center',alignContent:"center",marginTop:10}}>
          <Text >{this.props.name}</Text>
        </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const {width, height} = Dimensions.get('screen');
