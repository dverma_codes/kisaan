import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
    StyleSheet,
    TextInput,
    View,
    ScrollView,
    Image,
    Keyboard,
    Dimensions,
    TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    SafeAreaView,
    StatusBar,
    Button,
} from 'react-native';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Searchbar from '../../components/Searchbar';
import Banner from '../../components/Banner';
import Cat from '../../components/Cat';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Catcompro from './catcompro';

export default class catcom extends React.Component {
    state = {
        isshow: false,
    }

    plusminusshow() {
        if (this.state.isshow) {
            return (
                <AntDesign name='minus' color={themes.colors.primary} size={22}/>
            )
        } else {
            return (
                <AntDesign name='plus' color={themes.colors.primary} size={22}/>
            )
        }
    }

    renderview() {
        if (this.state.isshow) {
            return (
                <View style={{
                    width: width - 20,
                    margin: 10,
                    marginTop: 0,
                    backgroundColor: themes.colors.white,
                    borderBottomWidth: 1,
                    borderBottomColor: '#959a9c'
                }}>
                    {/* <ScrollView
                  showsButtons={false}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                console.log(product)
                    return (
                      <Catcompro
                        Productid={product.productid}
                        imageUri={product.productimage}
                        name={product.name}
                        navigation={this.props.navigation}
                      />
                    );
                  })}
                </ScrollView> */}
                    <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                        {this.props.products.map((product) => {
                            return (
                                <View style={{marginBottom: 10}} key={product.productid}>
                                    <Catcompro
                                        product_id={product.productid}
                                        imageUri={product.productimage}
                                        name={product.name}
                                        navigation={this.props.navigation}
                                    />
                                </View>
                            );
                        })}
                    </View>

                </View>


            )
        }
    }

    toggleCancel() {
        this.setState({
            isshow: !this.state.isshow,
        });

    }

    render() {
        return (
            <View>
                <View style={{
                    backgroundColor: themes.colors.white,
                    borderBottomWidth: 1,
                    borderBottomColor: '#959a9c',
                    width: width - 20,
                    height: hp('8%'),
                    margin: 10,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <Text primary style={{margin: hp('2.2%'), flex: 7, fontSize: width / 15}}>{this.props.title}
                        {this.props.products.Productid}</Text>
                    <TouchableOpacity
                        style={{
                            margin: hp('1%'),
                            flex: 0.5,
                            borderWidth: 1,
                            borderColor: themes.colors.primary,
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            this.setState({
                                isshow: !this.state.isshow,
                            });
                        }}
                    >
                        {this.plusminusshow()}
                    </TouchableOpacity>


                </View>
                {this.renderview()}
            </View>
        );
    }
}

const {width, height} = Dimensions.get('screen');
