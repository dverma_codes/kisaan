/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
    StyleSheet,
    View,
    ScrollView,
    Dimensions,
    SafeAreaView
} from 'react-native';
import Header from '../../components/Header';
import Searchbar from '../../components/Searchbar';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Checkavail from '../../components/checkavail';
import Catcom from './catcom';
import Mystatusbar from '../../components/mystatusbar'
import BanneR from '../../components/Banner';
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import * as ProductActions from '../../redux/actions/products';
import CategoryImage from '../../assets/common/Category-Page.jpg';
import _ from 'lodash';

class Shopbycategory extends React.Component {
    state = {
        isshow: false,
        categories: [
            // {
            //     title: 'Cream milk',
            //     products: [
            //         {
            //             productid: 1,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk'
            //         },
            //         {
            //             productid: 2,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk2'
            //         },
            //         {
            //             productid: 3,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk3'
            //         }
            //     ]
            // },
            // {
            //     title: 'Egg less biscuit',
            //     products: [
            //         {
            //             productid: 1,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk'
            //         },
            //         {
            //             productid: 2,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk2'
            //         },
            //         {
            //             productid: 3,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk3'
            //         }
            //     ]
            // },
            // {
            //     title: 'sweets',
            //     products: [
            //         {
            //             productid: 1,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk'
            //         },
            //         {
            //             productid: 2,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk2'
            //         },
            //         {
            //             productid: 3,
            //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            //             name: 'milk3'
            //         }
            //     ]
            // },

        ]
    }

    componentDidMount() {
        //   StatusBar.setBarStyle('dark-content', true);
        //   StatusBar.setBackgroundColor(themes.colors.white);
        // }

        // {
        //     title: 'sweets',
        //     products: [
        //         {
        //             productid: 1,
        //             productimage: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
        //             name: 'milk'
        //         },
        // }

        this.props.actions.getCategoriesWithProducts()
            .then(res => {
                this.setState({
                    categories: res.categories.map(category => ({
                        title: category.name,
                        products: category.products.map(product => {

                            const variantData = _.get(product.variants, "[0]");

                            const images = variantData.images
                                .sort(function (a, b) {
                                    return a.isDefault - b.isDefault;
                                })
                                .map(function (image) {
                                    return {
                                        original: image.url,
                                        thumbnail: image.url,
                                    };
                                });

                            // console.log(_.get(images, "[0].thumbnail"));

                            return {
                                productid: product.id,
                                productimage: _.get(images, "[0].thumbnail"),
                                name: product.name
                            }
                        })
                    }))
                })
            })
            .catch(err => console.error(err));
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <SafeAreaView style={{backgroundColor: themes.colors.white}}>
                    <ScrollView scrollEventThrottle={16} style={{backgroundColor: themes.colors.white}}>
                        <Mystatusbar/>
                        <Header
                            widhheader={wp('100%')}
                            heightheader={hp('10%')}
                            navigation={this.props.navigation}
                        />
                        <Searchbar
                            back={true}/>
                        <Checkavail/>
                        {/* <View style={{backgroundColor:themes.colors.white,borderBottomWidth:1,borderBottomColor:'#959a9c',width:width-20,height:hp('8%'), margin:10,flexDirection:'row',alignItems:'center'}}>
            <Text primary style={{margin:hp('2.2%'),flex:7, fontSize:width/15}}>hello 1</Text>
            <TouchableOpacity
            style={{margin:hp('1%'),flex:0.5, borderWidth:1,borderColor:themes.colors.primary,alignItems:'center'}}
            onPress={()=>{this.setState({
            isshow:!this.state.isshow,
            });}}
            >
             {this.plusminusshow()}
            </TouchableOpacity>


            </View>
            {this.renderview()} */}
                        {this.state.categories.map((categories) => {
                            return (
                                <Catcom
                                    title={categories.title}
                                    products={categories.products}
                                    navigation={this.props.navigation}
                                    key={categories.id}
                                />
                            )
                        })}

                        <View style={{marginTop: 20}}>
                            <BanneR
                                Case="large_banner"
                                imageSource={CategoryImage}
                                // imageUri={'https://www.atmeplay.com/images/users/avtar/avtar.png'}
                            />
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.8;
const styles = StyleSheet.create({
    tittle: {
        color: themes.colors.primary,
        fontWeight: 'bold',
        fontSize: 30,
    },
    text: {
        color: 'grey',
    },
    section: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        marginTop: 10,
    },
    textinput: {
        flex: 1,
        paddingLeft: 30,
    },
    button: {
        width: widthbutton,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});


//plusminusshow()
//   {
//     if(this.state.isshow)
//     {
//       return(
//       <AntDesign name='minus' color={themes.colors.primary} size ={22}/>
//       )
//     }
//     else{
//       return(
//         <AntDesign name='plus' color={themes.colors.primary} size ={22}/>
//         )
//     }
//   }
// renderview()
// {
//   if(this.state.isshow)
//   {
//     return(
//       <View style={{height:140,
//           width:width-20,
//           margin:10,
//           marginTop:0,
//           backgroundColor:themes.colors.white,
//           borderBottomWidth:1,
//           borderBottomColor:'#959a9c'}}><Text>hello checsdkfhsdkjhfjkasgfd</Text></View>
//     )
//   }
// }
// toggleCancel() {
//      this.setState({
//        isshow:!this.state.isshow,
//    });

// }

const mapStateToProps = (state, ownProps) => {
    return ({})
}

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        getCategoriesWithProducts: ProductActions.getCategoriesWithProducts,
    }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Shopbycategory)