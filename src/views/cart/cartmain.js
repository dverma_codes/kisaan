import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Checkavail from '../../components/checkavail';
import SearchBar from '../../components/Searchbar';
import Mystatusbar from '../../components/mystatusbar';
import Subtotal from './subtotal';
import Datecart from './dateincart';
import Product_cart from './product_cart';
import PriceDetails from './priceDetails';
import {Text} from '../../config';
import {useNavigation} from '@react-navigation/native';
export default function Cartmain({route, navigation}) {
  const productobj = route.params;

  const [pro, Setpro] = useState(['pro1']);
  return (
    <View style={{flex: 1}}>
      <SafeAreaView style={{backgroundColor: themes.colors.white}}>
        <ScrollView scrollEventThrottle={16}>
          <Mystatusbar />
          <Header
            widhheader={wp('100%')}
            heightheader={hp('10%')}
            navigation={navigation}
          />

          <SearchBar back={true} navigation={navigation} />
          <Checkavail navigation={navigation} />
          {
            // props to check which one to render
          }
          {
            // uncomment this use condition to return view based on the view
          }
          {pro && pro.length ? (
            <>
              <Subtotal />
              <Datecart />
              <Product_cart
                imagesrc={
                  'https://www.atmeplay.com/images/users/avtar/avtar.png'
                }
              />
              <Product_cart />
              <Product_cart />
              <PriceDetails />
              <View style={{flex: 1}}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Address')}
                  style={[
                    styles.button,
                    {
                      marginLeft: 20,
                      backgroundColor: themes.colors.primary,
                      borderColor: themes.colors.white,
                      borderRadius: 5,
                      alignItems: 'center',
                      alignContent: 'center',
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    white
                    style={{
                      fontSize: Platform.OS === 'ios' ? 15 : 20,
                      alignSelf: 'center',
                    }}>
                    PROCEED TO BUY{' '}
                  </Text>
                </TouchableOpacity>
                <View style={{height: 30, width: width}} />
              </View>
            </>
          ) : (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
              }}>
              <View
                style={{
                  height: hp('10%'),
                  width: wp('100%'),
                  justifyContent: 'center',
                }}>
                <Text
                  textcolor
                  style={{
                    fontSize: Platform.OS === 'ios' ? 25 : 30,
                    alignSelf: 'center',
                  }}>
                  No Product in cart
                </Text>
              </View>
              <View
                style={{
                  height: hp('10%'),
                  width: wp('100%'),
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={[
                    styles.button,
                    {
                      marginLeft: 20,
                      backgroundColor: themes.colors.primary,
                      borderColor: themes.colors.white,
                      borderRadius: 5,
                      alignItems: 'center',
                      alignContent: 'center',
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    white
                    style={{
                      fontSize: Platform.OS === 'ios' ? 15 : 20,
                      alignSelf: 'center',
                    }}>
                    Shop Now{' '}
                  </Text>
                </TouchableOpacity>
                <View style={{height: 30, width: width}} />
              </View>
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

function subtotal(product) {
  const total = 0;
  foreach((product) => {
    total += product.price;
  });
}
const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.9;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themes.colors.primary,
    flexDirection: 'column',
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
