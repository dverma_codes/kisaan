import React, { useState } from "react";
import Icontrash from 'react-native-vector-icons/Ionicons'
import {Block, Text} from '../../config';
import NumericInput from 'react-native-numeric-input';
import * as themes from '../../stack/theme';
import calandericon from '../../assets/common/calender.png';
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Platform, Image,
} from 'react-native';
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Product_cart = (props) => (
 <View
    style={{
      borderWidth: 1,
      borderColor: themes.colors.gray,
      height: hp(Platform.OS === 'ios' ? '25%' : '30%'),
      width: wp('100%'),
      backgroundColor: themes.colors.primary,
      flex: 1,
      flexDirection: 'row',
    }}>
    <View
      style={{
        flex: 2,
        backgroundColor: themes.colors.white,
        flexDirection: 'column',
      }}>
      <View style={{flex: 4, }} >
        <Image source={{uri: props.imagesrc}}
        style={{flex:0.8 , margin:20}}/>
      </View>
      <View style={{flex:0.7 ,marginLeft:20 , flexDirection:'row'}} >
        <View style={{flex:1}}></View>
        <TouchableOpacity
          style={{flex:2 , height:25,width:25,borderWidth:1, borderColor:themes.colors.brown , alignContent:'center' , justifyContent:'center'}}
        onPress={null}>
          < Icontrash name={'trash-outline'} color={themes.colors.brown} size={20} style={{alignItems:'flex-end',alignSelf:'center'  }}/>
        </TouchableOpacity>
        <View style={{flex:8}}/>
      </View>
      <View style={{ flex: 0.3, }}/>
    </View>
    <View
      style={{
        flex: 3,
        backgroundColor: themes.colors.white,
        flexDirection: 'column',
      }}><View style={{flex:1 , marginTop:10 , marginBottom:10 , marginLeft:5}}>
        <View style={{flex: 2, backgroundColor: themes.colors.white , alignContent:'center',justifyContent:'center' }}>
          <Text style={{fontSize:Platform.OS ==='ios' ? 12 : 15, alignContent:'center'}}> {props.productname}Name</Text>
        </View>
      <View style={{flex: 1, justifyContent:'center'}} >
        <Text>$46 {props.product}</Text>
      </View>
      <View style={{flex: 1, backgroundColor: themes.colors.white, justifyContent:'center'}} >
        <Text>{props.eligible ? 'Eligible for Free Shiping' : 'Not Eligible for free shiping'}
        </Text>
      </View>
      <View style={{flex: 3,  flexDirection:'row'}}>
        <View style={{flex: 1.5, flexDirection:'column'}} >
          <View style={{flex:1 , justifyContent:'center'}}><Text>Quantity</Text>
          </View>
          <View style={{flex:2 , justifyContent:'center'}}>
            <NumericInput
              value={null}
              type="up-down"
              onChange={null}
              onLimitReached={(true, 'can not empty')}
              totalWidth={50}
              rounded={true}
              totalHeight={30}
              iconSize={40}
              step={1}
              valueType="integer"
              textColor="#000000"
              iconStyle={{color: '#000000'}}
              minValue={1}
              upDownButtonsBackgroundColor={themes.colors.whiteback}
              containerStyle={{backgroundColor:themes.colors.whiteback}}
              onLimitReached={() => console.log('limit reached!')}
            />
          </View>
        </View>

        <View style={{flex: 3.5, }} >
          <View style={{flex:1 , justifyContent:'center'}}><Text>Weight</Text>
          </View>
          <View style={{flex:2 , justifyContent:'center'}}>
            <NumericInput
              value={null}
              type="up-down"
              onChange={null}
              onLimitReached={(true, 'can not empty')}
              totalWidth={wp('35%')}
              rounded={true}
              totalHeight={30}
              iconSize={40}
              step={1}
              valueType="integer"
              textColor="#000000"
              iconStyle={{color: '#000000'}}
              minValue={1}
              upDownButtonsBackgroundColor={themes.colors.whiteback}
              containerStyle={{backgroundColor:themes.colors.whiteback}}
              onLimitReached={() => console.log('limit reached!')}
            />
          </View>
        </View>

      </View>
      <View style={{flex: 2,flexDirection: 'row' , justifyContent:'center'}} >

          <View style={{flex: 1, justifyContent:'center'}}>
            <Image
              style={{height: 20, width: 20, alignSelf:'center'}}
              source={(calandericon)}
            />
          </View>
          <View style={{flex: 6 , justifyContent:'center',}}>
            <Text style={{fontSize: Platform.OS==='ios' ? 10:12 , marginLeft:5,}}>
              Delivery Date: 31 oct 2020
            </Text>
          </View>
        <View style={{flex:3 , justifyContent:'center' ,height:20 , backgroundColor:themes.colors.brown , marginTop:15 , }} >
          <TouchableOpacity
          onPress={null}><Text white style={{fontSize:Platform.OS==='ios' ? 8 : wp('100%')/50 ,
            alignSelf:'center',marginLeft:0}}>Change Date</Text></TouchableOpacity>
        </View>
        <View style={{flex:1}} />
        </View>
      </View>
    </View>
  </View>
);

export default Product_cart;
/*
      <View Style={{flex: 1, backgroundColor: themes.colors.primary}} />
      <View Style={{flex: 1, backgroundColor: themes.colors.black}} />
      <View Style={{flex: 1, backgroundColor: themes.colors.black}} />
      <View Style={{flex: 1}} />
      <View Style={{flex: 1}} />
      <View Style={{flex: 1}} />
      <View Style={{flex: 1}} />
      */
