import React from 'react';
import {Block, Text} from '../../config';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  Dimensions,
  TouchableOpacity, Platform,
} from "react-native";
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Subtotal = (props) => {
  return (
    <View
      style={{
        height: hp('15%'),
        width: wp('100%'),
        backgroundColor: themes.colors.accent,
      }}>
      <View style={{flex: 1, flexDirection: 'column', margin: 20}}>
        <View style={{flex: 1}}>
          <Text textcolor style={{fontSize:Platform.OS === 'ios' ? 15 : 20}}>Subtotal ({props.itemcount} items) : ${props.priceTwithoutax}</Text>
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={[
              styles.button,
              {
                backgroundColor: themes.colors.primary,
                borderColor: themes.colors.white,
                borderRadius:5,
                alignItems:'center',
                alignContent:'center',
                justifyContent:'center',
              },
            ]}
          >
            <Text white style={{fontSize:Platform.OS==='ios' ? 15 : 20 , alignSelf:'center'}}>PROCEED TO BUY </Text>
          </TouchableOpacity>

        </View>
      </View>
    </View>
  );
};
const {width, height} = Dimensions.get('screen');
const hieghtcircle = height * 0.2;

const widthcircle = width * 0.4;
const widthbutton = width * 0.9;
const styles = StyleSheet.create({
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default Subtotal;
