import React from 'react';
import {Block, Text} from '../../config';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const PriceDetails =(props)=>
  (
    <View style={{height:hp('45%'), width:wp('100%') , backgroundColor:themes.colors.white, marginTop:10}}>
      <View style={{flex:1, margin:20, backgroundColor:themes.colors.accent, flexDirection:'column',}}>
        <View style={{flex:0.7 , backgroundColor:themes.colors.pricedetails, justifyContent:'center'}}>
        <Text style={{marginLeft:10 , fontSize:Platform.OS==='ios' ? 15 : 20}}>PRICE DETAILS ({props.itemcount}items)</Text>
        </View>
        <View style={{flex:1 , justifyContent:'center', flexDirection:'row' , marginLeft:10}}>
          <View style={{flex:4 , alignSelf:'center',}}>
            <Text style={{ fontSize:Platform.OS==='ios' ? 15:20}}>Total MRP</Text></View>
          <View style={{flex:1, alignSelf:'center',marginRight:20}}>
            <Text style={{fontSize:Platform.OS==='ios' ? 15:20, alignSelf:'flex-end'}}>$ Price</Text></View>
        </View>
        <View style={{flex:1,justifyContent:'center', flexDirection:'row' , marginLeft:10}}>
         <View style={{flex:4 , alignSelf:'center',}}>
           <Text style={{ fontSize:Platform.OS==='ios' ? 15:20}}>Discount on MRP</Text></View>
        <View style={{flex:1, alignSelf:'center',marginRight:20}}>
          <Text style={{fontSize:Platform.OS==='ios' ? 15:20, alignSelf:'flex-end'}}>$ Price</Text></View>
        </View>
        <View style={{flex:1,justifyContent:'center', flexDirection:'row' , marginLeft:10}}>
          <Text style={{flex:Platform.OS==='ios'? 3.8 :3.6 , alignSelf:'center', fontSize:Platform.OS==='ios' ? 15:20}}>Coupon Discount</Text>
          <TouchableOpacity
          style={{flex:2, alignSelf:'center',marginRight:20,}}><Text style={{fontSize:Platform.OS==='ios' ? 15:20, alignSelf:'flex-end'}}>Apply coupon</Text></TouchableOpacity>
        </View>
        <View style={{flex:1, justifyContent:'center', flexDirection:'row' , marginLeft:10}}>
          <View style={{flex:4 , alignSelf:'center',}}>
            <Text style={{ fontSize:Platform.OS==='ios' ? 15:20}}>Shipping Fee</Text></View>
          <View style={{flex:1, alignSelf:'center',marginRight:20}}>
            <Text style={{fontSize:Platform.OS==='ios' ? 15:20, alignSelf:'flex-end'}}>$ Price</Text></View>
        </View>
        <View style={{flex:0.1 , borderColor:themes.colors.gray , borderBottomWidth:2 , marginLeft:20, marginRight:20}}/>
        <View style={{flex:1.2 , justifyContent:'center', flexDirection:'row' , marginLeft:10}}>
          <View style={{flex:4 , alignSelf:'center',}}>
            <Text textcolor style={{ fontSize:Platform.OS==='ios' ? 18:23, fontWeight:'bold'}}>TOTAL AMOUNT</Text></View>
          <View style={{flexz:1, alignSelf:'center',marginRight:20}}>
            <Text textcolor style={{fontSize:Platform.OS==='ios' ? 18:23,fontWeight:'bold', alignSelf:'flex-end'}}>$ Price</Text></View>
        </View>
        </View>
      </View>


  );
export default PriceDetails;
