import React from 'react';
import {Block, Text} from '../../config';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Datecart = (props) => {
  return (
    <View
      style={{
        height: hp('7%'),
        width: wp('100%'),
        backgroundColor: themes.colors.white,
        alignContent:'center',
        alignItems:'center',
        justifyContent:'center',
        borderColor:themes.colors.gray,
        borderWidth:1,
      }}>
      <Text bold style={{fontWeight:'bold', alignSelf:'center',fontSize: Platform.OS === 'ios' ? 20 : 25}}>
        {' '}
        Delivery Date : {props.date} {''} ({props.day})
      </Text>

    </View>
  );
};
export default Datecart;
