/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
//import {firebase} from '@react-native-firebase/auth';
import {Block, Text} from '../config';
import * as themes from '../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
  Linking,
  Platform,
} from 'react-native';

import LoginContainer from '../containers/Auth/Login';
import Imagecom from '../components/Imagecom';
import Imputwithimg from '../components/Inputwithimg';
//import asyncStr from '../config/Asyncstr';
import {Formik} from 'formik';
import CustomInput from '../components/CustomInput';
import * as Yup from 'yup';
import {attemptFacebookLogin} from '../redux/actions/auth';

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email is not valid')
    .required('Please enter email'),
  password: Yup.string()
    .min(8, 'Password must be of 8 characters')
    .required('Please enter password'),
});

class Login extends React.Component {
  componentDidMount() {
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor(themes.colors.white);
  }

  render() {
    const {attemptGoogleLogin} = this.props.actions;

    return (
      <View style={{flex: 1, backgroundColor: themes.colors.white}}>
        <SafeAreaView style={{flex: 1}}>
          <View
            style={{
              flex: 1,
              backgroundColor: themes.colors.white,
              flexDirection: 'column',
            }}>
            <View
              style={{
                flex: Platform.OS === 'ios' ? 1 : 1.5,
                backgroundColor: themes.colors.white,
                flexDirection: 'row',
              }}>
              <View style={{flex: 2}}>
                <View style={{alignSelf: 'center'}}>
                  <Imagecom
                    type={'kisaan_logo'}
                    left={10}
                    top={Platform.OS === 'ios' ? 10 : 45}
                    height={80}
                    width={120}
                  />
                </View>
              </View>
              <View style={{flex: 1, backgroundColor: themes.colors.white}}>
                <Imagecom type={'right_circle'} />
              </View>
            </View>
            <View style={{flex: 3}}>
              <Imagecom
                type={'login_and_account'}
                left={width / 2 - width * 0.3}
                height={hieghtcircle * 1.7}
                width={widthcircle * 1.5}
              />
            </View>

            <View style={{flex: 2.6}}>
              <Block
                card
                shadow
                color="white"
                flex={5}
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                  paddingTop: 0,
                  zIndex: 1,
                  height: height / 3,
                  width: width - 40,

                  marginBottom: 10,
                  marginLeft: 20,
                  marginRight: 20,
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 0,
                  },
                  shadowOpacity: 1,
                  shadowRadius: 5,

                  elevation: Platform.OS === 'ios' ? 100000 : 3,
                }}>
                <View
                  style={{
                    flex: 5,
                    backgroundColor: themes.colors.white,
                    marginLeft: 20,
                    marginRight: 20,
                    flexDirection: 'column',
                  }}>
                  <View style={{flex: 2}}>
                    <Text
                      style={{
                        marginLeft: 0,
                        fontSize:
                          Platform.OS === 'ios' ? width / 15 : width /10,
                      }}
                      textcolor>
                      Login
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Text
                      textcolor
                      bold
                      style={{
                        marginLeft: 0,
                        fontSize: Platform.OS === 'ios' ? 10 : 15,
                      }}>
                      {' '}
                      Please Login With
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      onPress={null}
                      style={{
                        height: height / 19 - 10,
                        width: (width - (2 * width) / 20) / 3.6,
                        marginLeft: 1,
                      }}>
                      <Imagecom
                        type={'custom'}
                        left={0}
                        top={8}
                        height={height / 19 - 10}
                        width={(width - (2 * width) / 20) / 3.6}
                        path={require('../assets/common/twitter.png')}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => attemptFacebookLogin()}
                      style={{
                        height: height / 19 - 10,
                        width: (width - (2 * width) / 20) / 3.6,
                        marginLeft: 10,
                      }}>
                      <Imagecom
                        type={'custom'}
                        left={0}
                        top={8}
                        height={height / 19 - 12}
                        width={(width - (2 * width) / 20) / 3.6}
                        path={require('../assets/common/facebook.png')}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => attemptGoogleLogin()}
                      style={{
                        height: height / 19 - 10,
                        width: (width - (2 * width) / 20) / 3.6,
                        marginLeft: 10,
                      }}>
                      <Imagecom
                        type={'custom'}
                        left={0}
                        top={8}
                        height={height / 19 - 12}
                        width={(width - (2 * width) / 20) / 3.6}
                        path={require('../assets/common/google.png')}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginTop: 20}}>
                    <Text
                      style={{fontSize: Platform.OS === 'ios' ? 10 : 15}}
                      textcolor>
                      OR
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('LoginScreen')
                      }
                      style={[
                        styles.button,
                        {
                          backgroundColor: themes.colors.secondary,
                          borderColor: themes.colors.white,
                          borderWidth: 0,
                          borderRadius: 0,
                        },
                      ]}>
                      <Text
                        bold
                        style={{
                          color: 'white',
                          fontSize: width / 22,
                        }}>
                        CONTINUE WITH EMAIL
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      borderBottomColor: '#e5e5e5',
                      borderBottomWidth: 1,
                      flex: 0.8,
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      marginTop: height / 80,
                      width: widthbutton,
                      height: 30,
                    }}>
                    <Text
                      style={{
                        fontSize:
                          Platform.OS === 'ios' ? width / 50 : width / 40,
                      }}
                      textcolor>
                      By continue you agree to kisaan{' '}
                    </Text>
                    <Text
                      h3
                      style={{
                        fontWeight: 'bold',
                        fontSize:
                          Platform.OS === 'ios' ? width / 50 : width / 40,
                      }}
                      textcolor
                      bold
                      onPress={() =>
                        Linking.openURL('http://linktoterms.html')
                      }>
                      Conditions of use
                    </Text>
                    <Text
                      style={{
                        fontSize:
                          Platform.OS === 'ios' ? width / 50 : width / 40,
                      }}
                      textcolor>
                      {' '}
                      and{' '}
                    </Text>
                    <Text
                      style={{
                        fontSize:
                          Platform.OS === 'ios' ? width / 50 : width / 40,
                        fontWeight: 'bold',
                      }}
                      textcolor
                      bold
                      onPress={() =>
                        Linking.openURL('http://linktoterms.html')
                      }>
                      Privacy Notice
                    </Text>
                  </View>
                  <View style={{flex: 1}} />
                </View>
              </Block>
            </View>

            {/* <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          onSubmit={(values, {setErrors}) =>
            this.props.actions.attemptLogin(
              values.email,
              values.password,
              setErrors,
            )
          }
          validationSchema={validationSchema}>
          {(props) => {
            const {
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
            } = props;

            return (
              <View>
                <CustomInput title={'Email'} name={'email'} {...props} />

                <CustomInput title={'Password'} name={'password'} {...props} />

                <Button onPress={handleSubmit} title="Submit" />
              </View>
            );
          }}
        </Formik> */}

            <View style={{flex: 0.3}} />
          </View>
        </SafeAreaView>
      </View>
    );
  }
}
const {width, height} = Dimensions.get('screen');
const hieghtcircle = height * 0.2;

const widthcircle = width * 0.4;
const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  container: {
    backgroundColor: themes.colors.white,
    justifyContent: 'center',
  },

  tittle: {
    color: themes.colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
  text: {
    color: 'grey',
  },
  section: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  textinput: {
    flex: 1,
    paddingLeft: 30,
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoginContainer(Login);
