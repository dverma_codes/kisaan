/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
import React from 'react';
import Subscribe from '../components/Subscribe';
import * as themes from '../stack/theme';
import _ from 'lodash';
import SplashScreen from 'react-native-splash-screen';
import Orientation from 'react-native-orientation-locker';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {Block, Text} from '../config';
import Header from '../components/Header';
import Searchbar from '../components/Searchbar';
import Banner from '../components/Banner';
import Cat from '../components/Cat';
import Proandtophome from '../components/proandtophome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Product from '../components/Product';
import Checkavail from '../components/checkavail';
import {FlatGrid} from 'react-native-super-grid';
import HomeContainer from '../containers/Home/Home';
import {formatPrice} from '../helpers/common';
import Mystatusbar from '../components/mystatusbar';

import SweetBanner from '../assets/common/sweets-banner.jpg';
import Banner1 from '../assets/common/banner-1.jpg';
import Banner2 from '../assets/common/banner-2.jpg';
import Banner3 from '../assets/common/banner-3.jpg';
import Banner4 from '../assets/common/banner-4.jpg';

import LinearGradient from 'react-native-linear-gradient';
import ScreenOrientation, {
  PORTRAIT,
  LANDSCAPE,
} from 'react-native-orientation-locker/ScreenOrientation';
import {useEffect} from 'react';
import Swiperlast from "../components/swiperlast";

// const statusbarandroid=()=>(
//    <>
//    {
//     StatusBar.setBarStyle('dark-content', true),
//     StatusBar.setBackgroundColor(themes.colors.white)
//    }
//    </>
// );

// const statusbarios=()=>(
//     <StatusBar translucent backgroundColor={themes.colors.accent} />

// )
class Home extends React.Component {
  _onOrientationDidChange = (orientation) => {
    if (orientation != 'PORTRAIT') {
      this.props.navigation.navigate('Notsupported');
    }
  };

  componentDidMount() {
    //     { Platform.OS == 'ios' ?
    //     (statusbarios()) : (statusbarandroid())
    // }
    SplashScreen.hide();
    Orientation.getAutoRotateState((rotationLock) =>
      this.setState({rotationLock}),
    );
    //this allows to check if the system autolock is enabled or not.

    Orientation.lockToPortrait();
    Orientation.addOrientationListener(this._onOrientationDidChange);
    this.props.actions
      .fetchHome()
      .then((res) => {
        this.setState({
          banners: res.data.banners,
          categories: res.data.categories.map((category) => {
            return {
              id: category.id,
              name: category.name,
              image: category.image_data.url,
            };
          }),
          products: res.data.products,
        });
      })
      .catch((err) => {
        console.log('home errr');
        console.error(err.message);
      });
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this._onOrientationDidChange);
  }

  state = {
    banners: [],
    categories: [
     1
      //   image: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
      // },
    ],
    types: [
      {
        case: 'swipper_small',
        image: [
          'https://www.atmeplay.com/images/users/avtar/avtar.png',
          'https://www.atmeplay.com/images/users/avtar/avtar.png',
          'https://www.atmeplay.com/images/users/avtar/avtar.png',
        ],
      },
    ],
    products: [],
  };

  render() {
    return (
      <View style={{flex: 1 , backgroundColor:themes.colors.white}}>
        <SafeAreaView>
          <Mystatusbar />
          <ScrollView scrollEventThrottle={16}>
            <Header
              widhheader={wp('100%')}
              heightheader={hp('10%')}
              navigation={this.props.navigation}
            />
            <Searchbar back={false} />
            <Checkavail navigation={this.props.navigation} />
            <View style={{backgroundColor: 'white'}}>
              <View
                style={{
                  height: 140,
                  marginTop: 10,
                  backgroundColor: 'white',
                  paddingTop: 5,
                  paddingBottom: 10,
                }}>
                <ScrollView
                  showsButtons={true}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {this.state.categories.map((category) => {
                    return (
                      <Cat
                        handleClick={() =>
                          this.props.navigation.navigate('Category')
                        }
                        key={category.id}
                        imageUri={category.image}
                        name={category.name}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            </View>

            <Banner
              Case={'swipper_small'}
              imageUri={this.state.banners.map(
                (banner) => banner.image_data.url,
              )}
            />

            <View
              style={{
                height: hp('43%'),
                width: wp('100%'),
                backgroundColor: themes.colors.white,
                flex: 1,
                flexDirection: 'column',
              }}>
              <Text
                adjustsFontSizeToFit
                style={{
                  marginLeft: 20,
                  marginTop: 20,
                  fontSize: width / 24,
                }}>
                Featured Products
              </Text>
              <ScrollView
                style={{marginTop: 20}}
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {this.state.products.map((product) => {
                  const {name, price, defaultImage} = _.get(
                    product.variants,
                    '[0]',
                    {},
                  );

                  return (
                    <Product
                      cols={themes.colors.accent}
                      imageUri={defaultImage.url}
                      texts={name}
                      StarCount={5}
                      Price={formatPrice(price)}
                      Pricedrop={0}
                      key={product.id}
                    />
                  );
                })}
                {/*<Product*/}
                {/*  cols={themes.colors.accent}*/}
                {/*  imageUri={*/}
                {/*    'https://www.atmeplay.com/images/users/avtar/avtar.png'*/}
                {/*  }*/}
                {/*  texts={'hello hello hello hello hello hello hello hello'}*/}
                {/*  StarCount={5}*/}
                {/*  Price={'135'}*/}
                {/*  Pricedrop={'85'}*/}
                {/*/>*/}
                {/*<Product*/}
                {/*  cols={themes.colors.accent}*/}
                {/*  imageUri={*/}
                {/*    'https://www.atmeplay.com/images/users/avtar/avtar.png'*/}
                {/*  }*/}
                {/*  texts={'hello hello hello hello hello hello hello'}*/}
                {/*  StarCount={5}*/}
                {/*  Price={'35'}*/}
                {/*  Pricedrop={'85'}*/}
                {/*/>*/}
                {/*<Product*/}
                {/*  cols={themes.colors.accent}*/}
                {/*  imageUri={*/}
                {/*    'https://www.atmeplay.com/images/users/avtar/avtar.png'*/}
                {/*  }*/}
                {/*  texts={'hello hello hello hello hello hello hello'}*/}
                {/*  StarCount={5}*/}
                {/*  Price={'35'}*/}
                {/*  Pricedrop={'85'}*/}
                {/*/>*/}
              </ScrollView>
            </View>
            <View
              style={{paddingTop: 25, backgroundColor: themes.colors.white}}>
              <Banner
                Case="large_touch"
                imageSource={SweetBanner}
                // imageUri={
                //     // 'https://www.atmeplay.com/images/users/avtar/avtar.png'
                // }
              />
            </View>
            {
              //product view start
            }
            <View
              style={{
                height: 750,
                width: wp('100%'),
                backgroundColor: themes.colors.white,
                flex: 1,
                flexDirection: 'column',
              }}>
              <Text
                bold
                style={{
                  marginLeft: 20,
                  marginTop: 20,
                  fontSize: 27,
                }}>
                Product
              </Text>
              <View
                style={{
                  height: 640,
                  width: '90%',
                  backgroundColor: themes.colors.accent,
                  marginLeft: 20,
                  marginTop: 20,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}>
                {this.state.products.slice(0, 4).map((product) => {
                  const {name, price, defaultImage} = _.get(
                    product.variants,
                    '[0]',
                    {},
                  );

                  return (
                    <Proandtophome
                      color={themes.colors.white}
                      Starcount={5}
                      defaultImage={defaultImage}
                      name={name}
                      price={price}
                    />
                  );
                  // return <Product
                  //     cols={themes.colors.accent}
                  //     imageUri={defaultImage.url}
                  //     texts={name}
                  //     StarCount={5}
                  //     Price={formatPrice(price)}
                  //     Pricedrop={0}
                  // />;
                })}
                {/*<Proandtophome color={themes.colors.white} Starcount={2} />*/}
                {/*<Proandtophome color={themes.colors.white} />*/}
                {/*<Proandtophome color={themes.colors.white} />*/}
                {/*<Proandtophome color={themes.colors.white} />*/}

                {/* <FlatList
                  data={arrayOfData}
                  horizontal
                  ItemSeparatorComponent={() => (
                    <View style={{width: 16, backgroundColor: 'pink'}} />
                  )}
                  renderItem={({item}) => <Product={item} />}
                /> */}
              </View>
            </View>
            {/* {this.state.products.map((product) => (
              <Product
                cols={themes.colors.accent}
                imageUri={
                  // 'https://www.atmeplay.com/images/users/avtar/avtar.png'
                  product.defaultImage.url
                }
                texts={product.name}
                StarCount={5}
                Price={formatPrice(product.price)}
                // Pricedrop={'85'}
              />
            ))}*/}
            <Subscribe />
            <View
              style={{paddingTop: 25, backgroundColor: themes.colors.white}}>
              <Banner
                handleClick={() => this.props.navigation.navigate('Category')}
                Case="large_touch"
                imageSource={Banner1}
                // imageUri={
                //     'https://www.atmeplay.com/images/users/avtar/avtar.png'
                // }
              />
            </View>
            <View
              style={{paddingTop: 25, backgroundColor: themes.colors.white}}>
              <Banner
                handleClick={() => this.props.navigation.navigate('Category')}
                Case="large_touch"
                imageSource={Banner2}
                // imageUri={
                //     'https://www.atmeplay.com/images/users/avtar/avtar.png'
                // }
              />
            </View>
            <View
              style={{paddingTop: 25, backgroundColor: themes.colors.white}}>
              <Banner
                handleClick={() => this.props.navigation.navigate('Category')}
                Case="large_touch"
                imageSource={Banner3}
                // imageUri={
                //     'https://www.atmeplay.com/images/users/avtar/avtar.png'
                // }
              />
            </View>
            <View
              style={{paddingTop: 25, backgroundColor: themes.colors.white}}>
              <Banner
                handleClick={() => this.props.navigation.navigate('Category')}
                Case="large_touch"
                imageSource={Banner4}
                // imageUri={
                //     'https://www.atmeplay.com/images/users/avtar/avtar.png'
                // }
              />
            </View>
            <View
              style={{
                padding: 20,
                alignItems: 'center',
                backgroundColor: themes.colors.white,
              }}>
              <TouchableOpacity
                style={{width: '35%'}}
                onPress={() => this.props.navigation.navigate('Category')}>
                <LinearGradient
                  start={{x: 1, y: 0}} //here we are defined x as start position
                  end={{x: 0, y: 0}} //here we can define axis but as end position
                  colors={['#0c461b', '#0d7b29']}
                  style={{
                    borderRadius: 25,
                    marginLeft: 10,
                    height: 30,
                    alignItems: 'center',
                    alignContent: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    white
                    style={{
                      alignSelf: 'center',
                      fontSize: Platform.OS === 'ios' ? 15 : 20,
                    }}>
                    Shop Now
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: 750,
                width: wp('100%'),
                backgroundColor: themes.colors.accent,
                flex: 1,
                flexDirection: 'column',
              }}>
              <Text
                bold
                style={{
                  marginLeft: 20,
                  marginTop: 20,
                  fontSize: 27,
                }}>
                Top Selling
              </Text>
              <View
                style={{
                  height: 640,
                  width: '90%',
                  backgroundColor: themes.colors.white,
                  marginLeft: 20,
                  marginTop: 20,
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}>
                {this.state.products.slice(0, 4).map((product) => {
                  const {name, price, defaultImage} = _.get(
                    product.variants,
                    '[0]',
                    {},
                  );

                  return (
                    <Proandtophome
                      color={themes.colors.white}
                      Starcount={5}
                      defaultImage={defaultImage}
                      name={name}
                      price={price}
                    />
                  );
                  // return <Product
                  //     cols={themes.colors.accent}
                  //     imageUri={defaultImage.url}
                  //     texts={name}
                  //     StarCount={5}
                  //     Price={formatPrice(price)}
                  //     Pricedrop={0}
                  // />;
                })}
                {/*<Proandtophome color={themes.colors.accent} Starcount={2} />*/}
                {/*<Proandtophome color={themes.colors.accent} />*/}
                {/*<Proandtophome color={themes.colors.accent} />*/}
                {/*<Proandtophome color={themes.colors.accent} />*/}
              </View>
            </View>
            <View style={{height:hp('25%') , backgroundColor:themes.colors.white}}>
            <Swiperlast />
            </View>
            <Banner
              Case={'swipper_small'}
              imageUri={this.state.banners.map(
                (banner) => banner.image_data.url,
              )}
            />
          <View style={{height:30 , backgroundColor:themes.colors.accent}}></View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  tittle: {
    color: themes.colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
  text: {
    color: 'grey',
  },
  section: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  textinput: {
    flex: 1,
    paddingLeft: 30,
  },
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default HomeContainer(Home);
