
import * as themes from '../../stack/theme';
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
} from 'react-native';
import {Block, Text} from '../../config';

import StarRating from 'react-native-star-rating';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
