
import * as themes from '../../stack/theme';
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
} from 'react-native';
import {Block, Text} from '../../config';

import StarRating from 'react-native-star-rating';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function nameandreview (props)
{
    return(
   <View style={{height:hp('10%'), width:wp('100%'),backgroundColor:themes.colors.white,alignContent:'center', flexDirection:'row'}}>
       <Text bold style={{marginTop:hp('2.5%'),marginLeft:10, fontSize: 23, flex:3}}> {props.name} </Text>
            <View style={{marginTop:hp('3.5%'),flex:1/2}}>
            <StarRating
                    disabled={true}
                    maxStars={5}
                    rating={props.starcount}
                    starSize={10}        />
                    
             </View>  
             <Text style={{marginTop:hp('3%'), fontSize: 15, flex:1}}>  {props.totalreview} reviews</Text>
           
     </View>
    )
}
