import * as themes from '../../stack/theme';
import React, {useState} from 'react';
import {
    View,
    TouchableOpacity,
} from 'react-native';
import {Text} from '../../config';
import Iconant from 'react-native-vector-icons/AntDesign';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function hiddenhandle(isHidden, setisHidden) {
    setisHidden(!isHidden);
}

export default function Productdis(props) {
    const [isHidden, setisHidden] = useState(false);

    return (
        <View
            style={{
                // height: isHidden ? hp('15%') : hp('7%'),
                width: wp('100%') - 20,
                marginTop: 10
            }}>
            <TouchableOpacity
                style={{
                    // height: hp('4%'),
                    flexDirection: 'row',
                    width: wp('100%') - 20,
                    backgroundColor: '#f9f9f9',
                    borderWidth: 0.5,
                    borderColor: themes.colors.gray,
                    alignItem: 'center',
                    alignContent: 'center',
                    padding: 15
                }}
                onPress={() => hiddenhandle(isHidden, setisHidden)}>
                <Text
                    textcolor
                    bold
                    style={{flex: 8, marginTop: 5, marginLeft: 5, fontSize: 15}}>
                    PRODUCT DESCRIPTION{' '}
                </Text>
                {isHidden ? (
                    <Iconant
                        style={{flex: 1, marginTop: 5}}
                        name="up"
                        size={20}
                        color={themes.colors.primary}
                    />
                ) : (
                    <Iconant
                        style={{flex: 1, marginTop: 5}}
                        name="down"
                        size={20}
                        color={themes.colors.primary}
                    />
                )}
            </TouchableOpacity>
            {isHidden ? (
                <View
                    style={{
                        // height: hp('10%'),
                        width: wp('100%') - 20,
                        backgroundColor: '#f9f9f9',
                        borderColor: themes.colors.gray,
                        borderLeftWidth: 0.5,
                        borderBottomWidth: 0.5,
                        borderRightWidth: 0.5,
                        padding: 15
                    }}>
                    <Text style={{marginLeft: 5, marginTop: 5, marginRight: 5}}>{props.description}</Text>
                </View>
            ) : null}
        </View>
    );
}
// this.state.isHidden ?
// <View style={style.activityContainer} hide={false}>
//   <ActivityIndicator size="small" color="#00ff00"
//    animating={true}/></View> : null
