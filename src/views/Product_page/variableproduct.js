import React, {useEffect, useState} from 'react';

import * as themes from '../../stack/theme';
import {
    StyleSheet,
    TextInput,
    View,
    ScrollView,
    Image,
    Keyboard,
    Dimensions,
    TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    SafeAreaView,
    StatusBar,
    Button,
} from 'react-native';
import {Block, Text} from '../../config';
import Header from '../../components/Header';
import Searchbar from '../../components/Searchbar';
import Banner from '../../components/Banner';
import Cat from '../../components/Cat';
import NumericInput from 'react-native-numeric-input';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Product from '../../components/Product';
import Checkavail from '../../components/checkavail';
import NameandReview from './nameandreview';
import BanneR from '../../components/bannerswiper';
import {Dropdown} from 'react-native-material-dropdown';
import ProductDes from './product_discription';
import Mystatusbar from '../../components/mystatusbar';
import ProductContainer from '../../containers/Products/Product';
import ProductImage from '../../assets/common/product-dummy.jpg';
import _ from 'lodash';
import {getAppUrl} from "../../helpers/api";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Layout from "../../components/Layout/Layout";
import {formatPrice} from "../../helpers/common";

function Variableproduct({route, navigation, ...props}) {
    const [Quantity, setQuantity] = useState(1);
    const [weight, setweight] = useState(1);

    const {product_id} = route.params;

    const [product, setProduct] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    useEffect(function () {
        props.actions.getProductById(product_id)
            .then(res => {
                setProduct(res.product)
                setIsLoading(false);
            })
            .catch(err => console.error(err));
    }, [product_id]);

    const avail = true;

    const productVariant = _.get(product, "variants[0]", {
        name: "",
        price: 0,
        short_description: "",
        additional_info: "",
        description: "",
        images: [{
            url: getAppUrl('frontend/static/media/thumbnail.ead95de8.jpg'),
            isDefault: true
        }]
    })

    const {
        name,
        price,
        short_description,
        additional_info,
        description,
        ...variantData
    } = productVariant;

    const images = variantData.images
        .sort(function (a, b) {
            return a.isDefault - b.isDefault;
        })
        .map(function (image) {
            return {
                original: image.url,
                thumbnail: image.url,
            };
        });

    if (isLoading) {
        return <VariableProductPlaceholder navigation={navigation}/>;
    }

    return (
        <Layout navigation={navigation}>
            <NameandReview
                name={name}
                starcount={5}
                totalreview={10}
            />
            {/*<BanneR/>*/}

            <Banner
                Case={'swipper_small'}
                imageUri={images.map(
                    (image) => image.thumbnail,
                )}
            />

            <View style={{marginLeft: 10, flexDirection: 'column'}}>
                <Text primary style={{fontSize: 30}}>
                    {formatPrice(price)}
                </Text>
                <Text style={{fontSize: 25, marginTop: 10}}>
                    {avail ? 'IN STOCK' : 'OUT OF STOCK'}{' '}
                </Text>
                <Text style={{fontSize: 15, marginTop: 5}}>
                    Ship from and sold by Kisaan
                </Text>

                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 15, marginTop: 8, flex: 2}}>
                        Quantity{' '}
                    </Text>
                    <Text
                        style={{fontSize: 15, marginTop: 8, flex: 2, marginLeft: 5}}>
                        Weight
                    </Text>
                    <TouchableOpacity
                        onPress={null}
                        style={[
                            styles.button,
                            {
                                flex: 5,
                                marginRight: 50,
                                backgroundColor: themes.colors.secondary,
                                borderColor: themes.colors.white,
                                borderWidth: 0,
                                borderRadius: 5,
                                marginTop: 2,
                            },
                        ]}>
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 15,
                            }}>
                            Check Availability
                        </Text>
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection: 'row', marginTop: 10}}>
                    <View style={{flex: 2, marginLeft: 5}}>
                        <NumericInput
                            value={Quantity}
                            type="up-down"
                            onChange={(value) => setQuantity(value)}
                            onLimitReached={(true, 'can not empty')}
                            totalWidth={50}
                            rounded={true}
                            totalHeight={height / 30}
                            iconSize={40}
                            step={1}
                            valueType="integer"
                            textColor="#000000"
                            iconStyle={{color: '#000000'}}
                            minValue={1}
                            onLimitReached={() => console.log('limit reached!')}
                        />
                    </View>
                    <View style={{flex: 9}}>
                        <NumericInput
                            containerStyle={{}}
                            value={weight}
                            type="up-down"
                            onLimitReached={(true, 'can not empty')}
                            onChange={(value) => setweight(value)}
                            rounded={true}
                            totalHeight={height / 30}
                            separatorWidth={1}
                            totalWidth={width - width / 3.1}
                            iconSize={10}
                            step={1}
                            valueType="integer"
                            textColor="#000000"
                            iconStyle={{color: '#000000'}}
                        />
                    </View>
                </View>
                <TouchableOpacity>
                    <View style={{flexDirection: 'row', marginTop: 30}}>
                        <View style={{flex: 1}}>
                            <Image
                                style={{height: 20, width: 20}}
                                source={require('../../assets/common/calender.png')}
                            />
                        </View>
                        <View style={{flex: 12}}>
                            <Text style={{fontSize: 15}}>
                                Choose Delivery Date: 31 oct 2020
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={null}
                    style={[
                        {
                            width: widthbuttonlarge,
                            // height: 35,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: themes.colors.secondary,
                            borderColor: themes.colors.white,
                            borderWidth: 0,
                            borderRadius: 5,
                            marginTop: 10,
                            paddingTop: 5,
                            paddingBottom: 5
                        },
                    ]}>
                    <Text white style={{fontSize: 25}}>
                        ADD TO CART
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={null}
                    style={[
                        {
                            width: widthbuttonlarge,
                            // height: 35,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#f9f9f9',
                            borderColor: themes.colors.gray,
                            borderWidth: 0.5,
                            borderRadius: 5,
                            marginTop: 10,
                            paddingTop: 5,
                            paddingBottom: 5
                        },
                    ]}>
                    <Text primary style={{fontSize: 22, alignItems: 'center'}}>
                        BUY NOW
                    </Text>
                </TouchableOpacity>
                <View style={{height: hp('40%'), marginTop: 10, marginRight: 10}}>
                    <Text>
                        {
                            //from api kissan line
                        }{' '}
                        kissan milk line
                    </Text>
                    <View>
                        {
                            // share this
                        }
                    </View>
                    <ProductDes id={product_id} description={description}/>
                    {/*{*/}
                    {/*    //<View>< REVIEW >*/}
                    {/*}*/}
                    {/*<Text style={{fontSize: 25, marginTop: 10}}>Recomended Products</Text>*/}
                    {/*<Text style={{marginTop: 15}}>*/}
                    {/*    dolor sit amet, consectetur adipiscing elit*/}
                    {/*</Text>*/}
                </View>
            </View>
        </Layout>
    );
}

function VariableProductPlaceholder(props) {
    return <Layout navigation={props.navigation}>
        <SkeletonPlaceholder backgroundColor={themes.colors.gray} highlightColor={themes.colors.gray2}>
            <SkeletonPlaceholder.Item flexDirection="row" marginTop={10}>
                <SkeletonPlaceholder.Item width={wp('50%')} height={hp('5%')} borderRadius={1}/>
                <SkeletonPlaceholder.Item marginLeft={wp("30%")} width={wp('20%')} height={hp('5%')} borderRadius={1}/>
            </SkeletonPlaceholder.Item>
            <SkeletonPlaceholder.Item marginTop={10} height={hp('40%')}></SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
    </Layout>;
}

const {width, height} = Dimensions.get('screen');

const widthbutton = width * 0.2;
const widthbuttonlarge = width * 0.95;
const styles = StyleSheet.create({
    tittle: {
        color: themes.colors.primary,

        fontSize: 30,
    },
    text: {
        color: 'grey',
    },
    section: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        marginTop: 10,
    },
    textinput: {
        flex: 1,
        paddingLeft: 30,
    },
    button: {
        width: widthbutton,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default ProductContainer(Variableproduct);
