/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */

import React, {useEffect} from 'react';
import * as themes from '../../../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {Block, Text} from '../../../config';
import Imagecom from '../../../components/Imagecom';
import Searchbar from '../../../components/Searchbar';
import Banner from '../../../components/Banner';
import Cat from '../../../components/Cat';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerNavigator} from 'react-navigation';
import Product from '../../../components/Product';
import Checkavail from '../../../components/checkavail';
import {FlatGrid} from 'react-native-super-grid';
import HomeContainer from '../../../containers/Home/Home';
import {formatPrice} from '../../../helpers/common';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Myaccount_tab from '../../../components/Myaccount_tab';
import Layout from '../../Layout/layouth';
export default function MyAccount(props) {
  useEffect(function () {
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor(themes.colors.white);
  }, []);

  return (
    
    <Layout
    title={'Login & Security'} >
    <View style={styles.container}>
    <SafeAreaView >
  {/* <View style={{flex:1}}>
<Imagecom
type={'right_circle'}
left={width - widthcircle + 80}
top={-80}
height={hieghtcircle}
width={widthcircle}
/>
<Imagecom
type={'kisaan_logo'}
left={width / 2 - width / 6}
top={25}
height={hieghtcircle - hieghtcircle / 1.8}
width={widthcircle - widthcircle / 5}
/>
</View> */}
      <View style={{ flex:1,backgroundColor:themes.colors.white}}>
{/*        
        <View style={{marginTop: 20, backgroundColor:themes.colors.white}}>
        <Text textcolor style={{fontSize: 30, marginTop:300, backgroundColor:themes.colors.white, height:60}}>
          Login & Security
        </Text> */}

          {
            // component start
          }
          <Myaccount_tab Header={'Name:'} UserText={'Dhruv'} />

          <Myaccount_tab Header={'Mobile Phone Number'} UserText={'Dhruv'} />

          <Myaccount_tab Header={'Email'} UserText={'Dhruv'} />

          <Myaccount_tab Header={'Password'} UserText={'Dhruv'} />

          {
            // end here
          }
        </View>
      
  
    </SafeAreaView>
    </View>
    </Layout>
  );
}
const {width, height} = Dimensions.get('screen');

const hieghtcircle = height * 0.2;

const widthcircle = width * 0.4;
const widthbutton = width * 0.8;

const styles = StyleSheet.create({
  container: {
   flex:1,
   flexDirection: 'column',
   backgroundColor: themes.colors.white,
  },

  tittle: {
    color: themes.colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
  text: {
    color: 'grey',
  },
  section: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10,
  },
  textinput: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  content_section: {
    padding: 24,
    flex: 6,
  },
  profile_section: {
    flex: 1,
    backgroundColor: '#f0f5f9',
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
    padding: 15,
  },
});



