/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */

import React from 'react';
import {StyleSheet, View} from 'react-native';
import Text from '../../../config/Text';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
// import Layout from '../Layout/Layout';
import Layout from '../../Layout/layouth';

export default function (props) {
  return (
    <Layout title={'Your Addresses'}>
      <ScrollView>
        <View
          style={{
            ...styles.address_section,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 80,
            paddingBottom: 80,
          }}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('MyAccount.YourAddresses.AddAddress')}
            style={{alignItems: 'center'}}>
            <Icon name="plus-circle" size={30} color="grey" />
            <Text>Add Address</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.address_section}>
          <Text>Default: </Text>
          <Text>User Name</Text>
          <Text>House No. 150</Text>
          <Text>Thapar Colony, street no. 2,</Text>
          <Text>LUDHIANA, PUNJAB 141401</Text>
          <Text>India</Text>
          <Text>Phone Number: 9876543210</Text>
          <Text>Add Delivery Instructions</Text>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <TouchableOpacity>
              <Text>Edit</Text>
            </TouchableOpacity>
            <Text> | </Text>
            <TouchableOpacity>
              <Text>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.address_section}>
          <Text>Default: </Text>
          <Text>User Name</Text>
          <Text>House No. 150</Text>
          <Text>Thapar Colony, street no. 2,</Text>
          <Text>LUDHIANA, PUNJAB 141401</Text>
          <Text>India</Text>
          <Text>Phone Number: 9876543210</Text>
          <Text>Add Delivery Instructions</Text>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <TouchableOpacity>
              <Text>Edit</Text>
            </TouchableOpacity>
            <Text> | </Text>
            <TouchableOpacity>
              <Text>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.address_section}>
          <Text>Default: </Text>
          <Text>User Name</Text>
          <Text>House No. 150</Text>
          <Text>Thapar Colony, street no. 2,</Text>
          <Text>LUDHIANA, PUNJAB 141401</Text>
          <Text>India</Text>
          <Text>Phone Number: 9876543210</Text>
          <Text>Add Delivery Instructions</Text>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <TouchableOpacity>
              <Text>Edit</Text>
            </TouchableOpacity>
            <Text> | </Text>
            <TouchableOpacity>
              <Text>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </Layout>
  );
}

const styles = StyleSheet.create({
  address_section: {
    backgroundColor: '#f0f5f9',
    marginTop: 5,
    marginBottom: 5,
    padding: 15,
  },
});
