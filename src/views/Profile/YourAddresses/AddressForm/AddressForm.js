/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */

import React from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
import * as themes from '../../../../stack/theme';
import Text from '../../../../config/Text';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
// import Layout from '../../Layout/Layout';
import Layout from '../../../Layout/layouth';
import CustomInput from '../../../../components/CustomInput';
import {Formik} from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  name: Yup.string()
      // .email('Email is not valid')
      .required('Please enter name'),
    address: Yup.string()
      // .min(8, 'Password must be of 8 characters')
      .required('Please enter address'),
    city: Yup.string().required('Please enter address'),
    state: Yup.string().required('Please enter state'),
    country: Yup.string().required('Please enter country'),
    postal_code: Yup.string().required('Please enter postal_code'),
    phone: Yup.string().required('Please enter phone'),
  });

export default function (props) {
  return (
    <Layout title={'Add Address'}>
      {/* <ScrollView> */}
        <View>
            <Formik
                initialValues={{ name: "", address: "", city: "", state: "", country: "", postal_code: "", phone: "" }}
                onSubmit={(values, {setErrors, resetForm}) => {
                  resetForm();
                  props.navigation.navigate("MyAccount.YourAddresses")
                }
                }
                validationSchema={validationSchema}>

                {(props) => {
                  const {
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    errors,
                  } = props;
                  return (
                    <>
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'Name'}
                        placeholder={'Name'}
                        name={'name'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'Address'}
                        placeholder={'Address'}
                        name={'address'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'City'}
                        placeholder={'City'}
                        name={'city'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'State'}
                        placeholder={'State'}
                        name={'state'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'Country'}
                        placeholder={'Country'}
                        name={'country'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'Postal Code'}
                        placeholder={'Postal Code'}
                        name={'postal_code'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      <CustomInput
                        stylepro={{flex: 1, fontFamily: 'Poppins-Light'}}
                        title={'Phone'}
                        placeholder={'Phone'}
                        name={'phone'}
                        // icon={require('../assets/common/mail.png')}
                        {...props}
                      />
                      
                      <TouchableOpacity
                        onPress={handleSubmit}
                        style={[
                          styles.button,
                          {
                            backgroundColor: themes.colors.secondary,
                            borderColor: themes.colors.white,
                            borderWidth: 0,
                            borderRadius: 0,
                            marginTop: height / 100 + 10,
                          },
                        ]}>
                        <Text
                          style={{
                            color: 'white',
                            fontFamily: 'Poppins-Light',
                            fontSize: width / 20,
                            marginLeft:width/ 2 -width/ 7,
                          }}>
                          CONTINUE{' '}
                        </Text>
                      </TouchableOpacity>
                    </>
                  );
                }}
            </Formik>
        </View>
        {/* </ScrollView> */}
    </Layout>
  );
}

const {width, height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  address_section: {
    backgroundColor: '#f0f5f9',
    marginTop: 5,
    marginBottom: 5,
    padding: 15,
  },
});
