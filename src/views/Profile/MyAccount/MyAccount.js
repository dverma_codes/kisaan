/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */

import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Text from '../../../config/Text';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Layout from '../../Layout/layouth';
import Address from '../../../assets/common/address.png';
import Profile from '../../../assets/common/person.png';
import Subscription from '../../../assets/common/rss.png'
export default function MyAccount(props) {
  return (
    <Layout title={'Your Account'}>
      <View style={styles.profile_section}>
        <View style={{flex: 2}}>
          <Image
            source={{
              uri: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            }}style={{flex: 1, width: 70,alignSelf:'center',
             height: 80, resizeMode: 'center'}}
          
          />
        </View>
        <View style={{flex: 4}}>
          <Text  style={styles.heading}>
            Your Orders
          </Text>
          <Text>Track, Return, Or buy things again</Text>
        </View>
      </View>
      <View style={styles.profile_section}>
        <View style={{flex: 2}}>
          <Image
            source={Profile}
            style={{flex: 1, width: 60,alignSelf:'center', height: 80,
             resizeMode: 'stretch'}}
          />
        </View>
        <View style={{flex: 4}}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('MyAccount.ProfileAndSecurity')
            }>
            <Text  style={styles.heading}>
              {'Profile & Security'}
            </Text>
          </TouchableOpacity>
          <Text>Edit login, name and mobile number</Text>
        </View>
      </View>
      <View style={styles.profile_section}>
        <View style={{flex: 2}}>
          <Image
            source={Address}
            style={{flex: 1, width: 60,alignSelf:'center', height: 80, resizeMode: 'stretch'}}
          
          />
        </View>
        <View style={{flex: 4}}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('MyAccount.YourAddresses')
            }>
            <Text style={styles.heading}>
              Your Addresses
            </Text>
          </TouchableOpacity>
          <Text>Edit Addresses for orders and gifts</Text>
        </View>
      </View>

      <View style={styles.profile_section}>
        <View style={{flex: 2}}>
          <Image
            source={{
              uri: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            }}
            style={{flex: 1, width: 70,alignSelf:'center', height: 80, resizeMode: 'center'}}
          
          />
        </View>
        <View style={{flex: 4}}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('MyAccount.PaymentOptions')
            }>
            <Text  style={styles.heading}>
              Payment Options
            </Text>
          </TouchableOpacity>
          <Text>Edit or add payment methods</Text>
        </View>
      </View>
      <View style={styles.profile_section}>
        <View style={{flex: 2}}>
          <Image
            source={Subscription
            }
            style={{flex: 1, width: 70,alignSelf:'center', height: 80, 
            resizeMode: 'center'}}
          
          />
        </View>
        <View style={{flex: 4}}>
          <Text h3 style={styles.heading}>
            Subscription
          </Text>
          <Text>Manage Subscriptions</Text>
        </View>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  profile_section: {
    flex: 1,
    backgroundColor: '#f0f5f9',
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
    padding: 15,
  },
heading: {
  fontSize:24,
  marginTop: 10
}
});
