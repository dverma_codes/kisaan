/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */

import React, {useEffect} from 'react';
import * as themes from '../../../stack/theme';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import Text from '../../../config/Text';
import Header from '../../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { useNavigation } from '@react-navigation/native';

export default (function({title, children, ...props}) {

    const navigation = useNavigation();

    useEffect(function() {
        StatusBar.setBarStyle('dark-content', true);
        StatusBar.setBackgroundColor(themes.colors.white);
    }, []);

    return <View style={styles.container}>
        <SafeAreaView>
          <View style={{flex: 1}}>
            <Header
              widhheader={wp('100%')}
              heightheader={hp('14%')}
              navigation={navigation}
            />
            </View>
            <View style={styles.content_section}>
              <View style={{flex: 1}}>
                <Text h2>{title}</Text>
              </View>
              <View style={{flex: 11}}>
                {children}
              </View>
            </View>
        </SafeAreaView>
    </View>
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: '#fff',
  },
  content_section: {
    padding: 24,
    flex: 6
  }
});