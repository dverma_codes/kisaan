/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
//import {firebase} from '@react-native-firebase/auth';
import {Block, Text} from '../config';
import * as themes from '../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
  Platform,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import RegisterContainer from '../containers/Auth/Register';
import Imagecom from '../components/Imagecom';
import Imputwithimg from '../components/Inputwithimg';
//import asyncStr from '../config/Asyncstr';
import {Formik} from 'formik';
import CustomInput from '../components/CustomInput';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  name: Yup.string().required('Please enter name'),
  email: Yup.string()
    .email('Email is not valid')
    .required('Please enter email'),
  password: Yup.string()
    .min(8, 'Password must be of 8 characters')
    .required('Please enter password'),
});

class Register extends React.Component {
  componentDidMount() {
    /* StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor(themes.colors.white);*/
  }

  render() {
    return (
      <KeyboardAvoidingView
        contentContainerStyle={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'position' : 'position'}
        style={{flex: 1, backgroundColor: themes.colors.white}}>
        <View style={{flex: 1, backgroundColor: themes.colors.white}}>
          <View style={{flex: 1}}>
            <SafeAreaView style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  backgroundColor: themes.colors.white,
                }}>
                <View
                  style={{
                    flex: Platform.OS === 'ios' ? 1 : 1.5,
                    backgroundColor: themes.colors.white,
                    flexDirection: 'row',
                  }}>
                  <View style={{flex: 2}}>
                    <View style={{alignSelf: 'center'}}>
                      <Imagecom
                        type={'kisaan_logo'}
                        left={10}
                        top={Platform.OS === 'ios' ? 10 : 45}
                        height={80}
                        width={120}
                      />
                    </View>
                  </View>
                  <View style={{flex: 1, backgroundColor: themes.colors.white}}>
                    <Imagecom type={'right_circle'} />
                  </View>
                </View>
                <View style={{flex: 2.2, backgroundColor: themes.colors.white}}>
                  <Imagecom
                    type={'register'}
                    left={width / 2 - width * 0.35}
                    height={hieghtcircle * 1.4}
                    width={widthcircle * 1.8}
                  />
                </View>
                <View style={{flex: 0.25}} />
                <View style={{flex: 3}}>
                  <Block
                    card
                    shadow
                    color="white"
                    flex={3}
                    style={{
                      flex: 3,
                      flexDirection: 'column',
                      zIndex: 1,
                      width: width - (2 * width) / 20,

                      marginLeft: width / 20,
                      marginRight: width / 20,
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 0,
                      },
                      shadowOpacity: 1,
                      shadowRadius: 5,

                      elevation: Platform.OS === 'ios' ? 100000 : 3,
                    }}>
                    <View style={{marginLeft: 20, marginRight: 20}}>
                      <Text style={{fontSize: width / 13}} textcolor>
                        Register
                      </Text>
                      <Text
                        style={{marginTop: height / 100, fontSize: width / 25}}
                        textcolor>
                        {' '}
                        Please register to continue
                      </Text>

                      <Formik
                        initialValues={{
                          email: '',
                          password: '',
                          name: '',
                        }}
                        onSubmit={(values, {setErrors}) =>
                          this.props.actions.attemptRegister(values, setErrors)
                        }
                        validationSchema={validationSchema}>
                        {(props) => {
                          const {
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            values,
                            errors,
                          } = props;
                          return (
                            <>
                              <CustomInput
                                stylepro={{
                                  flex: 1,
                                  fontFamily: 'Poppins-Regular',
                                }}
                                title={'Name'}
                                placeholder={'Name'}
                                name={'name'}
                                icon={require('../assets/common/person.png')}
                                {...props}
                              />
                              <CustomInput
                                stylepro={{
                                  flex: 1,
                                  fontFamily: 'Poppins-Regular',
                                }}
                                title={'Email'}
                                placeholder={'Email'}
                                name={'email'}
                                icon={require('../assets/common/mail.png')}
                                {...props}
                              />
                              <CustomInput
                                stylepro={{
                                  flex: 1,
                                  fontFamily: 'Poppins-Regular',
                                }}
                                title={'Password'}
                                placeholder={'Password'}
                                name={'password'}
                                icon={require('../assets/common/password.png')}
                                type={'password'}
                                {...props}
                              />
                              {/* <Imputwithimg type={'email_input'} hold={'Name'} />
                    <Imputwithimg type={'email_input'} hold={'Email'} />
                    <Imputwithimg type={'pass_input'} hold={'Password'} /> */}
                              <TouchableOpacity
                                onPress={handleSubmit}
                                style={[
                                  styles.button,
                                  {
                                    backgroundColor: themes.colors.secondary,
                                    borderColor: themes.colors.white,
                                    borderWidth: 0,
                                    borderRadius: 0,
                                    marginTop: 0,
                                  },
                                ]}>
                                <Text
                                  style={{
                                    color: 'white',
                                    fontFamily: 'Poppins-Regular',
                                    fontSize: width / 20,
                                  }}>
                                  CONTINUE
                                </Text>
                              </TouchableOpacity>
                            </>
                          );
                        }}
                      </Formik>
                    </View>
                  </Block>
                </View>
                <View style={{flex: 0.25}} />
              </View>
              {/*
        <Formik
          initialValues={{
            email: '',
            password: '',
            password_confirmation: '',
            name: '',
          }}
          onSubmit={(values) => this.props.actions.attemptRegister(values)}
          validationSchema={validationSchema}>
          {(props) => {
            const {
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
            } = props;

            return (
              <View>
                <CustomInput title={'Name'} name={'name'} {...props} />

                <CustomInput title={'Email'} name={'email'} {...props} />

                <CustomInput title={'Password'} name={'password'} {...props} />

                <CustomInput
                  title={'Password Confirmation'}
                  name={'password_confirmation'}
                  {...props}
                />

                <Button onPress={handleSubmit} title="Submit" />
              </View>
            );
          }}
        </Formik> */}
            </SafeAreaView>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const {width, height} = Dimensions.get('screen');
const hieghtcircle = height * 0.2;

const widthcircle = width * 0.4;
const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  button: {
    width: widthbutton,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default RegisterContainer(Register);
