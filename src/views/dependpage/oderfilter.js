import React, {useState} from 'react';

import * as themes from '../../stack/theme';
import {View, ScrollView, TouchableOpacity, SafeAreaView} from 'react-native';
import {Text} from '../../config';
import Header from '../../components/Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Mystatusbar from '../../components/mystatusbar';
import {useNavigation} from '@react-navigation/native';
import SearchBar from '../../components/Searchbar';
import {CheckBox} from 'react-native-elements';

export default function Oderfilter(props) {
  const [checked, Setchecked] = useState([
    {
      title: 'Orders',
      Check: false,
    },
    {
      title: 'Open Orders',
      Check: false,
    },
    {
      title: 'Cancelled orders',
      Check: false,
    },
    {
      title: 'Subsription Cancelled orders',
      Check: false,
    },
  ]);

  const [checkTime, SetcheckTime] = useState([
    {
      title: 'Last 30 days',
      Check: false,
    },
    {
      title: 'Last 6 Month',
      Check: false,
    },
    {
      title: '2021',
      Check: false,
    },
    {
      title: '2020',
      Check: false,
    },
  ]);
  const checkboxHandler = (value, index) => {
    const newValue = checked.map((checkbox, i) => {
      if (i !== index) {
        return {
          ...checkbox,
          check: false,
        };
      }
      if (i === index) {
        const item = {
          ...checkbox,
          check: !checkbox.check,
        };
        return item;
      }
      return checkbox;
    });
    Setchecked(newValue);
  };

  const checkTimeHandler = (value, index) => {
    const newValue = checkTime.map((checkbox, i) => {
      if (i !== index) {
        return {
          ...checkbox,
          check: false,
        };
      }
      if (i === index) {
        const item = {
          ...checkbox,
          check: !checkbox.check,
        };
        return item;
      }
      return checkbox;
    });
    SetcheckTime(newValue);
  };

  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: themes.colors.accent}}>
      <SafeAreaView style={{backgroundColor: themes.colors.accent}}>
        <ScrollView scrollEventThrottle={16}>
          <Mystatusbar />
          <Header
            widhheader={wp('100%')}
            heightheader={hp('10%')}
            navigation={navigation}
          />

          <SearchBar back={true} navigation={navigation} />
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              marginLeft: 10,
              marginRight: 10,
            }}>
            <View style={{flex: 2, flexDirection: 'row', marginTop: 10}}>
              <Text textcolor style={{flex: 3, fontSize: 30}}>
                Order Type
              </Text>
              <TouchableOpacity
                style={{
                  flex: 1,
                  borderRadius: 5,
                  backgroundColor: themes.colors.textcolor,
                }}
                onPress={null}>
                <Text
                  white
                  bold
                  style={{
                    flex: 1,
                    marginTop: 7,
                    alignSelf: 'center',
                    fontSize: 15,
                  }}>
                  APPLY NOW
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{marginTop: 30}}>
              {checked.map((opt, i) => (
                <View
                  style={{
                    flex: 1,
                    marginTop: 1,
                    backgroundColor: themes.colors.white,
                  }}>
                  <CheckBox
                    containerStyle={{
                      backgroundColor: themes.colors.white,
                      borderWidth: 0,
                    }}
                    title={opt.title}
                    key={i}
                    textStyle={{
                      fontSize: 15,
                      fontColor: themes.colors.textcolor,
                    }}
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checkedColor={themes.colors.primary}
                    checked={opt.check}
                    onPress={() => {
                      checkboxHandler(opt.check, i);
                    }}
                  />
                </View>
              ))}
            </View>

            <View style={{flex: 2, flexDirection: 'row', marginTop: 10}}>
              <Text textcolor style={{flex: 3, fontSize: 30}}>
                Order Time
              </Text>
            </View>
            <View style={{marginTop: 30}}>
              {checkTime.map((opt, i) => (
                <View
                  style={{
                    flex: 1,
                    marginTop: 1,
                    backgroundColor: themes.colors.white,
                  }}>
                  <CheckBox
                    containerStyle={{
                      backgroundColor: themes.colors.white,
                      borderWidth: 0,
                    }}
                    title={opt.title}
                    key={i}
                    textStyle={{
                      fontSize: 15,
                      fontColor: themes.colors.textcolor,
                    }}
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checkedColor={themes.colors.primary}
                    checked={opt.check}
                    onPress={() => {
                      checkTimeHandler(opt.check, i);
                    }}
                  />
                </View>
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
