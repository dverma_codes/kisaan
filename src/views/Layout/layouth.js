/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */

import React, {useEffect} from 'react';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import Text from '../../config/Text';
import Header from '../../components/Header';

import Imagecom from '../../components/Imagecom';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import {platform} from 'react-native';
export default (function ({title, children, ...props}) {
  const navigation = useNavigation();

  useEffect(function () {
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor(themes.colors.white);
  }, []);

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View style={{flex: 1, width: wp('100%'), height: hp('14%')}}>
          <Imagecom
            type={'right_circle'}
            left={width - widthcircle + 50}
            top={-30}
            height={hieghtcircle}
            width={widthcircle}
          />
          <Imagecom
            type={'kisaan_logo'}
            left={width / 2 - width / 6}
            top={50}
            height={hieghtcircle - hieghtcircle / 1.8}
            width={widthcircle - widthcircle / 5}
          />
        </View>
        <View style={styles.content_section}>
          <View style={{flex: 1}}>
            <Text h2 textcolor>{title}</Text>
          </View>
          <View style={{flex: 11}}>{children}</View>
        </View>
      </SafeAreaView>
    </View>
  );
});

const {width, height} = Dimensions.get('screen');
const hieghtcircle = height * 0.2;

const widthcircle = width * 0.4;
const widthbutton = width * 0.8;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  content_section: {
    padding: 24,
    flex: 6,
    marginTop:30
  },
});
