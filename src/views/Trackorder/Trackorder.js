import React, {useState} from 'react';
import * as themes from '../../stack/theme';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
} from 'react-native';
import {Text} from '../../config';
import Mystatusbar from '../../components/mystatusbar';
import Header from '../../components/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Searchbar from '../../components/Searchbar';
import Orderstep from './orderstep';
import Mapsorder from './mapsorder';
import Pickupleft from './pickupleft';
export default function Trackorder({navigation, ...props}) {
  const [arrivalday, SetarrivalDay] = useState('Arriving Wednessday');
  return (
    <View style={{flex: 1, backgroundColor: themes.colors.white}}>
      <SafeAreaView style={{backgroundColor: themes.colors.white}}>
        <ScrollView
          scrollEventThrottle={16}
          style={{backgroundColor: themes.colors.white}}>
          <Mystatusbar />
          <Header
            widhheader={wp('100%')}
            heightheader={hp('10%')}
            navigation={navigation}
          />
          <Searchbar back={true} />
          <View
            style={{
              flex: 1,
              backgroundColor: themes.colors.white,
              width: wp('100%'),
              flexDirection: 'column',
            }}>
            <View style={{margin: 10, flex: 1, marginBottom: 0}}>
              <Text
                textcolor
                style={{
                  fontSize: Platform.OS === 'ios' ? 25 : wp('100%') / 12,
                  marginLeft: 10,
                  fontWeight: 'bold',
                }}>
                Arriving Monday{props.day}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                margin: 20,
                marginTop: 0,
                marginBottom: 0,
              }}>
              <View style={{flex: 3}}>
                <Text
                  black
                  style={{
                    fontSize: Platform.OS === 'ios' ? 20 : wp('100%') / 15,
                  }}>
                  23-11-2020{props.date}
                </Text>
              </View>
              <View
                style={{
                  flex: 3,
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                <Text textcolor>
                  order #:213213123213123123123{props.order}
                </Text>
              </View>
            </View>
            <View>
              <Orderstep arrivingday={'MONDAY'} />
            </View>
            <View>
              <Mapsorder />
            </View>
            <View>
              <View style={{height:30 , width:wp('100%') , backgroundColor:themes.colors.accent}}></View>
            </View>
            <View>
              <View style={{ height:hp('15') ,flexDirection:'column' , margin:20}}>
              <Text style={{flex:2 , fontSize:Platform.OS==='ios' ? 20 : wp("100%")/15 , fontWeight:'bold'}}>Shipping Address</Text>
                <Text style={{flex:1 ,  fontSize:Platform.OS==='ios' ? 13 : wp("100%")/20 }}>house no</Text>
                <Text style={{flex:1,  fontSize:Platform.OS==='ios' ? 13 : wp("100%")/20 }}>steet no</Text>
                <Text style={{flex:1 ,  fontSize:Platform.OS==='ios' ? 13 : wp("100%")/20 }}>city and country</Text>
              </View>
            </View>
            <View>
              <Pickupleft />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
