import * as React from 'react';
import { StyleSheet, View, Text, FlatList, Image } from "react-native";
import StepIndicator from 'react-native-step-indicator';
import {useState} from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import trackordertick from '../../assets/common/trackordertick.png';
import * as themes from '../../stack/theme';
const stepIndicatorStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 40,
  separatorStrokeWidth: 3,
  currentStepStrokeWidth: 5,
  stepStrokeCurrentColor: themes.colors.primary,
  separatorFinishedColor: themes.colors.textcolor,
  separatorUnFinishedColor: themes.colors.gray,
  stepIndicatorFinishedColor: themes.colors.primary,
  stepIndicatorUnFinishedColor: themes.colors.gray,
  stepIndicatorCurrentColor: themes.colors.primary,
  stepIndicatorLabelFontSize: 15,
  currentStepIndicatorLabelFontSize: 15,
  stepIndicatorLabelCurrentColor: '#000000',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
  labelColor: themes.colors.primary,
  labelSize: 20,
  currentStepLabelColor: themes.colors.primary,
};

export default function Orderstep(props) {
  const {arrivingday} = props;
  const [lable, Setlable] = useState([
    'Order Today',
    'Shipped',
    'Out For delivery',
    arrivingday,
  ]);
  const renderStepIndicator = () => (
  <View style={{ height:20 , width:20}}><Image
    style={{height:20 , width:22}}
    source={trackordertick} />
  </View>
    );
  return (
    <View style={styles.container}>
      <View style={styles.stepIndicator}>
        <StepIndicator
          customStyles={stepIndicatorStyles}
          stepCount={4}
          currentPosition={2}
          renderStepIndicator={renderStepIndicator}
          direction="vertical"
          labels={lable}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {

    height: hp('40%'),
    width:  wp('100%'),
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  stepIndicator: {
    marginVertical: 10,
    paddingHorizontal: 20,
  },
  rowItem: {
    flex: 3,
    paddingVertical: 20,
  },
  title: {
    height: hp('2%'),
    width: wp('50%'),
    fontSize: 30,
    color: '#000',
  },
  body: {
    flex: 1,
    fontSize: 15,
    color: '#606060',
    lineHeight: 24,
    marginRight: 8,
  },
});
