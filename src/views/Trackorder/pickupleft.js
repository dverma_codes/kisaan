import * as React from 'react';
import { StyleSheet, View, FlatList, Image, Platform } from "react-native";
import StepIndicator from 'react-native-step-indicator';
import {useState} from 'react';

import {Text} from '../../config';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import * as themes from '../../stack/theme';
import Productpickup from "./productpickup";
const Pickupleft = (props) => {
  return (
    <View style={{width: wp('100'), backgroundColor: themes.colors.accent}}>
   <View style={{marginTop:hp('2%')}}>
     <Text
       textcolor
       style={{
         fontSize: Platform.OS === 'ios' ? 20 : wp('100%') / 15,
         marginLeft: 20,
         marginBottom:hp('2%'),
         fontWeight: 'bold',
       }}>Pick Up Where You Left
     </Text>
   </View>
      <Productpickup StarCount={2} />
      <Productpickup StarCount={5} />
      <Productpickup StarCount={5} />
      <Productpickup StarCount={5} />
    <View style={{height:40 , width:wp('100%') , backgroundColor:themes.colors.accent}}/>
    </View>
  );
};

export default Pickupleft;
