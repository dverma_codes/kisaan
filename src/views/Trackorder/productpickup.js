//stars repo link official ducument:https://github.com/djchie/react-native-star-rating
// https://github.com/djchie/react-native-star-rating
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import { View, StyleSheet, Image, Platform } from "react-native";
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Block, Text} from '../../config';
import StarRating from 'react-native-star-rating';
import LinearGradient from 'react-native-linear-gradient';

import * as themes from '../../stack/theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import star from '../../assets/android/Starfill.png';
export default function Productpickup(props) {
  return (
    <View style={{ marginTop:2,height:hp('25%') , width:wp('100%') ,backgroundColor:themes.colors.white , flexDirection:'row'}}>
      <View style={{flex:2 , backgroundColor:themes.colors.white}}>
        <Image source={{uri: props.image}}
        style={{height:hp('20') , width:wp('45') , alignSelf:'center',marginLeft:0,margin:20, backgroundColor:themes.colors.white}}/>
      </View>
      <View style={{flex:3 , backgroundColor:themes.colors.white, marginTop:20 , marginBottom:20}}>
        <View style={{flex:2.5 , marginRight:20}}><Text style={{flex:1 , fontSize:Platform.OS==='ios'
        ? 13 : wp('100')/25 ,justifyContent: 'center'}}>test product name come from props</Text></View>
        <View style={{flex:1.5 , width:wp('70%') , flexDirection:"row"}}>
          <View style={{flex:1.3}}><StarRating
          disabled={false}
          maxStars={5}
          fullStar={star}
          rating={props.StarCount}
          starSize={12}
          /></View>
          <View style={{flex:3 , marginLeft:10 }}>
        <Text style={{flex:1 , fontSize:Platform.OS==='ios'
            ? 13 : wp('100')/25 ,justifyContent: 'center'}}>(Review)
        </Text>
        </View></View>
        <View style={{flex:2}}><Text style={{flex:1 , fontSize:Platform.OS==='ios'
            ? 13 : wp('100')/25 ,justifyContent: 'center'}}>$8.50 price dollar</Text></View>
        <View style={{flex:2}}>
          <TouchableOpacity
            style={{
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              width: wp('20%'),
            }}
            onPress={null}>
            <LinearGradient
              start={{x: 1, y: 0}} //here we are defined x as start position
              end={{x: 0, y: 0}} //here we can define axis but as end position
              colors={['#0c461b', '#0d7b29']}
              style={{
                borderWidth: 1,
                borderRadius: 20,
                height: 25,
                width: wp('20%'),

                alignItems: 'center',
              }}>
              <View style={{alignSelf: 'center'}}>
                <Text white bold style={{fontSize: 9, marginTop: 5}}>
                  Add to cart
                </Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>

        </View>
        <View style={{flex:1}} />
      </View>

    </View>
  );
}
/*

<StarRating
  disabled={false}
  maxStars={5}
  fullStar={require('../assets/android/Starfill.png')}
  rating={this.props.StarCount}
  starSize={10}
/>
*/


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: wp('40%'),
    marginLeft: wp('5%'),
    borderRadius: 5,
  },
});
