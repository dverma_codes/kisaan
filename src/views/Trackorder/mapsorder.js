import React from 'react';
import {Image, SafeAreaView, View} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
/*
import {GOOGLE_API_KEY } from ".....api othe file "*/

const Mapsorder = (props) => {

  /*  uncomment and get mapview dome with some changes =>
  const initialRegion = {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  };
  const mapView = React.useRef();

  const [Wearhouseadd, setWearhouseadd] = React.useState(null);
  const [streetName, setStreetName] = React.useState('');
  const [fromLocation, setFromLocation] = React.useState(null);
  const [toLocation, setToLocation] = React.useState(null);
  const [region, setRegion] = React.useState(null);

  const [duration, setDuration] = React.useState(0);
  const [isReady, setIsReady] = React.useState(false);
  const [angle, setAngle] = React.useState(0);

  React.useEffect(() => {
    let {StartLOC, currentLocation} = props.loc;

    let fromLoc = currentLocation.gps;
    let toLoc = Wearhouseadd.location;
    let street = currentLocation.streetName;

    let mapRegion = {
      latitude: (fromLoc.latitude + toLoc.latitude) / 2,
      longitude: (fromLoc.longitude + toLoc.longitude) / 2,
      latitudeDelta: Math.abs(fromLoc.latitude - toLoc.latitude) * 2,
      longitudeDelta: Math.abs(fromLoc.longitude - toLoc.longitude) * 2,
    };

    setWearhouseadd(Wearhouseadd);
    setStreetName(street);
    setFromLocation(fromLoc);
    setToLocation(toLoc);
    setRegion(mapRegion);
  }, []);

  function calculateAngle(coordinates) {
    let startLat = coordinates[0].latitude;
    let startLng = coordinates[0].longitude;
    let endLat = coordinates[1].latitude;
    let endLng = coordinates[1].longitude;
    let dx = endLat - startLat;
    let dy = endLng - startLng;

    return (Math.atan2(dy, dx) * 180) / Math.PI;
  }

  function zoomIn() {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta / 2,
      longitudeDelta: region.longitudeDelta / 2,
    };

    setRegion(newRegion);
    mapView.current.animateToRegion(newRegion, 200);
  }

  function zoomOut() {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta * 2,
      longitudeDelta: region.longitudeDelta * 2,
    };

    setRegion(newRegion);
    mapView.current.animateToRegion(newRegion, 200);
  }*/
  /*
  function renderMap() {
    const destinationMarker = () => (
      <Marker
        coordinate={toLocation}
      >
        <View
          style={{
            height: 40,
            width: 40,
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: themes.colors.white
          }}
        >
          <View
            style={{
              height: 30,
              width: 30,
              borderRadius: 15,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: themes.colors.primary
            }}
          >
            <Image
              source={icons.pin}
              style={{
                width: 25,
                height: 25,
                tintColor:themes.colors.white
              }}
            />
          </View>
        </View>
      </Marker>
    )*/
  /*const BikeIcon = () => (
    <Marker
      coordinate={fromLocation}
      anchor={{ x: 0.5, y: 0.5 }}
      flat={true}
      rotation={angle}
    >
      <Image
        source={bikeicon}
        style={{
          width: 40,
          height: 40
        }}
      />
    </Marker>
  )*/
  /* return (
    <View style={{ flex: 1 }}>
      <MapView
        ref={mapView}
        provider={PROVIDER_GOOGLE}
        initialRegion={region}
        style={{ flex: 1 }}
      >
        <MapViewDirections
        // used to make a directions line
        //edit by dhruv
          origin={fromLocation}
          destination={toLocation}
          apikey={GOOGLE_API_KEY}
          strokeWidth={5}
          strokeColor={themes.colors..primary}
          optimizeWaypoints={true}
          onReady={result => {
            setDuration(result.duration)

            if (!isReady) {
              // Fit route into maps
              mapView.current.fitToCoordinates(result.coordinates, {
                edgePadding: {
                  right: (SIZES.width / 20),
                  bottom: (SIZES.height / 4),
                  left: (SIZES.width / 20),
                  top: (SIZES.height / 8)
                }
              })

              // Reposition the car
              let nextLoc = {
                latitude: result.coordinates[0]["latitude"],
                longitude: result.coordinates[0]["longitude"]
              }

              if (result.coordinates.length >= 2) {
                let angle = calculateAngle(result.coordinates)
                setAngle(angle)
              }

              setFromLocation(nextLoc)
              setIsReady(true)
            }
          }}
        />
        {destinationMarker()}
        {carIcon()}
      </MapView>
    </View>
  )
}*/
  function rendermap() {
    return (
      <View
        style={{backgroundColor: 'red', height: hp('30%'), width: wp('100%')}}>
        {
          // render this with key to get work done without error
          // enter key in appdelegate.m and anroidmenifesto.xml
        }
             <MapView
        style={{height:hp('30%' , wp('100%'))}}
        provider={PROVIDER_GOOGLE}>

        </MapView>
      </View>
    );
  }

  return (
    <View style={{flex: 1}}>
      {/* <MapView
        style={{flex: 1}}


      />*/}
      {rendermap()}
    </View>
  );
};
export default Mapsorder;
