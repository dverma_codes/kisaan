/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Image, Dimensions, TextInput} from 'react-native';
import * as themes from '../stack/theme';
import AntICON from 'react-native-vector-icons/AntDesign';
import {Block, Text} from '../config';
import {DrawerActions} from '@react-navigation/native';
import Imagecom from './Imagecom';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';

import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import SearchBar from 'react-native-search-bar';
export default function Prolistgrid(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View
        style={{
          height: 100,
          marginTop: 0,
          marginLeft: 20,
          width: 100,
          backgroundColor: 'white',
          paddingTop: 5,
          paddingBottom: 5,
        }}>
        <Image
          source={{uri: props.image}}
          style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
        />
      </View>
      <View style={[styles.Rightside, {marginLeft: 20}]}>
        <View style={styles.textholder}>
          <TouchableOpacity onPress={null}>
            <Text style={{marginTop: 10, marginLeft: 20}}>
              hello {props.Text}
            </Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity onPress={null}>
          <View
            style={{
              marginLeft: 20,
              height: 20,
              marginBottom: 20,
              width: 80,
              borderRadius: 15,
              alignItems: 'center',
              backgroundColor: themes.colors.primary,
            }}>
            <Text white bold style={{marginTop: 3, fontSize: 10}}>
              Add To Cart
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 100,
    backgroundColor: themes.colors.white,
  },
  image: {
    flex: 2,
    width: null,
    height: hp('5'),
    resizeMode: 'stretch',
  },
  rightside: {
    flex: 4,
  },
  textholder: {
    marginTop: 10,
    marginRight: 140,
    flexWrap: 'wrap',
    marginLeft: 0,
    flex: 1,
  },
  text: {
    fontSize: 12,
  },
  addcart: {
    marginLeft: 10,
    backgroundColor: themes.colors.primary,
    borderRadius: 15,
    alignItems: 'center',
  },
  textcart: {
    marginTop: 10,
    alignSelf: 'center',
  },
});
