/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  // TextInput,
  View,
  StyleSheet,
  Platform,
} from "react-native";

import {TextInput, HelperText} from 'react-native-paper';

export default (props) => {
  const {
    name,
    title,
    errors,
    handleChange,
    handleBlur,
    values,
    touched,
    setFieldTouched,
    stylepro,
    placeholder,
    icon,
    type,
  } = props;

  const hasError =
    typeof errors[name] !== 'undefined' && errors[name] && touched[name];

  const inputType = type || 'text';

  return (
    <>
      <View>
        <View style={styles.sectionStyle}>
          <TextInput
            label={title}
            error={hasError}
            style={{...(stylepro || {}), backgroundColor: '#fff' , fontSize:Platform.OS==="ios" ? 12: 15 ,}}
            placeholder={placeholder || ''}
            onChangeText={handleChange(name)}
            onBlur={() => setFieldTouched(name)}
            value={values[name]}
            secureTextEntry={inputType == 'password'}
            right={<TextInput.Icon icon={icon} />}
          />
          {/* {icon || false ? (
          <Image source={icon} style={styles.imageStyle} />
        ) : null} */}
          {/* <HelperText type="error" visible={hasError}>
          Email address is invalid!
        </HelperText> */}
        </View>

        {hasError && (
          <HelperText type="error" visible={hasError}>
            {errors[name]}
          </HelperText>
          // <Text
          //   style={{
          //     color: 'red',
          //     fontSize: 12,
          //     padding: 0,
          //     margin: 0,
          //     marginTop: -10,
          //   }}>
          //   {hasError
          //     ? errors[name]
          //     : ''}
          // </Text>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    borderColor: '#000',
    height: 40,
    borderRadius: 0,
    marginTop: 8,
    marginBottom:8,
  },
  imageStyle: {
    padding: 0,
    margin: 5,
    height: 5,
    width: 7,
    resizeMode: 'center',
    alignItems: 'center',
  },
});
