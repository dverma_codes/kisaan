/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Banner_swiper from './bannerswiper';

import * as theme from '../stack/theme';
import Swiper from 'react-native-swiper';
class Banner extends Component {
  state = {
    case: '',
  };

  render() {
    switch (this.props.Case) {
      case 'small_banner': {
        return (
          <View
            style={{
              height: 250,
              marginTop: 0,
              backgroundColor: 'white',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <Image
              source={this.props.imageSource ? this.props.imageSource : {uri: this.props.imageUri}}
              style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
            />
          </View>
        );
      }
      case 'large_banner': {
        return (
          <View
            style={{
              height: 400,
              marginTop: 0,
              backgroundColor: 'white',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <Image
              source={this.props.imageSource ? this.props.imageSource : {uri: this.props.imageUri}}
              style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
            />
          </View>
        );
      }
      case 'small_touch': {
        return (
          <TouchableOpacity>
            <View
              style={{
                height: 100,
                marginTop: 0,
                backgroundColor: 'white',
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <Image
                source={{uri: this.props.imageUri}}
                style={{
                  flex: 1,
                  width: null,
                  height: null,
                  resizeMode: 'cover',
                }}
              />
            </View>
          </TouchableOpacity>
        );
      }
      case 'large_touch': {
        return (
          <TouchableOpacity onPress={this.props.handleClick}>
            <View
              style={{
                height: 250,
                marginTop: 0,
                backgroundColor: 'white',
                paddingTop: 5,
                paddingBottom: 5,
              }}>
              <Image
                source={this.props.imageUri ? {uri: this.props.imageUri} : this.props.imageSource}
                style={{
                  flex: 1,
                  width: null,
                  height: null,
                  resizeMode: 'cover',
                }}
              />
            </View>
          </TouchableOpacity>
        );
      }
      case 'swipper_small': {
        return (
          <View style={{height: 250, marginTop: 0, paddingBottom: 5}}>
            <Swiper
              loop={false}
              dot={
                <View
                  style={{
                    backgroundColor: theme.colors.gray,
                    width: 10,
                    height: 10,
                    borderRadius: 4,
                    marginLeft: 3,
                    marginRight: 3,
                    marginTop: 3,
                    marginBottom: 3,
                  }}
                />
              }
              activeDot={
                <View
                  style={{
                    backgroundColor: theme.colors.white,
                    width: 10,
                    height: 10,
                    borderRadius: 4,
                    marginLeft: 3,
                    marginRight: 3,
                    marginTop: 3,
                    marginBottom: 3,
                  }}
                />
              }>
              {this.props.imageUri.map((imagesip, i) => {
                return <Banner_swiper imageUri={imagesip} keyimage={i} />;
              })}
            </Swiper>
          </View>
        );
      }

      case 'swipper_large': {
        return (
          <View
            style={{
              height: 200,
              marginTop: 0,
              backgroundColor: 'white',
              paddingTop: 5,
              paddingBottom: 10,
            }}>
            <Swiper
              loop={false}
              dot={
                <View
                  style={{
                    backgroundColor: theme.colors.gray,
                    width: 10,
                    height: 10,
                    borderRadius: 4,
                    marginLeft: 3,
                    marginRight: 3,
                    marginTop: 3,
                    marginBottom: 3,
                  }}
                />
              }
              activeDot={
                <View
                  style={{
                    backgroundColor: theme.colors.white,
                    width: 10,
                    height: 10,
                    borderRadius: 4,
                    marginLeft: 3,
                    marginRight: 3,
                    marginTop: 3,
                    marginBottom: 3,
                  }}
                />
              }>
              {this.props.images.map((bannerimage) => {
                return <Banner_swiper imageUri={bannerimage.image} />;
              })}
            </Swiper>
          </View>
        );
      }
    }
  }
}
export default Banner;
