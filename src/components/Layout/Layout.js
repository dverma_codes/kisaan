import {SafeAreaView, ScrollView, View} from "react-native";
import * as themes from "../../stack/theme";
import Mystatusbar from "../mystatusbar";
import Header from "../Header";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import Searchbar from "../Searchbar";
import Checkavail from "../checkavail";
import React from "react";
import {withNavigation} from "react-navigation";

function Layout({navigation, ...props}) {
    return <View style={{flex: 1}}>
        <SafeAreaView style={{backgroundColor: themes.colors.white}}>
            <ScrollView
                scrollEventThrottle={16}
                style={{backgroundColor: themes.colors.white}}>
                <Mystatusbar/>
                <Header
                    widhheader={wp('100%')}
                    heightheader={hp('10%')}
                    navigation={navigation}
                />
                <Searchbar
                back={true}/>
                <Checkavail/>
                {props.children}
            </ScrollView>
        </SafeAreaView>
    </View>
}

export default withNavigation(Layout);
