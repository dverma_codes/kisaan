/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
import React, {useState} from 'react';
import * as themes from '../stack/theme';
import Orientation from 'react-native-orientation-locker';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';

import {Block, Text} from '../config';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function Notsuppported(...props)
{
  return(
    <View style={{flex:1 , alignItems:'center' , alignContent:'center' , backgroundColor:themes.colors.primary}}>
      <Text white>We does not support Landscape mode Till now</Text>
    </View>
  )
}
