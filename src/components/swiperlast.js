import React, {useState} from 'react';
import Subscribe from '../components/Subscribe';
import * as themes from '../stack/theme';
import _ from 'lodash';
import SplashScreen from 'react-native-splash-screen';
import Orientation from 'react-native-orientation-locker';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  Button,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Swiperlast = () => {
  const [banner, Setbanner] = useState(['1', '2', '3']);
  return (
    <ScrollView
      style={{height: hp('25%')}}
      showsButtons={true}
      horizontal={true}
      centerContent={true}
      loop={true}
      showsHorizontalScrollIndicator={false}>
      {banner.map((i) => {
        return <Slides image={i} />;
      })}
    </ScrollView>
  );
};

const Slides = (props) => (
  <TouchableOpacity><View
    style={{
      height: hp('22%'),
      width: wp('86%'),
      marginLeft: 25,
      marginTop: 20,
      borderRadius: 5,
      borderColor:themes.colors.primary,
    }}
  >
    <Image style={{height:hp('22%') , width:wp('85%') , backgroundColor:themes.colors.brown
      ,borderRadius: 7,
      borderColor:themes.colors.primary,}}/>
  </View></TouchableOpacity>
);

export default Swiperlast;
