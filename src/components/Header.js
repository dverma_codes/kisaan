/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Image, Dimensions} from 'react-native';
import * as theme from '../stack/theme';

import {DrawerActions} from '@react-navigation/native';
import Imagecom from '../components/Imagecom';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {TouchableOpacity} from 'react-native-gesture-handler';
export default class Header extends React.Component {
  state = {
    case: '',
  };
  barpress = () => {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  };
  cartpress = () => {
    this.props.navigation.navigate('Cart');
  };

  render() {
    return (
      <View
        style={[
          styles.header,
          {
            width: width,
            height: this.props.heightheader,
            backgroundColor: theme.colors.white,
          },
        ]}>
        <View // logo
          style={{
            width: width * 0.6,
            height: this.props.heightheader,
            backgroundColor: 'white',
          }}>
          <Imagecom
            type={'kisaan_logo'}
            left={25}
            top={hp('2%')}
            height={hp('6%')}
            width={wp('19%')}
          />
        </View>
        <View
          style={{
            width: width * 0.4,
            height: this.props.heightheader,
            backgroundColor: theme.colors.white,
            justifyContent: 'flex-end',
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
          }}>
          {/*
         <TouchableOpacity style={styles.touchable} onPress={null}>
            <Image
              source={require('../assets/android/header1.png')}
              style={styles.imageStyle}
            />
          </TouchableOpacity> */}
          <TouchableOpacity style={styles.touchable} onPress={this.cartpress}>
            <Image
              source={require('../assets/android/header2.png')}
              style={styles.imageStyle}
            />
          </TouchableOpacity>

          <TouchableOpacity style={styles.touchable} onPress={this.barpress}>
            <Image
              source={require('../assets/android/header3.png')}
              style={styles.imageStyle}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const {width, height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: 'row',
  },
  imageStyle: {
    padding: 10,
    margin: wp('2.5%'),
    height: hp('2.5%'),
    width: wp('7%'),
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  touchable: {
    margin: wp('2.5%'),
    height: hp('5%'),
    width: wp('9%'),
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});
