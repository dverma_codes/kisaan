import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  StatusBar,
  Platform,
  SafeAreaView,
} from 'react-native';
import {Block, Text} from '../config';
import * as themes from '../stack/theme';
export default function MyStatusBar() {
  return (
    <View style={[styles.statusBar, {backgroundColor: themes.colors.white}]}>
      <SafeAreaView>
        <StatusBar
          translucent
          backgroundColor={themes.colors.white}
          barStyle="dark-content"
        />
      </SafeAreaView>
    </View>
  );
}

const STATUSBAR_HEIGHT = StatusBar.currentHeight;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  content: {
    flex: 1,
  },
});
