/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Avatar } from 'react-native-paper';

class Cat extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.handleClick}>
                <View style={{height: hp('17%'), width: wp('23%'), marginLeft: 10}}>
                    <View style={{flex: 1}}>
                        <Avatar.Image size={wp(20)} source={{uri: this.props.imageUri}} />
                        {/*<Image*/}
                        {/*    source={{uri: this.props.imageUri}}*/}
                        {/*    style={{*/}
                        {/*        flex: 2,*/}
                        {/*        width: null,*/}
                        {/*        height: hp('8'),*/}
                        {/*        // resizeMode: 'cover',*/}
                        {/*        borderRadius: 50,*/}
                        {/*    }}*/}
                        {/*/>*/}
                        <Text style={{flex: 1, fontSize: 12, alignSelf: 'center', paddingTop: 10}}>
                            {this.props.name}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default Cat;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
