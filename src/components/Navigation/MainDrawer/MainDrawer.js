import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {drawerNavigation} from '../../../../navigation';
import Custom from '../../../views/Drawer/custom';

const Drawer = createDrawerNavigator();

export default function MainDrawer() {
  return <Drawer.Navigator drawerContent={(props) => <Custom {...props} />}>
    {
      drawerNavigation.map(function({name, component, options = {}, ...otherProps}) {
        return <Drawer.Screen 
          key={name}
          name={name}
          component={component}
          options={options}
          {...otherProps} />
      })
    }
  </Drawer.Navigator>;
}