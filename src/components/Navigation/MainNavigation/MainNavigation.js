import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AuthNavigation from '../AuthNavigation/AuthNavigation';
import MainDrawer from '../MainDrawer/MainDrawer';

const Stack = createStackNavigator();

export default function MainNavigation(props) {
    return <Stack.Navigator>
        <Stack.Screen name={"Home"} component={MainDrawer} options={{headerShown: false}} />
        {!props.authUser && (<Stack.Screen name="Auth" component={AuthNavigation} options={{headerShown: false}} />)}
    </Stack.Navigator>
};