import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {authNavigation} from '../../../../navigation';

const Auth = createStackNavigator();

export default function AuthStack() {
  return (
    <Auth.Navigator>
      {
        authNavigation.map(function({name, component, options = {}, ...otherProps}) {
          return <Auth.Screen
            key={name}
            name={name}
            component={component}
            options={options}
            {...otherProps}
          />
        })
      }
    </Auth.Navigator>
  );
}