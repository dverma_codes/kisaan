import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

class Igbanner extends Component {
  render() {
    return (
      <View
        // eslint-disable-next-line react-native/no-inline-styles
        style={{
          height: 200,
          marginTop: 0,
          backgroundColor: 'white',
          paddingTop: 5,
          paddingBottom: 5,
        }}>
        <Image
          source={{uri: this.props.imageUri}}
          // eslint-disable-next-line react-native/no-inline-styles
          style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
        />
      </View>
    );
  }
}
export default Igbanner;
