import React, {Component} from 'react';
import {View, StyleSheet, Image, Dimensions} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Block, Text} from '../config';
import StarRating from 'react-native-star-rating';
import LinearGradient from 'react-native-linear-gradient';
import startfill from '../assets/android/Starfill.png';
import {formatPrice} from "../helpers/common";
import * as themes from '../stack/theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Swiper from 'react-native-swiper';
export default function Proandtophome({name, price, ...props}) {

  return (
    <View
      style={{
        width: width / 2.6,
        height: 290,
        marginLeft: 18,
        marginTop: 19,
        backgroundColor: props.color,
      }}>
      <Image
        style={{width: '95%', height: '50%', alignSelf: 'center'}}
        source={{
          // uri: 'https://www.atmeplay.com/images/users/avtar/avtar.png',
            uri: props.defaultImage ? props.defaultImage.url : 'https://www.atmeplay.com/images/users/avtar/avtar.png'
        }}
      />
      <Text style={{marginTop: 10, marginLeft: 10}}>{name ? name : "text only"}</Text>
      <View style={{marginTop: 10, marginLeft: 10, flexDirection: 'row'}}>
        <View style={{flex: 1}}>
          <StarRating
            disabled={false}
            maxStars={5}
            fullStar={startfill}
            rating={props.Starcount}
            starSize={10}
          />
        </View>
        <View style={{flex: 1}} />
      </View>
      <View style={{marginTop: 10, marginLeft: 10}}>
        <Text>{formatPrice(price)}</Text>
      </View>
      <TouchableOpacity style={{marginTop: 10}}>
        <LinearGradient
          start={{x: 1, y: 0}} //here we are defined x as start position
          end={{x: 0, y: 0}} //here we can define axis but as end position
          colors={['#0c461b', '#0d7b29']}
          style={{
            width: '50%',

            borderRadius: 25,
            marginLeft: 10,
            height: 25,
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <Text white style={{alignSelf: 'center', marginTop: 3}}>
            Add to cart
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}

const {width, height} = Dimensions.get('screen');
