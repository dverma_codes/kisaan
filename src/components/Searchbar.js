/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React,{ useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Image, Dimensions, TextInput} from 'react-native';
import * as theme from '../stack/theme';
import AntICON from 'react-native-vector-icons/AntDesign';
import {Block, Text} from '../config';
import { DrawerActions, useNavigation } from "@react-navigation/native";
import Imagecom from './Imagecom';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {TouchableOpacity} from 'react-native-gesture-handler';
import SearchBar from 'react-native-search-bar';
import search from '../assets/android/search.png';
export default function Searchbar(props)
{

  const [state,setState]=useState('');
  const list = ['The Weeknd', 'Drake', 'Roddy Ricch', 'Dua Lipa'];
  const navigation=useNavigation();
  return(

      <View
        style={{
          width: wp('100%'),
          height: 80,
          backgroundColor: theme.colors.secondary,
          paddingTop: 1,
        }}>
        <View style={styles.sectionStyle}>
          {
          props.back ? (
            <TouchableOpacity
            onPress={() =>navigation.goBack(null)}>
          <AntICON name='arrowleft'
          size={30}
          color={theme.colors.white} />
          </TouchableOpacity>
          ): null}
          <View style={
            {backgroundColor:theme.colors.white, flexDirection:'row',marginTop: 5,
            borderRadius:3, borderWidth:1, width:'100%',marginRight:5,
            borderColor: theme.colors.white,
            }}>
          <TextInput
            style={{
              flex: 1,
              fontFamily: 'Poppins-Regular',
              fontSize: 12,
              paddingLeft: 15,
              height: 40,
              borderRadius:3,
              width:'80%',
              backgroundColor: theme.colors.white,
            }}
            placeholder={'Search'}
            placeholderTextColor={theme.colors.gray}
            underlineColorAndroid="transparent"
            onTouchStart={() => navigation.navigate('Searchscreen')}
          />
          <TouchableOpacity
          style={{
             height: 40,
            width: 40,
            resizeMode: 'stretch',
            alignSelf: 'center',
            borderRadius:3,
            backgroundColor: theme.colors.white,

          }}
          onPress={()=>(viewfun([list,state]))}
          >
            <Image
            source={search}
            style={styles.imageStyle}
          />
          </TouchableOpacity>
          {
            // if touch button  search add 'TouchableOpacity' to image
          }
        </View>
        </View>

      </View>
  )
}


function viewfun([list,state])
{
 <View>
 {/* {filterList([list,state]).map((listItem, index) => (
   <Text key={index}>{listItem}</Text>
 ))} */}
 {// please add function for search herer
 }
</View>;
}

function filterList([list, state])
{
 list.filter((listItem) =>
 listItem.toLowerCase().includes(state.search.toLowerCase()),
 );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.secondary,
    borderWidth: 0,
    // borderRadius: 3,
    // borderBottomColor: '#e5e5e5',
    // borderBottomWidth: 1,
    // borderColor: '#000',
    height: 45,

    margin: 20,
  },
  imageStyle: {
    height: 20,
    width: 20,
    alignSelf:'center',
    marginTop:8,

    resizeMode: 'stretch',
  },
});

//   state = {
//     search: '',
//   };

//   filterList(list) {
//     return list.filter(listItem => listItem.toLowerCase().includes(this.state.search.toLowerCase()));
//   }

//   render() {
//     const list = ['The Weeknd', 'Drake', 'Roddy Ricch', 'Dua Lipa'];

//     return (
//       <View style={styles.container}>
//         <TextInput
//           onChangeText={(search) => this.setState({search})}
//           style={styles.searchBar}
//         />
//         {this.filterList(list).map((listItem, index) => (
//           <Text key={index} style={styles.itemText}>{listItem}</Text>
//         ))}
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: 'black',
//     alignItems: 'center',
//     height: '100%',
//   },
//   searchBar: {
//     fontSize: 24,
//     margin: 10,
//     width: '90%',
//     height: 50,
//     backgroundColor: 'white',
//   },
//   itemText: {
//     margin: 10,
//     color: 'white',
//     fontSize: 24,
//     backgroundColor: 'blue',
//     width: '100%',
//     height: 50
//   }
// });
