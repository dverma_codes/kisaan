//stars repo link official ducument:https://github.com/djchie/react-native-star-rating
// https://github.com/djchie/react-native-star-rating
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Block, Text} from '../config';
import StarRating from 'react-native-star-rating';
import LinearGradient from 'react-native-linear-gradient';

import * as themes from '../stack/theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Swiper from 'react-native-swiper';
export default class Product extends Component {
  render() {
    return (
      <View style={[styles.container, {backgroundColor: this.props.cols}]}>
        <View style={{flex: 1, marginTop: 5}}>
          <Image
            source={{uri: this.props.imageUri}}
            style={{
              flex: 1,
              width: wp('25%'),
              height: 100,
              resizeMode: 'cover',
            }}
          />
        </View>
        <View
          style={{
            flex: 1 / 5,
            height: 15,
            width: wp('35'),
            alignSelf: 'flex-start',
            paddingTop: 10,
            paddingLeft: 20,
            flexDirection: 'row',
          }}>
          <Text
            bold
            style={{
              alignSelf: 'flex-start',
              flex: 1,
              flexShrink: 1,
              flexDirection: 'column',
            }}>
            {this.props.texts}
          </Text>
        </View>
        <View
          style={{
            flex: 1 / 11,

            width: wp('14'),
            alignItems: 'flex-start',
            alignSelf: 'flex-start',
            paddingLeft: 20,
            marginTop: 0,
          }}>
          <StarRating
            disabled={false}
            maxStars={5}
            fullStar={require('../assets/android/Starfill.png')}
            rating={this.props.StarCount}
            starSize={10}
          />
        </View>
        <View
          style={{
            flex: 1 / 8,
            alignSelf: 'flex-start',
            paddingLeft: 20,
          }}>
          <Text bold>
            {this.props.Price}
            {'  '}
          </Text>
          {/* <Text gray bold style={{textDecorationLine: 'line-through'}}>
            {this.props.Pricedrop}
          </Text> */}
        </View>
        <View
          style={{
            width: wp('12'),
            height: 30,
            flex: 1 / 5,
            marginTop: 0,
            alignItems: 'center',
          }}>
          {/*<TouchableOpacity*/}
          {/*  style={{*/}
          {/*    height: '100%',*/}
          {/*    justifyContent: 'center',*/}
          {/*    alignItems: 'center',*/}
          {/*    width: wp('30%'),*/}
          {/*  }}*/}
          {/*  onPress={null}>*/}
          {/*  <LinearGradient*/}
          {/*    start={{x: 1, y: 0}} //here we are defined x as start position*/}
          {/*    end={{x: 0, y: 0}} //here we can define axis but as end position*/}
          {/*    colors={['#0c461b', '#0d7b29']}*/}
          {/*    style={{*/}
          {/*      borderWidth: 1,*/}
          {/*      borderRadius: 20,*/}
          {/*      height: 25,*/}
          {/*      width: wp('30%'),*/}

          {/*      alignItems: 'center',*/}
          {/*    }}>*/}
          {/*    <View style={{alignSelf: 'center'}}>*/}
          {/*      <Text white bold style={{fontSize: 9, marginTop: 5}}>*/}
          {/*        SUBCRIPTION @ {this.props.pricesub}*/}
          {/*      </Text>*/}
          {/*    </View>*/}
          {/*  </LinearGradient>*/}
          {/*</TouchableOpacity>*/}
        </View>
        <View
          style={{
            height: 40,
            flex: 1 / 3,
            marginTop: 0,
            alignSelf: 'flex-start',
          }}>
          <TouchableOpacity
            style={{
              backgroundColor: themes.colors.secondary,
              borderColor: themes.colors.white,
              borderWidth: 1,
              borderRadius: 5,
              height: 39,
              justifyContent: 'center',
              alignItems: 'flex-start',
              width: wp('40%'),
              marginTop: 7,
            }}
            onPress={null}>
            <View style={{alignSelf: 'center'}}>
              <Text white bold>
                BUY NOW
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: wp('40%'),
    marginLeft: wp('5%'),
    borderRadius: 5,
  },
});
