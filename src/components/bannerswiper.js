/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

class Banner_swiper extends Component {
    render() {

        return (
            <TouchableOpacity onPress={null}>
                <View
                    style={{
                        height: 250,
                        marginTop: 0,
                        backgroundColor: 'white',
                        paddingTop: 5,
                        paddingBottom: 5,
                    }}>
                    <Image
                        source={{uri: this.props.imageUri}}
                        key={this.props.keyimage}
                        style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
                    />
                </View>
            </TouchableOpacity>
        );
    }
}

export default Banner_swiper;
