/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, StyleSheet, Platform } from "react-native";
import {Image} from 'react-native';
import * as theme from '../stack/theme';

export default class Imagecom extends React.Component {
  state = {
    case: '',
  };
  componentDidMount() {}
  render() {
    switch (this.props.type) {
      case 'right_circle': {
        return (
          <View style={{flex: 1 , alignSelf:'flex-end' }}>
            <Image
            style={{

              alignSelf: 'flex-end',
              marginTop:Platform.OS ==='ios' ? -10 : 18,
              height: 70,
              width: 50,
              position: 'absolute',
            }}
            resizeMode={'center'}
            source={require('../assets/common/Ellipse1copy.png')}
          />
          </View>
        );
      }
      case 'kisaan_logo': {
        return (
          <Image
            style={{
              ...StyleSheet.absoluteFillObject,
              alignSelf: 'flex-end',
              marginLeft: this.props.left,
              marginTop: this.props.top,
              height: this.props.height,
              width: this.props.width,
              position: 'absolute',
            }}
            resizeMode={'stretch'}
            source={require('../assets/common/kisaanlogo.png')}
          />
        );
      }
      case 'login_and_account': {
        return (
          <Image
            style={{
              ...StyleSheet.absoluteFillObject,
              alignSelf: 'flex-end',
              marginLeft: this.props.left,
              marginTop: this.props.top,
              height: this.props.height,
              width: this.props.width,
              position: 'absolute',
            }}
            resizeMode={'stretch'}
            source={require('../assets/common/loginandaccount.png')}
          />
        );
      }
      case 'register': {
        return (
          <Image
            style={{
              ...StyleSheet.absoluteFillObject,
              alignSelf: 'flex-end',
              marginLeft: this.props.left,
              marginTop: this.props.top,
              height: this.props.height,
              width: this.props.width,
              position: 'absolute',
            }}
            resizeMode={'stretch'}
            source={require('../assets/common/register.png')}
          />
        );
      }
      case 'forgot_password': {
        return (
          <Image
            style={{
              ...StyleSheet.absoluteFillObject,
              alignSelf: 'flex-end',
              marginLeft: this.props.left,
              marginTop: this.props.top,
              height: this.props.height,
              width: this.props.width,
              position: 'absolute',
            }}
            resizeMode={'stretch'}
            source={require('../assets/common/forgot.png')}
          />
        );
      }
      case 'shipment': {
        return (
          <Image
            style={{
              ...StyleSheet.absoluteFillObject,
              alignSelf: 'flex-end',
              marginLeft: this.props.left,
              marginTop: this.props.top,
              height: this.props.height,
              width: this.props.width,
              position: 'absolute',
            }}
            resizeMode={'stretch'}
            source={require('../assets/common/shipmenttrack.png')}
          />
        );
      }
      case 'custom': {
        return (
          <Image
            style={{
              ...StyleSheet.absoluteFillObject,
              alignSelf: 'flex-end',
              marginLeft: this.props.left,
              marginTop: this.props.top,
              height: this.props.height,
              width: this.props.width,
              position: 'absolute',
            }}
            resizeMode={'stretch'}
            source={this.props.path}
          />
        );
      }
    }
  }
}
