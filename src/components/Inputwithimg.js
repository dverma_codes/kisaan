/* eslint-disable eslint-comments/no-unused-disable */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {Image, SafeAreaView} from 'react-native';
import * as theme from '../stack/theme';

export default class Inputwithimg extends React.Component {
  state = {
    case: '',
  };
  componentDidMount() {}
  render() {
    switch (this.props.type) {
      case 'email_input': {
        return (
          <View style={styles.sectionStyle}>
            <TextInput
              // eslint-disable-next-line react-native/no-inline-styles
              style={{flex: 1, fontFamily: 'Poppins-Black'}}
              placeholder={this.props.hold}
              underlineColorAndroid="transparent"
            />
            <Image
              source={require('../assets/common/mail.png')}
              style={styles.imageStyle}
            />
          </View>
        );
      }
      case 'pass_input': {
        return (
          <View style={styles.sectionStyle}>
            <TextInput
              style={{flex: 1, fontFamily: 'Poppins-Black'}}
              placeholder={this.props.hold}
              underlineColorAndroid="transparent"
            />
            <Image
              source={require('../assets/common/password.png')}
              style={styles.imageStyle}
            />
          </View>
        );
      }
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 0,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    borderColor: '#000',
    height: 35,
    borderRadius: 0,
    margin: 8,
  },
  imageStyle: {
    padding: 10,
    margin: 5,
    height: 10,
    width: 12,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});
