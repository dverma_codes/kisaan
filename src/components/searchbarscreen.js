import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {Image, Dimensions, TextInput, InteractionManager} from 'react-native';
import * as theme from '../stack/theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {TouchableOpacity} from 'react-native-gesture-handler';
import search from '../assets/android/search.png';
import AntICON from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';

export default class Searchbar extends React.Component {
  /* const [state, setState] = useState('');
  const navigation = useNavigation();*/
  state = {};
  inputRef = React.createRef();
  componentDidMount() {
    this.focusInputWithKeyboard();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.focusInputWithKeyboard();
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener();
  }

  focusInputWithKeyboard() {
    console.log('focus');
    this.inputRef.current.focus();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: theme.colors.white}}>
        <View style={{flex: 0.5}} />
        <View style={{flex: 1, backgroundColor: theme.colors.white}}>
          <View
            style={{
              width: wp('100%'),
              height: 80,
              backgroundColor: theme.colors.secondary,
              paddingTop: 1,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 5,
                height: 70,
                width: '100%',
                marginRight: 5,
                alignItems: 'center',
              }}>
              <View>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack(null)}>
                  <AntICON
                    name="arrowleft"
                    size={30}
                    color={theme.colors.white}
                  />
                </TouchableOpacity>
              </View>
              <TextInput
                ref={this.inputRef}
                style={{
                  flex: 1,
                  fontFamily: 'Poppins-Regular',
                  fontSize: 12,
                  paddingLeft: 15,
                  height: 40,
                  borderRadius: 3,
                  width: '80%',
                  backgroundColor: theme.colors.white,
                }}
                returnKeyType="go"
                placeholder={'Search'}
                placeholderTextColor={theme.colors.gray}
                underlineColorAndroid="transparent"
                onChangeText={null}
              />
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 0,
                  marginLeft: 0,
                  marginRight: 20,
                  height: 40,
                  width: 40,
                  resizeMode: 'stretch',
                  alignSelf: 'center',
                  borderRadius: 3,
                  backgroundColor: theme.colors.white,
                }}
                onPress={console.log('working')}>
                <Image source={search} style={styles.imageStyle} />
              </TouchableOpacity>
              {
                // if touch button  search add 'TouchableOpacity' to image
              }
            </View>
          </View>
        </View>
        <View style={{flex: 9}}>
          <ScrollView scrollEventThrottle={16} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.secondary,
    borderWidth: 0,
    // borderRadius: 3,
    // borderBottomColor: '#e5e5e5',
    // borderBottomWidth: 1,
    // borderColor: '#000',
    height: 45,

    margin: 20,
  },
  imageStyle: {
    height: 20,
    width: 20,
    alignSelf: 'center',
    marginTop: 8,

    resizeMode: 'stretch',
  },
});
