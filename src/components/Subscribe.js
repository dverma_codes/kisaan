import * as themes from '../stack/theme';
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Button,
} from 'react-native';
import {Block, Text} from '../config';
import LinearGradient from 'react-native-linear-gradient';
import facebook from '../assets/shareicons/facebook.png';
import twitter from '../assets/shareicons/twitter.png';
import youtube from '../assets/shareicons/youtube.png';
import printest from '../assets/shareicons/printest.png';
import instagram from '../assets/shareicons/instagram.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function Sharethis(props) {
  return (
    <View
      style={{
        height: 350,
        width: wp('100%'),
        backgroundColor: themes.colors.secondary,
        alignContent: 'center',
        flexDirection: 'column',
        paddingTop: 30,
      }}>
      <View
        style={{
          flex: 2,
          marginRight: 20,
          marginLeft: 20,
          flexDirection: 'column',
        }}>
        <Text white bold style={{flex: 1, fontSize: 23}}>
          Subscribe to our Newsletter
        </Text>
        <Text
          white
          bold
          numberOfLines={2}
          style={{flex: 1, fontSize: 13, flexDirection: 'row'}}>
          LONGLONG TEXT it is test of long long text
        </Text>
      </View>
      <View style={{flex: 2, flexDirection: 'row', paddingLeft: 20}}>
        <TextInput
          style={{
            flex: 2 / 3.2,
            height: 45,
            paddingLeft: 20,
            borderColor: 'gray',
            backgroundColor: themes.colors.white,
            borderRadius: 25,
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
          }}
          placeholder={'Enter your email address'}
          placeholderTextColor={themes.colors.gray}
          onTextInput={null}
          onChange={null}
        />
        <TouchableOpacity
          style={{
            flex: 1 / 3,
            width: '100%',
            height: '100%',
          }}
          onPress={null}>
          <LinearGradient
            start={{x: 1, y: 0}} //here we are defined x as start position
            end={{x: 0, y: 0}} //here we can define axis but as end position
            colors={['#0c461b', '#0d7b29']}
            style={{
              width: wp('30%'),
              marginRight: 20,
              borderRadius: 25,
              borderBottomLeftRadius: 0,
              borderTopLeftRadius: 0,
              height: 45,
              alignItems: 'center',
            }}>
            <View style={{alignItems: 'center'}}>
              <Text white bold style={{fontSize: width / 25, marginTop: 12}}>
                REACH US >
              </Text>
            </View>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View style={{flex: 3, paddingLeft: 20}}>
        <Text white bold style={{flex: 1 / 3, fontSize: wp('100%') / 18}}>
          Follow Us
        </Text>
        <View style={{flex: 1, flexDirection: 'row', marginTop:10}}>
          <View style={{flex: 1}}>
            <TouchableOpacity>
              <Image style={styles.image} source={facebook} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, marginLeft: 20}}>
            <TouchableOpacity>
              <Image style={styles.image} source={instagram} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, marginLeft: 20}}>
            <TouchableOpacity>
              <Image style={styles.image} source={youtube} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, marginLeft: 20}}>
            <TouchableOpacity>
              <Image style={styles.image} source={twitter} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, marginLeft: 20}}>
            <TouchableOpacity>
              <Image style={styles.image} source={printest} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 5}} />
        </View>
      </View>
    </View>
  );
}
const {width, height} = Dimensions.get('screen');


const styles = StyleSheet.create({
  image: {
    height: 45,
    width: 45,
  },
});
