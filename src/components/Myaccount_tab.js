/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import * as themes from '../stack/theme';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {Block, Text} from '../config';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {TouchableOpacity, TextInput} from 'react-native-gesture-handler';

export default class Myaccount_tab extends React.Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: themes.colors.accent,
          height: hp('8%'),
          width: width - 40,
          marginTop:2,
          flexDirection: 'row',
          borderWidth:1,
          borderColor: themes.colors.accent,
        }}>
        <View style={{flexDirection: 'column'}}>
          <Text
            secondary
            style={{
              marginLeft: 10,
              height: hp('3%'),
              width: wp('72%'),
              marginTop: 5,
              fontSize: 20,
            }}>
            {this.props.Header}
          </Text>
          <TextInput
            style={{
              height: hp('5%'),
              width: wp('72%'),
              marginLeft: 13,
              marginTop: -10,
            }}
            editable={false}
            // editable={this.state.confirmResult ? false : true}
          >
            <Text textcolor fontSize={20}>
              {this.props.UserText}
            </Text>
          </TextInput>
        </View>
        <View style={{flex: 1 / 3}}>
          <TouchableOpacity
            style={{
              backgroundColor: themes.colors.textcolor,
              height: hp('3%'),
              width: wp('13%'),
              marginTop: hp('2%'),
            }}
            onPress={null}>
            <Text white bold style={{fontSize: 15,marginTop:4, alignSelf: 'center'}}>
              EDIT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const {width, height} = Dimensions.get('screen');
