/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Image, Platform, Dimensions, TextInput} from 'react-native';
import * as theme from '../stack/theme';
import {Block, Text} from '../config';
import {DrawerActions} from '@react-navigation/native';
import Imagecom from './Imagecom';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {TouchableOpacity} from 'react-native-gesture-handler';
import SearchBar from 'react-native-search-bar';
import Enincon from 'react-native-vector-icons/Ionicons';


export default class Checkavail extends React.Component {
  state = {
    pincode: [],
  };
  cartpress = () => {
    this.props.navigation.navigate('MyAccount.YourAddresses', {
      peramenters: 'check',
    });
  };
  componentDidMount() {
    //object address has to pas in the component
    // this.props.address
    /// ah dek lio kive hou ga
  }

  render() {
    if (this.state.pincode && this.state.pincode.length) {
      return (
        <View
          style={{
            width: wp('100%'),
            height: 47,
            backgroundColor: theme.colors.checkavail,
            paddingTop: 1,
          }}>
          <View style={[styles.sectionStyle, {}]}>
            <Enincon
              style={{flex: 1}}
              name="location-sharp"
              size={20}
              color={theme.colors.white}
            />
            <View style={{flex: 4, height: 30, width: '30%'}}>
              <TouchableOpacity
                onpress={this.cartpress}
                style={{
                  borderColor: theme.colors.white,
                  width: '100%',
                  height: 30,

                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <Text white style={{fontSize: 15}}>
                  Delivery To{' '}
                  {
                    //from props
                  }
                </Text>
              </TouchableOpacity>
            </View>
            {/* <TextInput
            style={{
              flex: 1,
              fontFamily: 'Poppins-Regular',
              fontSize: 10,
               height: 25,
              paddingTop:1,
              paddingBottom:2,
              paddingLeft: 10,
              borderColor: theme.colors.white,
              backgroundColor: theme.colors.white,
              borderRadius: 0,
              marginLeft: 10,
             // alignContent: 'stretch',
              //alignItems: 'stretch'
            }}
            placeholder={'Enter  Your  Pincode'}
            placeholderTextColor={theme.colors.gray}
            underlineColorAndroid="transparent"
         /> */}
            <TouchableOpacity
              onPress={this.cartpress}
              style={{
                borderColor: theme.colors.white,
                width: 100,
                height: 30,
                flex: 2,

                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderRadius: 20,
                marginRight: 6,
              }}>
              <Text white bold>
                Check Now
              </Text>
            </TouchableOpacity>
            {
              // if touch button  search add 'TouchableOpacity' to image
            }
          </View>
        </View>
      );
    } else {
      return (
        <View
          style={{
            width: wp('100%'),
            height: 47,
            backgroundColor: theme.colors.checkavail,
            paddingTop: 1,
          }}>
          <View
            style={[
              styles.sectionStyle,
              {
                justifyContent: 'center',
                alignItems: 'center',
              },
            ]}>
            <Image
              source={require('../assets/android/avail.png')}
              style={styles.imageStyle}
            />
            <Text
              white
              bold
              style={{flex: 2.7, fontSize: Platform.OS == 'ios' ? 10 : 12}}>
              Check Availabity
            </Text>
            <TextInput
              style={{
                flex: 4,
                fontFamily: 'Poppins-Regular',
                fontSize: 10,
                height: 25,
                paddingTop: 1,
                paddingBottom: 2,
                paddingLeft: 10,
                borderColor: theme.colors.white,
                backgroundColor: theme.colors.white,
                borderRadius: 0,
                marginLeft: 5,
                width: '100%',
                // alignContent: 'stretch',
                //alignItems: 'stretch'
              }}
              placeholder={'Enter Your Pincode'}
              placeholderTextColor={theme.colors.gray}
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity
              style={{
                borderColor: theme.colors.white,
                width: 100,
                height: 30,
                flex: 2,
                marginLeft: 10,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderRadius: 20,
                marginRight: 6,
              }}>
              <Text white bold>
                Check Now
              </Text>
            </TouchableOpacity>
            {
              // if touch button  search add 'TouchableOpacity' to image
            }
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  sectionStyle: {
    flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: theme.colors.checkavail,
    borderWidth: 0,
    borderRadius: 0,
    borderBottomColor: theme.colors.checkavail,
    borderBottomWidth: 1,
    borderColor: '#000',
    height: 30,

    margin: 10,
  },
  imageStyle: {
    padding: 10,
    flex: 1 / 10,
    margin: 10,
    height: 7,
    width: 10,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});
